//
//  Session.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import Foundation
import UIKit
import SAMKeychain
import UserNotifications


enum Session {
    @UserDefault(key: "accessToken", defaultValue: nil)
    static var accessToken: String?
    
    @UserDefault(key: "currentUserID", defaultValue: nil)
    static var currentUserID: String?
    
    @UserDefault(key: "motionTagJWTToken", defaultValue: nil)
    static var motionTagJWTToken: String?
    
    @Keychain(account: "password")
    static var password: String?
    
    static var currentUser: User? {
        get { JSONCache.currentUser.loadFromFile() }
        set {
            if let user = newValue {
                currentUserID = user.id
            }
            JSONCache.currentUser.saveToFile(newValue)
            currentUserID = newValue?.id
        }
    }
    
    static var language: String {
        return Locale.autoupdatingCurrent.languageCode ?? "en"
    }
    
    static func isValidSession() -> Bool {
        return currentUserID?.isEmpty == false && accessToken?.isEmpty == false
    }
    
    static func shouldRestoreUserSession () -> Bool {
        let isComplete = currentUser != nil
        return !isComplete
    }
    
    static func close(error: Error? = nil) {
        guard self.isValidSession() else {
            return
        }
        
        self.removeUserIDAndAccessToken()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        Notifications.sessionClosed.post(error)
    }
    
    static func removeUserIDAndAccessToken() {
        Session.currentUserID = nil
        Session.accessToken = nil
        Session.currentUser = nil
        Session.motionTagJWTToken = nil
        UserDefaults.standard.synchronize()
    }
}
