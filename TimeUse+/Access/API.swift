//
//  API.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import Foundation
import Alamofire
import Tapptitude

struct APISettings {
    static let serverURL = Constants.API_URL
    static let errorDomain = "API+TimeUse+"
    
    var httpHeaders: [HTTPHeader] {
        var headers: [HTTPHeader] = []
        if let token = Session.accessToken {
            headers.append(HTTPHeader.authorization("Bearer \(token)"))
        }
        headers.append(HTTPHeader(name: "language", value: Session.language))
        return headers
    }
}

typealias Result = Tapptitude.Result

class API {
    
    // MARK: - Authentication
    
    @discardableResult
    static func registerWithEmail(_ email: String, password: String, registrationCode: String, callback: @escaping (_ result: Result<AuthSession>)->Void) -> TTCancellable? {
        let path = "auth/register"
        let params : [String: Any] = ["email": email,
                                      "password": password,
                                      "code": registrationCode,
                                      "uuid": UUID().uuidString]
        
        return request(.post, path: path, parameters: params, encoding: .json).responseAPIDecode { (response,  result: Result<AuthSession>) in
            if let session = result.value {
                Session.accessToken = session.token
                Session.motionTagJWTToken = session.user.jwtToken
                Session.currentUser = session.user
            }
            callback(result)
        }
    }
    
    @discardableResult
    static func loginWithEmail(_ email: String, password: String, callback: @escaping (_ result: Result<AuthSession>)->Void) -> TTCancellable? {
        let path = "auth/login"
        let params : [String: Any] = ["email": email,
                                      "password": password]
        
        return request(.post, path: path, parameters: params, encoding: .json).responseAPIDecode { (response,  result: Result<AuthSession>) in
            if let session = result.value {
                Session.accessToken = session.token
                Session.motionTagJWTToken = session.user.jwtToken
                Session.currentUser = session.user
            }
            callback(result)
        }
    }
    
    @discardableResult
    static func logout(_ callback: @escaping (Result<Void>) -> Void) -> TTCancellable? {
        let path = "auth/logout"
        
        return request(.post, path: path, encoding: .url).responseAPI { (response, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func changePassword(oldPassword: String, newPassword: String, callback: @escaping (Result<Void>)->Void) -> TTCancellable? {
        let path = "auth/change-password"
        let params : [String: Any] = ["oldPassword": oldPassword,
                                      "newPassword": newPassword]
        
        return request(.post, path: path, parameters: params, encoding: .json).responseAPI { (response, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func resetPassword(email: String, newPassword: String, registrationCode: String, callback: @escaping (Result<Void>)->Void) -> TTCancellable? {
        let path = "auth/reset-password"
        let params : [String: Any] = ["email": email,
                                      "inviteCode": registrationCode,
                                      "newPassword": newPassword]
        
        return request(.post, path: path, parameters: params, encoding: .json).responseAPI { (response, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func updateUser(settings: UserSettings, callback: @escaping (Result<AuthSession>)->Void) -> TTCancellable? {
        let path = "users/me"
        let params: [String: Any] = ["settings": ["transferDataOverWifi": settings.transferDataOverWifiBool]]
        
        return request(.put, path: path, parameters: params, encoding: .json).responseAPIDecode { (response, result: Result<AuthSession>) in
            if let session = result.value {
                Session.currentUser = session.user
            }
            callback(result)
        }
    }
    
    @discardableResult
    static func getFAQ(callback: @escaping (Result<[FAQ]>) -> Void) -> TTCancellable? {
        let path = "faq"
        
        return request(.get, path: path, encoding: .url).responseAPIDecode(keyPath: "data") { (reponse, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func getSettings(callback: @escaping (Result<Settings>) -> Void) -> TTCancellable? {
        let path = "users/settings"
        
        return request(.get, path: path, encoding: .url).responseAPIDecode(keyPath: "settings") { (reponse, result) in
            callback(result)
        }
    }
    
    // MARK: - Events
    
    @discardableResult
    static func getEvents(by day: String, timezone: String, callback: @escaping (Result<EventsData>) -> Void) -> TTCancellable? {
        let path = "events"
        let params: [String: Any] = ["day": day,
                                     "timezone": timezone]
        
        return request(.get, path: path, parameters: params, encoding: .url).responseAPIDecode(keyPath: "data") { (reponse, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func getEvent(by id: String, callback: @escaping (Result<EventData>) -> Void) -> TTCancellable? {
        let path = "events/\(id)"
        
        return request(.get, path: path, encoding: .url).responseAPIDecode(keyPath: "data") { (reponse, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func getCalendarEvents(callback: @escaping (Result<CalendarEvents>) -> Void) -> TTCancellable? {
        let path = "events/calendar"
        
        return request(.get, path: path, encoding: .url).responseAPIDecode(keyPath: "data") { (reponse, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func updateEvent(id: String, type: String, duration: Int, activities: [EventActivity], callback: @escaping (Result<Void>) -> Void) -> TTCancellable? {
        let path = "events/\(id)"
        var params: [String: Any] = ["name": type,
                                     "duration": duration]
        
        if activities.isEmpty {
            params["activities"] = []
        } else {
            params["activities"] = try? activities.asArrayOfDict()
        }
        
        return request(.put, path: path, parameters: params, encoding: .json).responseAPI { (response, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func deleteEvent(by id: String, callback: @escaping (Result<Void>) -> Void) -> TTCancellable? {
        let path = "events/\(id)"
        
        return request(.delete, path: path, encoding: .url).responseAPI { (reponse, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func addUntrackedEvent(untrackedEvents: [String], startDate: Date, endDate: Date, timezone: String, callback: @escaping (Result<Void>) -> Void) -> TTCancellable? {
        let path = "events/untracked"
        let params: [String: Any] = ["untrackedEvents": untrackedEvents,
                                     "startDate": "\(startDate)",
                                     "endDate": "\(endDate)",
                                     "startDateTimezone": timezone,
                                     "endDateTimezone": timezone]
        
        return request(.post, path: path, parameters: params, encoding: .json).responseAPI { (response, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func updateUntrackedEvent(id: String, untrackedEvents: [String], callback: @escaping (Result<Void>) -> Void) -> TTCancellable? {
        let path = "events/untracked/\(id)"
        let params: [String: Any] = ["untrackedEvents": untrackedEvents]
        
        return request(.put, path: path, parameters: params, encoding: .json).responseAPI { (response, result) in
            callback(result)
        }
    }
    
    @discardableResult
    static func mergeEvent(by id: String, callback: @escaping (Result<Void>) -> Void) -> TTCancellable? {
        let path = "events/\(id)/merge"
        
        return request(.delete, path: path, encoding: .url).responseAPI { (reponse, result) in
            callback(result)
        }
    }
    
    // MARK: - Statistics
    
    @discardableResult
    static func getStatistics(from: String, to: String, timezone: String, orderBy: MovingStatisticsOrderType = .totalEvents, callback: @escaping (Result<Statistics>) -> Void) -> TTCancellable? {
        let path = "events/statistics"
        let params: [String: Any] = ["from": from,
                                     "to": to,
                                     "timezone": timezone,
                                     "orderBy": orderBy.rawValue]
        
        return request(.get, path: path, parameters: params, encoding: .url).responseAPIDecode(keyPath: "data") { (reponse, result) in
            callback(result)
        }
    }
    
    // MARK: - Notifications
    
    @discardableResult
    static func updatePushToken(_ token: String, _ callback: ((Result<Void>) -> Void)? = nil) -> TTCancellable? {
        let path = "users/fcm"
        let params = ["token": token]
        
        return request(.put, path: path, parameters: params, encoding: .json).responseAPI { (response, result) in
            callback?(result)
        }
    }
    
    // MARK: - Presets
    
    @discardableResult
    static func getPresets(callback: @escaping (Result<Presets>) -> Void) -> TTCancellable? {
        let path = "groups/presets"
        
        return request(.get, path: path, encoding: .url).responseAPIDecode(keyPath: "presets") { (response, result: Result<Presets>) in
            callback(result)
        }
    }
}
