//
//  API+Config.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import Foundation
import Alamofire

extension API {
    /// apply custom API settings like URL, httpheaders
    class func request(_ method: HTTPMethod, path: String, serverURL: String = APISettings.serverURL, parameters: [String : Any]? = nil, encoding: Encoding = .json, headers: [String: String]? = nil) -> DataRequest {
        
        let url = URL(string: serverURL)!.appendingPathComponent(path)
        
        // append http headers
        var mutableHeaders = headers?.compactMap { HTTPHeader(name: $0.key, value: $0.value) } ?? []
        APISettings().httpHeaders.forEach { mutableHeaders.append($0) }
        
        var request = try! URLRequest(url: url, method: method, headers: HTTPHeaders(mutableHeaders))
        
        switch encoding {
        case .url_jsonQuery:
            // params into a JSON ---> ?json=json
            if let params = parameters {
                let data = try! JSONSerialization.data(withJSONObject: params, options: [])
                let json = String(data: data, encoding: .utf8)!
                request = try! URLEncoding.default.encode(request, with: ["json": json])
            }
        case .url, .json:
            request = try! encoding.default.encode(request, with: parameters)
        }
        
        request.timeoutInterval = 30.0;//seconds
        
        return Alamofire.AF.request(request)
    }
    
    enum Encoding {
        case json
        case url
        case url_jsonQuery
        
        var `default`: ParameterEncoding {
            switch self {
            case .json:
                #if DEBUG
                return JSONEncoding.prettyPrinted
                #else
                return JSONEncoding.default
                #endif
            case .url: return URLEncoding.default
            case .url_jsonQuery: abort()
            }
        }
    }
}


final class TTDecodableResponseSerializer<T: Decodable>: ResponseSerializer {
    
    typealias SerializedObject = Result<T>
    
    private lazy var errorSerializer = APIErrorResponseSerializer<Any>()
    private var keyPath: String?
    private var decoder: JSONDecoder
    
    init(keyPath: String? = nil, decoder: JSONDecoder = JSONDecoder()) {
        self.keyPath = keyPath
        self.decoder = decoder
    }
    
    public func serialize(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) throws -> Result<T> {
        do {
            let result = try errorSerializer.serialize(request: request, response: response, data: data, error: error)
            
            switch result {
            case .success(_):
                do {
                    var object:T
                    if let keyPath = keyPath {
                        object = try decoder.decode(T.self, from: data!, keyPath: keyPath, separator: ".")
                    } else {
                        object = try decoder.decode(T.self, from: data!)
                    }
                    return .success(object)
                }
                catch {
                    return .failure(error)
                }
            case .failure(let error):
                return .failure(error)
                
            }
        }
    }
}

final class APIJsonSerializer<T>: ResponseSerializer {
    
    typealias SerializedObject = Tapptitude.Result<T>
    
    private lazy var errorSerializer = APIErrorResponseSerializer<Any>()
    private var keyPath: String?
    
    init(keyPath: String? = nil) {
        self.keyPath = keyPath
    }
    
    public func serialize(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) throws -> Tapptitude.Result<T> {
        do {
            let result = try errorSerializer.serialize(request: request, response: response, data: data, error: error)
            
            switch result {
            case .success(_):
                do {
                    let value = try JSONResponseSerializer().serialize(request: request, response: response, data: data, error: error)
                    var newValue: Any? = value
                    if let keyPath = keyPath {
                        if let dictValue = value as? NSDictionary {
                            newValue = dictValue.value(forKeyPath: keyPath)
                        } else {
                            let failureReason = "Unexpected response format. Expected: NSDictionary ---> got: \(String(describing: newValue))"
                            let error = NSError(domain: APISettings.errorDomain, code: 2, userInfo: [NSLocalizedDescriptionKey : failureReason])
                            return .failure(error)
                        }
                    }
                    
                    if let value = newValue as? T {
                        return .success(value)
                    } else {
                        let failureReason = "Unexpected response format. Expected: \(T.self) ---> got: \(String(describing: newValue))"
                        let error = NSError(domain: APISettings.errorDomain, code: 2, userInfo: [NSLocalizedDescriptionKey : failureReason])
                        return .failure(error)
                    }
                } catch let error {
                    if let data = data {
                        print("Request failed:", request?.url?.absoluteString ?? "", "\nResponse:", String(data:data, encoding: String.Encoding.utf8) ?? "")
                    }
                    return .failure(error)
                }
            case .failure(let error):
                return .failure(error)
            }
        }
    }
}

struct APIErrorResponseSerializer<T: Any>: ResponseSerializer {
    typealias SerializedObject = Result<()>
    
    public func serialize(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) throws -> Result<()> {
        var apiError = error
        if let data = data {
            do {
                apiError = try JSONDecoder().decode(APIError.self, from: data)
            } catch DecodingError.dataCorrupted(let context) {
                print(context.debugDescription)
            } catch DecodingError.keyNotFound(let key, let context) {
                //No error
                print("\(key.stringValue) was not found, \(context.debugDescription)")
            } catch DecodingError.typeMismatch(let type, let context) {
                print("\(type) was expected, \(context.debugDescription)")
            } catch DecodingError.valueNotFound(let type, let context) {
                print("no value was found for \(type), \(context.debugDescription)")
            } catch {
                print("Unknown error")
            }
        }
        
        guard apiError == nil else {
            print("\n❗ Request failed: ",
                  request?.httpMethod ?? "",
                  request?.url?.absoluteString ?? "",
                  "\n\tResponse: " + (data.flatMap({ String(data:$0, encoding: .utf8) }) ?? ""))
            
            
            let missingSession = apiError == .missingSession
            let accessDenied = response?.statusCode == 403 || response?.statusCode == 401 || missingSession
            if accessDenied {
                DispatchQueue.main.async {
                    Session.close(error: apiError!)
                }
            }
            
            return .failure(apiError!)
        }
        
        return .success(())
    }
}

extension DataRequest {
    
    func responseAPIDecode<T: Decodable>(decoder: JSONDecoder = .default(), keyPath: String? = nil, completion: @escaping (DataResponse<Result<T>, AFError>, Tapptitude.Result<T>) -> Void) -> Self {
        let serializer: TTDecodableResponseSerializer<T> = TTDecodableResponseSerializer(keyPath: keyPath, decoder: decoder)
        return validate().response(responseSerializer: serializer) { (response) in
            if !self.responseWasCanceled(response) {
                switch response.result {
                case .success(let result): completion(response, result)
                case .failure(let error):  completion(response, .failure(error))
                }
            }
        }
    }
    
    public func responseAPI_JSON<T>(keyPath: String? = nil, _ completion: @escaping (DataResponse<T?, AFError>) -> Void) -> Self {
        let serializer = APIJsonSerializer<T>(keyPath: keyPath)
        return validate().response(responseSerializer: serializer) { response in
            if !self.responseWasCanceled(response) {
                completion(response.map { $0.value })
            }
        }
    }
    
    public func responseAPI(_ completion: @escaping (DataResponse<Void, AFError>, Tapptitude.Result<Void>) -> Void) -> Self {
        let errorSerializer = APIErrorResponseSerializer<Any>()
        return validate().response(responseSerializer: errorSerializer) { (response) in
            if !self.responseWasCanceled(response) {
                switch response.result {
                case .failure(let error): completion(response.map { $0.value }, .failure(error))
                case .success(let result): completion(response.map { $0.value }, result)
                }
            }
        }
    }
    
    func responseWasCanceled<T>(_ response: DataResponse<T, AFError>) -> Bool {
        var canceled = false
        if let error = response.error as NSError? {
            canceled = error.domain == NSURLErrorDomain && error.code == NSURLErrorCancelled
        }
        
        if self.task?.state == .canceling {
            canceled = true
        }
        
        return canceled
    }
}

import Tapptitude
extension Alamofire.Request: TTCancellable {
    public func cancelRequest() {
        self.cancel()
    }
}

extension Alamofire.DataResponse {
    /// helper method to show response data as string
    var dataAsString : String? {
        get {
            return data != nil ? String(data: self.data!, encoding: .utf8) : nil
        }
    }
}

extension AFResult where Success: Collection {
    public func map<NewValue>(as type: NewValue.Type) -> Tapptitude.Result<[NewValue]> {
        switch self {
        case .success(let value):
            return .success(value.map({$0 as! NewValue }))
        case .failure(let error):
            return .failure(error)
        }
    }
    
    public func map<NewValue>() -> Tapptitude.Result<[NewValue]> {
        switch self {
        case .success(let value):
            return .success(value.map({$0 as! NewValue }))
        case .failure(let error):
            return .failure(error)
        }
    }
}

extension DecodingError {
    
    var debugDescription:String {
        switch self {
        case .dataCorrupted(let context):
            return context.debugDescription
        case .keyNotFound(_, let context):
            return context.debugDescription
        case .typeMismatch(_, let context):
            return context.debugDescription
        case .valueNotFound(_, let context):
            return context.debugDescription
        default:
            return self.localizedDescription
        }
    }
}
