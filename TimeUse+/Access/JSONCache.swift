//
//  JSONCache.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import Foundation

/// Usage Ex:
/// - loading -- `lazy var creditCards: [CreditCard]? = JSONCache.creditCards.loadFromFile()`
/// - saving -- `JSONCache.creditCards.saveToFile(cards)`
enum JSONCache {
    
//    static var bookingHistory: CodableCaching<[Booking]> {
//        return userResource()
//    }
//
//    static var community: CodableCaching<[Post]> {
//        return resource()
//    }
    
    static var currentUser: CodableCaching<User> {
        return userResource()
    }
    
    static func clearAllSavedResource() {
        CodableCaching<Any>.deleteCachingDirectory()
    }
}

extension JSONCache {
    fileprivate static func userResourceID(function: String = #function) -> String {
        let id = Session.currentUserID ?? ""
        return function + "_" + id
    }
    
    fileprivate static func userResource<T>(function: String = #function) -> CodableCaching<T> {
        return CodableCaching(resourceID: userResourceID(function: function))
    }
    
    fileprivate static func resource<T>(function: String = #function) -> CodableCaching<T> {
        return CodableCaching(resourceID: function)
    }
}

