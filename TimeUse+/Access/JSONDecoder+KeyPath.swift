//
//  JSONDecoder+KeyPath.swift
//  Decodable
//
//  Created by Alexandru Tudose on 08/12/2017.
//  Copyright © 2017 Tapptitude. All rights reserved.
//

import Foundation

extension JSONDecoder {
    
    fileprivate static let keyPaths: CodingUserInfoKey = CodingUserInfoKey(rawValue: "keyPath")!
    
    open func decode<T>(_ type: T.Type, from data: Data, keyPath: String, separator: Character = ".") throws -> T where T : Decodable {
        self.userInfo[JSONDecoder.keyPaths] = keyPath.split(separator: separator).map({ String($0) })
        return try decode(ProxyModel<T>.self, from: data).object
    }
    
    open func decode<T>(_ type: T.Type, from dict: [String:Any], keyPath: String? = nil, separator: Character = ".") throws -> T where T : Decodable {
        let data = try JSONSerialization.data(withJSONObject: dict)
        if let keyPath = keyPath {
            return try self.decode(type, from: data, keyPath: keyPath, separator: separator)
        } else {
            return try self.decode(type, from: data)
        }
    }
    
    open func decode<T>(_ type: T.Type, from array: [[String:Any]], keyPath: String? = nil, separator: Character = ".") throws -> T where T : Decodable {
        let data = try JSONSerialization.data(withJSONObject: array)
        if let keyPath = keyPath {
            return try self.decode(type, from: data, keyPath: keyPath, separator: separator)
        } else {
            return try self.decode(type, from: data)
        }
    }
}

extension JSONDecoder {
    public static func `default`(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") -> JSONDecoder {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = dateFormat
        dateFormater.locale = Locale(identifier: "en_US_POSIX")
        dateFormater.timeZone = TimeZone(secondsFromGMT: 0)
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormater)
        
        return decoder
    }
}

extension JSONDecoder {
    struct ProxyModel<T: Decodable>: Decodable {
        var object: T
        
        struct Key: CodingKey {
            let stringValue: String
            let intValue: Int? = nil
            
            init?(stringValue: String) {
                self.stringValue = stringValue
            }
            
            init?(intValue: Int) {
                return nil
            }
        }
        
        public init(from decoder: Decoder) throws {
            let stringKeyPaths = decoder.userInfo[JSONDecoder.keyPaths] as! [String]
            var keyPaths = stringKeyPaths.map({ Key(stringValue: $0)! })
            var container = try! decoder.container(keyedBy: Key.self)
            var key = keyPaths.removeFirst()
            for newKey in keyPaths {
                container = try container.nestedContainer(keyedBy: Key.self, forKey: key)
                key = newKey
            }
            
            object = try container.decode(T.self, forKey: key)
        }
    }
}

extension Encodable {
    
    func asDictionary(dateStrategy: JSONEncoder.DateEncodingStrategy = .secondsSince1970) throws -> [String: Any] {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = dateStrategy
        
        let data = try encoder.encode(self)
        let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        guard let dict = dictionary as? [String: Any] else {
            throw NSError.init(domain: "This object cannot be casted to a dictionary, probably an array", code: -1, userInfo: nil)
        }
        return dict
    }
    
    func asArrayOfDict(dateStrategy: JSONEncoder.DateEncodingStrategy = .secondsSince1970) throws -> [[String:Any]] {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = dateStrategy
        
        let data = try encoder.encode(self)
        let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        guard let array = dictionary as? [[String: Any]] else {
            throw NSError.init(domain: "This object cannot be casted to a an array of dictionaries, probably a dictionary", code: -1, userInfo: nil)
        }
        return array
    }
    
    func asDictionaryOpt(dateStrategy: JSONEncoder.DateEncodingStrategy = .secondsSince1970) -> [String: Any]? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = dateStrategy
        
        guard let data = try? encoder.encode(self) else { return nil }
        guard let dictionary = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else { return nil }
        return dictionary
    }
    
    func asJsonString(dateStrategy: JSONEncoder.DateEncodingStrategy = .secondsSince1970) -> String? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .millisecondsSince1970
        
        if let data = try? encoder.encode(self) {
            let json = String(data: data, encoding: .utf8)
            return json
        }
        return nil
    }
}
