//
//  MainDelegate.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation
import Firebase
import Tapptitude
import GoogleMaps

class MainDelegate: NSObject {
    static let instance = MainDelegate()
    private var motionTagHelper: MotionTagHelper!

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        _ = MotionTagHelper.shared
        GMSServices.provideAPIKey(Constants.GoogleAPIKey)
        FirebaseApp.configure()
        Messaging.messaging().delegate = PushNotificationHandler.shared
        UNUserNotificationCenter.current().delegate = PushNotificationHandler.shared
        registerSessionClosedNotification()
        UIViewController.TTErrorDisplayClass = ErrorDisplay.self
        
        if #available(iOS 13.0, *) {
        } else {
            window = UIWindow()
            start(window: window!)
        }
        
        return true
    }
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        start(window: window!)
    }
    
    private func start(window: UIWindow) {
        var controller: UIViewController!
        if Session.isValidSession() {
            controller = HomeViewController.instantiateFromHomeStoryboard()
        } else {
            controller = WelcomeViewController.instantiateFromAuthentificationStoryboard()
        }
        let navigation = UINavigationController(rootViewController: controller)

        window.rootViewController = navigation
        window.makeKeyAndVisible()
    }
    
    func registerSessionClosedNotification () {
        Notifications.sessionClosed.addObserver(self) { (self, error) in
            guard let window = self.window else {
                assert(false, "Unexpected case")
                return
            }
            self.start(window: window)
            self.window?.rootViewController?.checkAndShow(error: error)
        }
    }
    
    // MARK: - Application
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotificationHandler.shared.apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PushNotificationHandler.shared.application(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
    }
}
