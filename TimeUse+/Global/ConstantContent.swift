//
//  ConstantTexts.swift
//  Webcar
//
//  Created by Efraim Budusan on 10/18/17.
//  Copyright © 2017 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

extension Constants {
    class Fonts {
        
        enum condensedRoboto {
            case condensedBold
            case condensedRegular
            case condensedMedium
            case condensedLight
            
            func with(size:CGFloat) -> UIFont {
                switch self {
                case .condensedBold:
                    return UIFont(name: "RobotoCondensed-Bold", size: size)!
                case .condensedLight:
                    return UIFont(name: "RobotoCondensed-Light", size: size)!
                case .condensedMedium:
                    return UIFont(name: "RobotoCondensed-Medium", size: size)!
                case .condensedRegular:
                    return UIFont(name: "RobotoCondensed-Regular", size: size)!
                }
            }
        }
        
        enum roboto {
            case bold
            case medium
            case regular
            case light
            
            func with(size:CGFloat) -> UIFont {
                switch self {
                case .bold:
                    return UIFont(name: "Roboto-Bold", size: size)!
                case .medium:
                    return UIFont(name: "Roboto-Medium", size: size)!
                case .regular:
                    return UIFont(name: "Roboto-Regular", size: size)!
                case .light:
                    return UIFont(name: "Roboto-Light", size: size)!
                }
            }
        }
    }
}

extension Constants {
    
    class Values {
        static let toxicContentBarrier:Float = 0.33
    }
    
}


extension Constants {
    
    class Colors {
        
        enum main {
            case blue, red, lightBlue, dark, darkGrey, lightGrey, ultraLightGrey, topaz, work, walk
            case bicycle, passenger, bus, tram, train, plane, boat, metro, greenExpenses, yellowExpenses
            case purpleExpenses, car, bicycleSharing, ebicycle, ebicycleSharing, eScooter, taxiUber, carSharing
            case motorcycle, regionalTrain, coach, cableCar, ski
            
            
            var value:UIColor {
                switch self {
                case .lightBlue:
                    return UIColor(named: "lightBlue")! // #E5F3FD
                case .blue:
                    return UIColor(named: "blue")! // #028BEF
                case .darkGrey:
                    return UIColor(named: "darkGrey")! // #9DA8B0
                case .lightGrey:
                    return UIColor(named: "lightGrey")! // #DCE1E5
                case .red:
                    return UIColor(named: "red")! // #F74E6B
                case .dark:
                    return UIColor(named: "dark")! // #181818
                case .ultraLightGrey:
                    return UIColor(named: "ultraLightGrey")! // #F4F6F7
                case .topaz:
                    return UIColor(named: "topaz")! // #2EBDBC
                case .work:
                    return UIColor(named: "work")! // #D9AF7F
                case .walk:
                    return UIColor(named: "walk")! // #D2D220
                case .bicycle:
                    return UIColor(named: "bicycle")! // #68B5CF
                case .passenger:
                    return UIColor(named: "passenger")! // #e6bcbc
                case .bus:
                    return UIColor(named: "bus")! // #4BE4D0
                case .tram:
                    return UIColor(named: "tram")! // #f7b18e
                case .train:
                    return UIColor(named: "train")! // #EAB201
                case .plane:
                    return UIColor(named: "plane")! // #D581DE
                case .boat:
                    return UIColor(named: "boat")! // #A299F9
                case .greenExpenses:
                    return UIColor(named: "greenExpenses")! // #4BE4D0
                case .yellowExpenses:
                    return UIColor(named: "yellowExpenses")! // #FFBD05
                case .purpleExpenses:
                    return UIColor(named: "purpleExpenses")! // #9896FE
                case .metro:
                    return UIColor(named: "metro")! // #FC8346
                case .car:
                    return UIColor(named: "car")! // #E58686
                case .bicycleSharing:
                    return UIColor(named: "bicycle_sharing")! // #94d4ea
                case .ebicycle:
                    return UIColor(named: "ebike")! // #34a853
                case .ebicycleSharing:
                    return UIColor(named: "ebike_sharing")! // #24c950
                case .eScooter:
                    return UIColor(named: "escooter")! // #8dafed
                case .motorcycle:
                    return UIColor(named: "motorcycle")! // #5884D7
                case .taxiUber:
                    return UIColor(named: "taxi_uber")! //#d3334d
                case .carSharing:
                    return UIColor(named: "car_sharing")! // #F74E6B
                case .regionalTrain:
                    return UIColor(named: "regional_train")! // #ffd966
                case .coach:
                    return UIColor(named: "coach")! // #f19756
                case .cableCar:
                    return UIColor(named: "cable_car")! // #b7e1cd
                case .ski:
                    return UIColor(named: "ski")! // #4285f4
                }
            }
        }
    }
    

}
