//
//  TextStyles.swift
//  TimeUse
//
//  Created by Robert Havrisciuc on 14.12.2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

struct TextStyles {
    
    struct Keys {
        static let value = "%#@value@"
    }
    
    static func attributes(_ font: UIFont, color: UIColor) -> [NSAttributedString.Key: Any] {
        let attributes:[NSAttributedString.Key: Any] = [ NSAttributedString.Key.font: font,
                                                        NSAttributedString.Key.foregroundColor: color]
        return attributes
    }
    
    static func fontAttributes(_ font: UIFont) -> [NSAttributedString.Key: Any] {
        let attributes:[NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font]
        return attributes
    }
    
    static func colorAttributes(_ color: UIColor) -> [NSAttributedString.Key: Any] {
        let attributes:[NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: color]
        return attributes
    }

    static func underlineStyleAttributes(_ style: NSUnderlineStyle) -> [NSAttributedString.Key: Any] {
        let attributes:[NSAttributedString.Key: Any] = [NSAttributedString.Key.underlineStyle: style.rawValue,
                                                        NSAttributedString.Key.baselineOffset: 0]
        return attributes
    }
}
