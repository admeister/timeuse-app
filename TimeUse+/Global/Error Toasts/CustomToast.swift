//
//  CustomToast.swift
//  Opinon
//
//  Created by Efraim Budusan on 2/26/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import SwiftMessages

class CustomToastView:MessageView {
    
    enum ConfigType {
        case success
        case error
        case alert
        
        var backgroundColor: UIColor {
            switch self {
            case .success:
                return Constants.Colors.main.topaz.value
            case .error:
                return Constants.Colors.main.red.value
            default:
                return .clear
            }
        }
        
    }
    
    @IBOutlet weak var topSafeAreaConstraint: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(for type:ConfigType) {
        self.container.backgroundColor = type.backgroundColor
        self.tapHandler = { _ in SwiftMessages.hide() }
    }
    

}
