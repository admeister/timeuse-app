//
//  Texts.swift
//  TimeUse
//
//  Created by Robert Havrisciuc on 14.12.2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation

struct Texts {
    enum Button {
        case alreadyHaveAnAccount, resetPassword, addDetails, edit
        
        var text: String {
            switch self {
            case .alreadyHaveAnAccount:
                return NSLocalizedString("already_have_an_account", value: "already_have_an_account", comment: "")
            case .resetPassword:
                return NSLocalizedString("reset_password", value: "reset_password", comment: "")
            case .edit:
                return NSLocalizedString("edit", value: "edit", comment: "")
            case .addDetails:
                return NSLocalizedString("add_details", value: "add_details", comment: "")
            }
        }
    }
    
    enum Other {
        case registrationCode, password, newPassword, myEvents, settings, yes, areYouSureLogout, cancel, oldPassword
        case termsAndConditions, privacyPolicy, appVersion, changePassword, logout, dataTransferOverWifi, dataTransferOverWifiDescription
        case delete, caution, deleteEventDescription, unvalidatedEvents(Int), areYouSure, noActivityInformation, faq

        var text: String {
            switch self {
            case .registrationCode:
                return NSLocalizedString("registration_code", value: "registration_code", comment: "")
            case .password:
                return NSLocalizedString("password", value: "password", comment: "")
            case .newPassword:
                return NSLocalizedString("new_password", value: "new_password", comment: "")
            case .myEvents:
                return NSLocalizedString("my_events", value: "my_events", comment: "")
            case .settings:
                return NSLocalizedString("settings", value: "settings", comment: "")
            case .termsAndConditions:
                return NSLocalizedString("terms_and_conditions", value: "terms_and_conditions", comment: "")
            case .privacyPolicy:
                return NSLocalizedString("privacy_policy", value: "privacy_policy", comment: "")
            case .appVersion:
                return NSLocalizedString("app_version", value: "app_version", comment: "")
            case .changePassword:
                return NSLocalizedString("change_password", value: "change_password", comment: "")
            case .dataTransferOverWifi:
                return NSLocalizedString("data_transfer_over_Wifi", value: "data_transfer_over_Wifi", comment: "")
            case .dataTransferOverWifiDescription:
                return NSLocalizedString("data_transfer_over_Wifi_description", value: "data_transfer_over_Wifi_description", comment: "")
            case .logout:
                return NSLocalizedString("logout", value: "logout", comment: "")
            case .yes:
                return NSLocalizedString("yes", value: "yes", comment: "")
            case .cancel:
                return NSLocalizedString("cancel", value: "cancel", comment: "")
            case .areYouSureLogout:
                return NSLocalizedString("are_you_sure_logout", value: "are_you_sure_logout", comment: "")
            case .oldPassword:
                return NSLocalizedString("old_password", value: "old_password", comment: "")
            case .delete:
                return NSLocalizedString("delete", value: "delete", comment: "")
            case .caution:
                return NSLocalizedString("caution", value: "caution", comment: "")
            case .deleteEventDescription:
                return NSLocalizedString("delete_event_description", value: "delete_event_description", comment: "")
            case .unvalidatedEvents(let count):
                let format = NSLocalizedString("unvalidated_events", comment: "")
                return String(format: format, count, count)
            case .areYouSure:
                return NSLocalizedString("are_you_sure", value: "are_you_sure", comment: "")
            case .noActivityInformation:
                return NSLocalizedString("no_activity_information", value: "no_activity_information", comment: "")
            case .faq:
                return NSLocalizedString("faq", value: "faq", comment: "")
            }
        }
    }
    
    enum Statistics {
        case moving, stays, expenses, movingEvents, totalDistance, totalTime, averageSpeed
        
        var text: String {
            switch self {
            case .moving:
                return NSLocalizedString("moving", value: "moving", comment: "")
            case .stays:
                return NSLocalizedString("stays", value: "stays", comment: "")
            case .expenses:
                return NSLocalizedString("expenses", value: "expenses", comment: "")
            case .movingEvents:
                return NSLocalizedString("moving_event", value: "moving_event", comment: "")
            case .totalDistance:
                return NSLocalizedString("total_distance", value: "total_distance", comment: "")
            case .totalTime:
                return NSLocalizedString("moving_time", value: "moving_time", comment: "")
            case .averageSpeed:
                return NSLocalizedString("average_speed", value: "average_speed", comment: "")
            }
        }
    }
    
    enum Section {
        case stay, track, time, money, participants, expenditure
        case tracking, info, help
        
        var text: String {
            switch self {
            case .time:
                return NSLocalizedString("activity_duration", value: "activity_duration", comment: "")
            case .money:
                return NSLocalizedString("activity_budget", value: "activity_budget", comment: "")
            case .participants:
                return NSLocalizedString("activity_participants", value: "activity_participants", comment: "")
            case .expenditure:
                return NSLocalizedString("activity_expenditure", value: "activity_expenditure", comment: "")
            case .stay:
                return NSLocalizedString("stay", value: "stay", comment: "")
            case .track:
                return NSLocalizedString("track", value: "track", comment: "")
            case .tracking:
                return NSLocalizedString("tracking", value: "tracking", comment: "")
            case .help:
                return NSLocalizedString("help", value: "help", comment: "")
            case .info:
                return NSLocalizedString("info", value: "info", comment: "")
            }
        }
    }
    
    enum Activity {
        case alone, friends, householdMembers, myself, household, dailyNeeds, longTermNeeds, required, notRequired
        
        var text: String {
            switch self {
            case .alone:
                return NSLocalizedString("alone", value: "alone", comment: "")
            case .friends:
                return NSLocalizedString("friends", value: "friends", comment: "")
            case .household:
                return NSLocalizedString("household", value: "household", comment: "")
            case .householdMembers:
                return NSLocalizedString("household_members", value: "household_members", comment: "")
            case .myself:
                return NSLocalizedString("myself", value: "myself", comment: "")
            case .dailyNeeds:
                return NSLocalizedString("daily_needs", value: "daily_needs", comment: "")
            case .longTermNeeds:
                return NSLocalizedString("long_term_needs", value: "long_term_needs", comment: "")
            case .required:
                return NSLocalizedString("required", value: "required", comment: "")
            case .notRequired:
                return NSLocalizedString("not_required", value: "not_required", comment: "")
            }
        }
    }
    
    enum Event {
        case other, home, work, walk, bicycle, car, passenger, bus, tram, airplane, train, boat, subway, deleted, untracked
        case bicycleSharing, eBicycle, eBicycleSharing, carSharing, taxiUber, motorcycle, eScooter, cableCar
        case regionalTrain, coach, ski
        
        var text: String {
            switch self {
            case .other:
                return NSLocalizedString("event_other", value: "event_other", comment: "")
            case .home:
                return NSLocalizedString("event_home", value: "event_home", comment: "")
            case .work:
                return NSLocalizedString("event_work", value: "event_work", comment: "")
            case .walk:
                return NSLocalizedString("event_walk", value: "event_walk", comment: "")
            case .bicycle:
                return NSLocalizedString("event_bicycle", value: "event_bicycle", comment: "")
            case .car:
                return NSLocalizedString("event_car", value: "event_car", comment: "")
            case .passenger:
                return NSLocalizedString("event_passenger", value: "event_passenger", comment: "")
            case .bus:
                return NSLocalizedString("event_bus", value: "event_bus", comment: "")
            case .tram:
                return NSLocalizedString("event_tram", value: "event_tram", comment: "")
            case .airplane:
                return NSLocalizedString("event_airplane", value: "event_airplane", comment: "")
            case .train:
                return NSLocalizedString("event_train", value: "event_train", comment: "")
            case .boat:
                return NSLocalizedString("event_boat", value: "event_boat", comment: "")
            case .subway:
                return NSLocalizedString("event_subway", value: "event_subway", comment: "")
            case .deleted:
                return NSLocalizedString("event_deleted", value: "event_deleted", comment: "")
            case .untracked:
                return NSLocalizedString("event_untracked", value: "event_untracked", comment: "")
            case .bicycleSharing:
                return NSLocalizedString("event_bicycle_sharing", value: "event_bicycle_sharing", comment: "")
            case .eBicycle:
                return NSLocalizedString("event_ebicycle", value: "event_ebicycle", comment: "")
            case .eBicycleSharing:
                return NSLocalizedString("event_ebicycle_sharing", value: "event_ebicycle_sharing", comment: "")
            case .carSharing:
                return NSLocalizedString("event_car_sharing", value: "event_car_sharing", comment: "")
            case .taxiUber:
                return NSLocalizedString("event_taxi_uber", value: "event_taxi_uber", comment: "")
            case .motorcycle:
                return NSLocalizedString("event_motorcycle", value: "event_motorcycle", comment: "")
            case .eScooter:
                return NSLocalizedString("event_scooter", value: "event_scooter", comment: "")
            case .cableCar:
                return NSLocalizedString("event_cable_car", value: "event_cable_car", comment: "")
            case .regionalTrain:
                return NSLocalizedString("event_regional_train", value: "event_regional_train", comment: "")
            case .coach:
                return NSLocalizedString("event_coach", value: "event_coach", comment: "")
            case .ski:
                return NSLocalizedString("event_ski", value: "event_ski", comment: "")
            }
        }
    }
    
    enum Permission {
        case travelAnalysis, travelAnalysisDescription
        
        var text: String {
            switch self {
            case .travelAnalysisDescription:
                return NSLocalizedString("limited_travel_analysis_description", value: "limited_travel_analysis_description", comment: "")
            case .travelAnalysis:
                return NSLocalizedString("limited_travel_analysis", value: "limited_travel_analysis", comment: "")
            }
        }
    }
    
    enum Toast {
        case incorrectEmail, incorrectPassword, passwordSuccessfullyChanged, passwordSuccessfullyReset, durationLimitReached
        case noNetworkConnection, success, error, errorUnknown
        
        var text: String {
            switch self {
            case .incorrectEmail:
                return NSLocalizedString("invalid_email", value: "invalid_email", comment: "")
            case .incorrectPassword:
                return NSLocalizedString("incorrect_password", value: "incorrect_password", comment: "")
            case .passwordSuccessfullyChanged:
                return NSLocalizedString("password_successfully_changed", value: "password_successfully_changed", comment: "")
            case .passwordSuccessfullyReset:
                return NSLocalizedString("password_successfully_reset", value: "password_successfully_reset", comment: "")
            case .durationLimitReached:
                return NSLocalizedString("duration_limit_reached", value: "duration_limit_reached", comment: "")
            case .noNetworkConnection:
                return NSLocalizedString("no_network_connection", value: "no_network_connection", comment: "")
            case .success:
                return NSLocalizedString("success", value: "success", comment: "")
            case .error:
                return NSLocalizedString("error", value: "error", comment: "")
            case .errorUnknown:
                return NSLocalizedString("error_unknown", value: "error_unknown", comment: "")
            }
        }
    }
}
