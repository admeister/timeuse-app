//
//  Activity.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 15/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

enum ActivityType: String, Codable {
    case specific = "Specific"
    case general = "General"
}

class EventActivity: Codable, Equatable {
    
    var id: String
    var name: String
    var eventType: EventType
    var type: ActivityType
    var displayOn: [EventConfig]
    var icon: String
    var attribute: Attribute?
    var isSelected: Bool
    var settings: ActivitySettings

    var isFirst: Bool = false
    var isLast: Bool = false
    
    static func == (lhs: EventActivity, rhs: EventActivity) -> Bool {
        return lhs.id == rhs.id
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id", attribute = "attributes", isSelected = "selected"
        case name, eventType, type, displayOn, icon, settings
    }
}

class EventName: Codable {
    var eventConfig: EventConfig
    var isSelected: Bool
    
    var isLast: Bool = false
    var isFirst: Bool = false
    
    var isStayEvent: Bool {
        return eventConfig == .other || eventConfig == .home || eventConfig == .work
    }
    
    init(eventConfig: EventConfig, isSelected: Bool = false) {
        self.eventConfig = eventConfig
        self.isSelected = isSelected
    }
    
    enum CodingKeys: String, CodingKey {
        case isSelected = "selected", eventConfig = "name"
    }
}

class Attribute: Codable {
    var duration: Int!
    var budget: Int!
    var partners: String!
    var expenditures: [String] = []
    
    let settings: ActivitySettings?
    
    init(settings: ActivitySettings) {
        self.settings = settings
    }
    
    var hasAttributes: Bool {
        let isDurationActive = settings?.isDurationActive ?? false
        let isBudgetActive = settings?.isBudgetActive ?? false
        let isPartnersActive = settings?.isPartnersActive ?? false
        
        if isDurationActive && !isPartnersActive && !isBudgetActive {
            return duration != nil
        } else if isDurationActive && isPartnersActive && !isBudgetActive {
            return duration != nil && partners != nil
        } else if isDurationActive && !isPartnersActive && isBudgetActive {
            return duration != nil && budget != nil
        } else if isDurationActive && isPartnersActive && isBudgetActive {
            return duration != nil && partners != nil && budget != nil
        } else if !isDurationActive && isPartnersActive && !isBudgetActive {
            return partners != nil
        } else if !isDurationActive && isPartnersActive && isBudgetActive {
            return partners != nil && budget != nil
        } else if !isDurationActive && !isPartnersActive && isBudgetActive {
            return budget != nil
        }
        return true
    }
    
    enum CodingKeys: String, CodingKey {
        case duration, budget, partners, expenditures, settings
    }
}

class ActivitySettings: Codable {
    var isDurationActive: Bool
    var isBudgetActive: Bool
    var isPartnersActive: Bool
    var isExpendituresActive: Bool
    
    enum CodingKeys: String, CodingKey {
        case isDurationActive = "duration"
        case isBudgetActive = "budget"
        case isPartnersActive = "partners"
        case isExpendituresActive = "expenditures"
    }
}
