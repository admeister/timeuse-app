//
//  Statistics.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

enum MovingStatisticsOrderType: String {
    case totalEvents, totalDistance, totalTime, averageSpeed
}

func movingTime(for totalTime: Int) -> String {
    guard totalTime > 0 else {
        return "0h"
    }
    let hour = 60
    guard totalTime >= hour else {
        return "\(totalTime)min"
    }
    if totalTime % hour == 0 {
        return "\(totalTime / hour)h"
    } else {
        return "\(totalTime / hour)h \(totalTime % hour)min"
    }
}

class MovingSummary: Codable {
    var totalEvents: Int
    var totalDistance: Int
    var totalTime: Int
    var averageSpeed: Int
    
    var totalDistanceString: String {
        if totalDistance < 1000 {
            return "\(totalDistance) m"
        } else {
            return String(format: "%.1f km", Double(totalDistance)/1000)
        }
    }
    
    var movingTimeString: String {
        movingTime(for: totalTime)
    }
    
    var averageSpeedString: String {
        return "\(averageSpeed) km/h"
    }
    
    enum CodingKeys: String, CodingKey {
        case totalEvents, totalDistance, totalTime, averageSpeed
    }
}

class TopEvent: Codable {
    var name: EventConfig
    var meters: Int
    var emissions: Double
    var eventsCount: Int
    var duration: Int
    var averageSpeed: Int
    
    var distance: String {
        let value = numberFormatter(maxDigits: 1, value: Double(meters))
        if meters < 1000 {
            return "\(value) m"
        } else {
            return "\(value) km"
        }
    }
    
    var time: String {
        movingTime(for: duration)
    }
    
    var emissionsString: String {
        let value = numberFormatter(maxDigits: 2, value: emissions)
        if emissions < 1000 {
            return "\(value) g"
        } else {
            return "\(value) kg"
        }
    }
    
    var averageSpeedString: String {
        return "\(averageSpeed) km/h"
    }
    
    func numberFormatter(maxDigits: Int, value: Double) -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = maxDigits
        formatter.numberStyle = .decimal
        
        return formatter.string(from: NSNumber(value: value/1000)) ?? "\(value)"
    }
    
    enum CodingKeys: String, CodingKey {
        case name, meters, emissions, duration, averageSpeed
        case eventsCount = "count"
    }
}

class MovingStatistics: Codable {
    var summary: MovingSummary
    var topEvents: [TopEvent]
    
    enum CodingKeys: String, CodingKey {
        case summary, topEvents
    }
}

func stayedHours(from min: Int) -> String {
    let hour = 60
    let minInterval = 15
    let midInterval = 45
    guard min > 0 else {
        return "0h"
    }
    guard min >= 45 else {
        return "0.5h"
    }
    guard min >= hour else {
        return "1h"
    }
    if min % hour < minInterval {
        return "\(min / hour)h"
    } else if min % hour >= minInterval && min % hour < midInterval {
        return "\(min / hour).5h"
    } else {
        return "\((min / hour)+1)h"
    }
}

class StayingSummary: Codable {
    var timeSpentAtHome: Int
    var timeSpentAtWork: Int
    var timeSpentAtOther: Int
    var thisWeekTimeSpentAtHome: Double
    var thisWeekTimeSpentAtWork: Double
    var thisWeekTimeSpentAtOther: Double
    var previousWeekTimeSpentAtHome: Double
    var previousWeekTimeSpentAtWork: Double
    var previousWeekTimeSpentAtOther: Double
    
    var timeAtHomeString: String {
        return stayedHours(from: timeSpentAtHome)
    }
    
    var timeAtWorkString: String {
        return stayedHours(from: timeSpentAtWork)
    }
    
    var timeAtOtherString: String {
        return stayedHours(from: timeSpentAtOther)
    }
    
    var stayTime: Int {
        return timeSpentAtHome + timeSpentAtWork + timeSpentAtOther
    }
    
    var totalTime: String {
        return String(stayedHours(from: stayTime).dropLast())
    }
    
    enum CodingKeys: String, CodingKey {
        case timeSpentAtHome, timeSpentAtWork, timeSpentAtOther
        case thisWeekTimeSpentAtHome, thisWeekTimeSpentAtWork, thisWeekTimeSpentAtOther
        case previousWeekTimeSpentAtHome, previousWeekTimeSpentAtWork, previousWeekTimeSpentAtOther
    }
}

class TopActivity: Codable {
    var name: String
    var icon: String
    var value: Double
    var percent: Double
    
    var time: String {
        return stayedHours(from: Int(value))
    }
    
    var color: UIColor!
    var isLast: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case name, icon, value, percent
    }
}

class StayingStatistics: Codable {
    var summary: StayingSummary
    var topActivities: [TopActivity]
    
    let colors: [UIColor] = [Constants.Colors.main.greenExpenses.value, Constants.Colors.main.red.value,
                             Constants.Colors.main.blue.value, Constants.Colors.main.yellowExpenses.value,
                             Constants.Colors.main.purpleExpenses.value]
    
    var activities: [TopActivity] {
        guard topActivities.count > 0 else {
            return topActivities
        }
        for index in 0..<topActivities.count {
            topActivities[index].color = colors[index]
        }
        return topActivities
     }
    
    var totalTimeString: String {
        return String(stayedHours(from: Int(totalTime)).dropLast())
    }
    
    var totalTime: Double {
        return activities.map{$0.value}.reduce(0, +)
    }
    
    enum CodingKeys: String, CodingKey {
        case summary, topActivities
    }
}

class ExpensesStatistics: Codable {
    var topActivities: [TopActivity]
    
    let colors: [UIColor] = [Constants.Colors.main.greenExpenses.value, Constants.Colors.main.red.value,
                             Constants.Colors.main.blue.value, Constants.Colors.main.yellowExpenses.value,
                             Constants.Colors.main.purpleExpenses.value]
    
    var activities: [TopActivity] {
       guard topActivities.count > 0 else {
           return topActivities
       }
       for index in 0..<topActivities.count {
           topActivities[index].color = colors[index]
       }
       return topActivities
    }
    
    var totalBudget: Int {
        return activities.map{Int($0.value)}.reduce(0, +)
    }
    
    enum CodingKeys: String, CodingKey {
        case topActivities
    }
}

class Statistics: Codable {
    var moving: MovingStatistics
    var staying: StayingStatistics
    var expenses: ExpensesStatistics
    
    enum CodingKeys: String, CodingKey {
        case moving, staying, expenses
    }
}
