//
//  Event.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 12/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

struct LocationCoordinates: Codable {
    var latitude: Double
    var longitude: Double
}

struct Geometry: Codable {
    var coordinates: [[Double]]
    var type: GeometryType?
    
    enum GeometryType: String, Codable {
        case point = "Point"
        case track = "LineString"
    }
    
    init() {
        self.coordinates = []
    }
    
    var locationCoordinates: [LocationCoordinates] {
        var locations: [LocationCoordinates] = []
        guard let type = type else {
            return locations
        }
        switch type {
        case .track:
            coordinates.forEach { coordinate in
                locations.append(LocationCoordinates(latitude: coordinate[1], longitude: coordinate[0]))
            }
        case .point:
            locations.append(LocationCoordinates(latitude: coordinates.last![0], longitude: coordinates.first![0]))
        }
        return locations
    }
    
    enum CodingKeys: String, CodingKey {
        case coordinates, type
    }
}

enum EventType: String, Codable {
    case track = "Track"
    case stay = "Stay"
    case untracked = "Untracked"
}

enum EventStatus: String, Codable {
    case complete
    case incomplete
}

enum EventConfig: String, Codable, CaseIterable {
    case home, work, other
    case car, passenger, carSharing = "carsharing", taxiUber = "taxi_uber", motorcycle = "motorbike_scooter", walk, eScooter = "kick_scooter"
    case bicycle, bicycleSharing = "bikesharing", eBicycle = "ebicycle", eBicycleSharing = "ebikesharing"
    case bus, regionalTrain = "regional_train", subway, train, tram, airplane, boat, coach, cableCar = "cable_car", ski, otherTrack = "other_track"
    case deleted, untracked
    
    var title: String {
        switch self {
        case .other, .otherTrack:
            return Texts.Event.other.text
        case .home:
            return Texts.Event.home.text
        case .work:
            return Texts.Event.work.text
        case .walk:
            return Texts.Event.walk.text
        case .bicycle:
            return Texts.Event.bicycle.text
        case .car:
            return Texts.Event.car.text
        case .passenger:
            return Texts.Event.passenger.text
        case .bus:
            return Texts.Event.bus.text
        case .tram:
            return Texts.Event.tram.text
        case .airplane:
            return Texts.Event.airplane.text
        case .train:
            return Texts.Event.train.text
        case .boat:
            return Texts.Event.boat.text
        case .subway:
            return Texts.Event.subway.text
        case .deleted:
            return Texts.Event.deleted.text
        case .untracked:
            return Texts.Event.untracked.text
        case .bicycleSharing:
            return Texts.Event.bicycleSharing.text
        case .carSharing:
            return Texts.Event.carSharing.text
        case .taxiUber:
            return Texts.Event.taxiUber.text
        case .motorcycle:
            return Texts.Event.motorcycle.text
        case .eScooter:
            return Texts.Event.eScooter.text
        case .eBicycle:
            return Texts.Event.eBicycle.text
        case .eBicycleSharing:
            return Texts.Event.eBicycleSharing.text
        case .cableCar:
            return Texts.Event.cableCar.text
        case .regionalTrain:
            return Texts.Event.regionalTrain.text
        case .coach:
            return Texts.Event.coach.text
        case .ski:
            return Texts.Event.ski.text
        }
    }
    
    var titleColor: UIColor {
        switch self {
        case .deleted:
            return Constants.Colors.main.lightGrey.value
        case .untracked:
            return Constants.Colors.main.red.value
        default:
            return Constants.Colors.main.dark.value
        }
    }
    
    var dateColor: UIColor {
        switch self {
        case .deleted:
            return Constants.Colors.main.lightGrey.value
        default:
            return Constants.Colors.main.darkGrey.value
        }
    }
    
    var color: UIColor {
        switch self {
        case .other:
            return Constants.Colors.main.blue.value
        case .work:
            return Constants.Colors.main.work.value
        case .walk:
            return Constants.Colors.main.walk.value
        case .bicycle:
            return Constants.Colors.main.bicycle.value
        case .passenger:
            return Constants.Colors.main.passenger.value
        case .bus:
            return Constants.Colors.main.bus.value
        case .tram:
            return Constants.Colors.main.tram.value
        case .train:
            return Constants.Colors.main.train.value
        case .airplane:
            return Constants.Colors.main.plane.value
        case .boat:
            return Constants.Colors.main.boat.value
        case .home:
            return Constants.Colors.main.topaz.value
        case .car:
            return Constants.Colors.main.car.value
        case .subway:
            return Constants.Colors.main.metro.value
        case .deleted:
            return .clear
        case .untracked:
            return Constants.Colors.main.red.value
        case .bicycleSharing:
            return Constants.Colors.main.bicycleSharing.value
        case .eBicycle:
            return Constants.Colors.main.ebicycle.value
        case .eBicycleSharing:
            return Constants.Colors.main.ebicycleSharing.value
        case .carSharing:
            return Constants.Colors.main.carSharing.value
        case .taxiUber:
            return Constants.Colors.main.taxiUber.value
        case .motorcycle:
            return Constants.Colors.main.motorcycle.value
        case .eScooter:
            return Constants.Colors.main.eScooter.value
        case .cableCar:
            return Constants.Colors.main.cableCar.value
        case .regionalTrain:
            return Constants.Colors.main.regionalTrain.value
        case .coach:
            return Constants.Colors.main.coach.value
        case .ski:
            return Constants.Colors.main.ski.value
        case .otherTrack:
            return Constants.Colors.main.darkGrey.value
        }
    }
    
    var icon: UIImage {
        switch self {
        case .other:
            return #imageLiteral(resourceName: "other_stay_icon")
        case .home:
            return #imageLiteral(resourceName: "atHome_icon")
        case .work:
            return #imageLiteral(resourceName: "atWork_icon")
        case .walk:
            return #imageLiteral(resourceName: "walk_icon")
        case .bicycle:
            return #imageLiteral(resourceName: "bicycle_icon")
        case .car:
            return #imageLiteral(resourceName: "car_icon")
        case .passenger:
            return #imageLiteral(resourceName: "passenger_icon")
        case .bus:
            return #imageLiteral(resourceName: "bus_icon")
        case .tram:
            return #imageLiteral(resourceName: "tram_icon")
        case .airplane:
            return #imageLiteral(resourceName: "plane_icon")
        case .train:
            return #imageLiteral(resourceName: "train_icon")
        case .boat:
            return #imageLiteral(resourceName: "boat_icon")
        case .subway:
            return #imageLiteral(resourceName: "metro_icon")
        case .deleted:
            return #imageLiteral(resourceName: "deleted_icon")
        case .untracked:
            return #imageLiteral(resourceName: "alert_small_icon")
        case .bicycleSharing:
            return #imageLiteral(resourceName: "bike_sharing_icon")
        case .eBicycle:
            return #imageLiteral(resourceName: "ebike_icon")
        case .eBicycleSharing:
            return #imageLiteral(resourceName: "bike_sharing_icon")
        case .carSharing:
            return #imageLiteral(resourceName: "car_sharing_icon")
        case .taxiUber:
            return #imageLiteral(resourceName: "taxi_uber_icon")
        case .motorcycle:
            return #imageLiteral(resourceName: "motorcycle_icon")
        case .eScooter:
            return #imageLiteral(resourceName: "esctooter_icon")
        case .cableCar:
            return #imageLiteral(resourceName: "cable_car_icon")
        case .regionalTrain:
            return #imageLiteral(resourceName: "train_icon")
        case .coach:
            return #imageLiteral(resourceName: "coach_icon")
        case .ski:
            return #imageLiteral(resourceName: "ski_icon")
        case .otherTrack:
            return #imageLiteral(resourceName: "other_track_icon")
        }
    }
}

class Event: Codable, Equatable {
    var id: String
    var geometry: Geometry
    var status: EventStatus
    var meters: Double?
    var type: EventType
    var eventConfig: EventConfig
    var startDate: Date
    var endDate: Date
    var duration: Int
    var untrackedEvents: [EventConfig]
    
    var isFirst: Bool = false
    var isLast: Bool = false
    var isSelected: Bool = false
    var timeInterval = 10
    
    var trackDistance: String {
        guard let meters = meters else {
            return ""
        }
        if meters < 1000 {
            return "\(meters) m"
        } else {
            return String(format: "%.1f km", meters/1000)
        }
    }
    
    var upperDuration: Int {
        if duration % timeInterval == 0 {
            return duration
        } else {
            return duration + timeInterval - duration % timeInterval
        }
    }
    
    var timeDuration: String {
        let duration = Int((endDate.timeIntervalSince1970 - startDate.timeIntervalSince1970) / 60)
        guard duration > 0 else {
            return "| 0m"
        }
        let hour = 60
        guard duration >= hour else {
            return "| (\(duration)m)"
        }
        if duration % hour == 0 {
            return "| (\(duration / hour)h)"
        } else {
            return "| (\(duration / hour)h\(duration % hour)m)"
        }
    }
    
    var startDateTime: String {
        var value = ""
        if startDate.day == endDate.day {
            value = startDate.time
        } else {
            value = startDate.withFormat("E") + " " + startDate.time
        }
        return value
    }
    
    var endDateTime: String {
        var value = ""
        if startDate.day == endDate.day {
            value = endDate.time + " " + timeDuration
        } else {
            value = endDate.withFormat("E") + " " + endDate.time + " " + timeDuration
        }
        return value
    }
    
    init(eventConfig: EventConfig, startDate: Date, endDate: Date) {
        self.eventConfig = eventConfig
        self.id = ""
        self.status = .incomplete
        self.type = .untracked
        self.startDate = startDate
        self.endDate = endDate
        self.duration = Int((endDate.timeIntervalSince1970 - startDate.timeIntervalSince1970) / 60)
        self.untrackedEvents = []
        self.geometry = Geometry()
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id", eventConfig = "name"
        case geometry, status, meters, type, duration, untrackedEvents, endDate, startDate
    }
    
    static func == (lhs: Event, rhs: Event) -> Bool {
        return lhs.id == rhs.id
    }
}

class EventData: Codable {
    var event: Event
    var eventNames: [EventName]
    var eventActivities: [EventActivity]
    
    enum CodingKeys: String, CodingKey {
        case event, eventNames, eventActivities
    }
}

class EventSettings: Codable {
    var untrackedEventDuration: Double
    var minEventDurationTime: Int
    
    enum CodingKeys: String, CodingKey {
        case untrackedEventDuration, minEventDurationTime
    }
}

class EventsData: Codable {
    var events: [Event]
    var settings: EventSettings
    
    enum CodingKeys: String, CodingKey {
        case events, settings
    }
}
