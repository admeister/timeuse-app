//
//  FAQ.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 10/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation

class FAQ: Codable {
    var title: String
    var content: String
    
    enum CodingKeys: String, CodingKey {
        case title, content
    }
}

class Settings: Codable {
    var privacyPolicy: String
    var termsAndConditions: String
    
    enum CodingKeys: String, CodingKey {
        case privacyPolicy, termsAndConditions
    }
}
