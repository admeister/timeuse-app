//
//  Calendar.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 10/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation

class CalendarEvents: Codable {
    var incomplete: [String]
    var complete: [String]
    
    enum CodingKeys: String, CodingKey {
        case incomplete, complete
    }
}
