//
//  TemporaryEvent.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 22/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation

class TemporaryEvent {
    
    static let shared = TemporaryEvent()
    
    var activities: [EventActivity] = []
    var eventType: EventConfig!
    var completedActivites: [EventActivity] = []
    var duration: Int {
        var duration = 0
        completedActivites.forEach { (activity) in
            if let attribute = activity.attribute, attribute.duration != nil {
                duration += attribute.duration
            }
        }
        return duration
    }
    
    func refreshActivites() {
        activities.forEach { activity in
            activity.isFirst = false
            activity.isLast = false
        }
    }
    
    func addActivity(_ activity: EventActivity, attribute: Attribute? = nil) {
        if let activity = activities.first(where: { return $0.id == activity.id }) {
            activity.attribute = attribute
            activity.isSelected = true
        }
    }
    
    func removeActivity(_ activity: EventActivity) {
        if let activity = activities.first(where: { return $0.id == activity.id }) {
            activity.attribute = nil
            activity.isSelected = false
        }
    }
    
}
