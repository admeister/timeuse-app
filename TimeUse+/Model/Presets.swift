//
//  Preset.swift
//  TimeUse
//
//  Created by Robert Havrisciuc on 19/11/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation

struct Presets: Codable {
    struct Duration: Codable {
        let value: Int
        let title: String
    }
    
    struct Partners: Codable {
        struct Value: Codable {
            let one: String
            let two: String
            let three: String
        }
        let value: Value
        let title: String
        
        var displayedValues: [String] {
            return values.map { addExtraLine(value: $0) }
        }
        var values: [String] {
            return [value.one, value.two, value.three].filter { !$0.isEmpty }
        }
        
        private func addExtraLine(value: String) -> String {
            return value.replacingOccurrences(of: " ", with: "\n")
        }
    }
    
    struct Budget: Codable {
        struct Value: Codable {
            let min: Int
            let max: Int
            var interval: Int
        }
        let value: Value
        let title: String
    }
    
    struct Expenditures: Codable {
        struct Value: Codable {
            let firstSlotFirstValue: String
            let firstSlotSecondValue: String
            let secondSlotFirstValue: String
            let secondSlotSecondValue: String
            let thirdSlotFirstValue: String
            let thirdSlotSecondValue: String
        }
        let value: Value
        let title: String
        var firstSlotValues: [String] {
            return [value.firstSlotFirstValue, value.firstSlotSecondValue].filter { !$0.isEmpty }
        }
        var secondSlotValues: [String] {
            return [value.secondSlotFirstValue, value.secondSlotSecondValue].filter { !$0.isEmpty }
        }
        var thirdSlotValues: [String] {
            return [value.thirdSlotFirstValue, value.thirdSlotSecondValue].filter { !$0.isEmpty }
        }
        var allValues: [String] {
            return firstSlotValues + secondSlotValues + thirdSlotValues
        }
    }
    
    var duration: Duration
    var partners: Partners
    var budget: Budget
    var expenditures: Expenditures
}
