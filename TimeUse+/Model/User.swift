//
//  User.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import Foundation
import MotionTagSDK

struct UserSettings: Codable {
    var transferDataOverWifiBool: Bool
    
    init(transferDataOverWifi: Bool) {
        self.transferDataOverWifiBool = transferDataOverWifi
    }
    
    var transferDataOverWifi: UInt {
        switch transferDataOverWifiBool {
        case true:
            return DataTransferMode.wifiOnly.rawValue
        case false:
            return DataTransferMode.wifiAnd3G.rawValue
        }
    }
    
    var motionTagSettings: [String: AnyObject] {
        return [kMTDataTransferMode: transferDataOverWifi as AnyObject,
                kMTBatterySavingsMode: true as AnyObject]
    }
    
    enum CodingKeys: String, CodingKey {
        case transferDataOverWifiBool = "transferDataOverWifi"
    }
}

struct User: Codable {
    var id: String
    var settings: UserSettings
    var inviteCode: String
    var jwtToken: String
    var createDate: Date
    var email: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case jwtToken = "motionTagJWT"
        case createDate = "createdTs"
        case settings, inviteCode, email
    }
}

struct AuthSession: Decodable {
    var user: User
    var token: String?
    
    enum CodingKeys: String, CodingKey {
        case user
        case token = "authToken"
    }
}

