//
//  Constants.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import Foundation

struct Constants {
    #if PROD
    static let BASE_URL = "https://timeuse-plus-api.herokuapp.com/"
    static let API_URL = "\(BASE_URL)api/v1/"
    #elseif DEV
    static let BASE_URL = "https://time-use-api.herokuapp.com/"
    static let API_URL = "\(BASE_URL)api/v1/"
    #endif
    
    static let GoogleAPIKey = "AIzaSyBF_kju2dJO_-WGLE-yOk84PG3qaKhtqOA"
}


extension Constants {
    struct Currency {
        static let chf = "CHF"
    }
}

//MARK: - Notifications
extension Notifications {
    static let sessionClosed = Notification<Error?, String>() // payload, identity of payload
//    static let userDidCheckin = Notification<User, String>() // payload, identity of payload
//    static let userChangedTeam = Notification<Void, String>() // payload, identity of payload
}


