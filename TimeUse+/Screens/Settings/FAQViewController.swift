//
//  FAQViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 10/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class FAQViewController: __CollectionFeedController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: DataSource<FAQ>! {
        didSet { _dataSource = dataSource }
    }
    
    var cellController: FAQCellController! {
        didSet { _cellController = cellController }
    }
    
    var faq: [FAQ] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cellController = FAQCellController()
        loadData()
    }
    
    // MARK: - Load Data
    
    func loadData() {
        activityIndicator.startAnimating()
        API.getFAQ { (result) in
            self.activityIndicator.stopAnimating()
            switch result {
            case .success(let faq):
                self.faq = faq
                self.reloadDataSource()
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    func reloadDataSource() {
        dataSource = DataSource(faq)
    }
    
    // MARK: - Action
    
    @IBAction func backAction(_ sender: Any) {
        popViewController()
    }
    
}
