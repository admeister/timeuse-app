//
//  SettingsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 26/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class SettingsViewController : __CollectionFeedController, ItemTooglableSelectionCellDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var dataSource: SectionedDataSource<Any>! {
        didSet { _dataSource = dataSource }
    }
    
    var cellController: MultiCollectionCellController! {
        didSet { _cellController = cellController }
    }
    
    var settings: Settings!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dataTransferOverWifi = DataTransferOverWifiCellController()
        dataTransferOverWifi.delegate = self
        cellController = MultiCollectionCellController([dataTransferOverWifi,
                                                        SettingsItemCellController()])
        headerController = SettingsHeaderHeaderController()
        headerIsSticky = true
        emptyView = nil
        
        loadData()
    }
    
    // MARK: - Data loading
    
    func loadData() {
        activityIndicator.startAnimating()
        API.getSettings { (result) in
            self.activityIndicator.stopAnimating()
            switch result {
            case .success(let settings):
                self.settings = settings
                self.reloadDataSource()
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    func reloadDataSource() {
        var content: [[Any]] = []
        var sections: [SettingsHeader] = []

        addTrackingSection(to: &content, with: &sections)
        addHelpSection(to: &content, with: &sections)
        addInfoSection(to: &content, with: &sections)
        addLogoutSection(to: &content, with: &sections)

        dataSource = SectionedDataSource(content)
        dataSource.sectionHeaders = sections
    }
    
    func addTrackingSection(to content:inout [[Any]], with sections:inout [SettingsHeader]) {
        sections.append(SettingsHeader(name: Texts.Section.tracking.text).with(config: .topHeader))
        
        let dataTransferOverWifi = ItemToogleableSelection(wrappedContent: SettingsItemType.dataTransferOverWifi, isSelected: Session.currentUser?.settings.transferDataOverWifiBool ?? true)
        content.append([dataTransferOverWifi])
    }
    
    func addHelpSection(to content:inout [[Any]], with sections:inout [SettingsHeader]) {
        sections.append(SettingsHeader(name: Texts.Section.help.text).with(config: .topHeader))
        content.append([SettingsItem(type: .faq)])
    }
    
    func addInfoSection(to content:inout [[Any]], with sections:inout [SettingsHeader]) {
        sections.append(SettingsHeader(name: Texts.Section.info.text))
        content.append([SettingsItem(type: .terms), SettingsItem(type: .privacy), SettingsItem(type: .appVersion), SettingsItem(type: .email)])
    }
    
    func addLogoutSection(to content:inout [[Any]], with sections:inout [SettingsHeader]) {
        sections.append(SettingsHeader(name: "").with(config: .noHeader))
        content.append([SettingsItem(type: .changePassword), SettingsItem(type: .logout)])
    }
    
    // MARK: - ItemTooglableSelectionCellDelegate
    
    func didToogle(item: ItemToogleableSelection) {
        
        var userSettings: UserSettings!
        if item.wrappedContent.title == SettingsItemType.dataTransferOverWifi.title {
            userSettings = UserSettings(transferDataOverWifi: item.isSelected)
        }
        
        MotionTagHelper.shared.stop()
        activityIndicator.startAnimating()

        API.updateUser(settings: userSettings) { result in
            self.activityIndicator.stopAnimating()
            switch result {
            case .success(_):
                MotionTagHelper.shared.start(settings: userSettings.motionTagSettings)
            case .failure(let error):
                MotionTagHelper.shared.start(settings: Session.currentUser?.settings.motionTagSettings)
                self.checkAndShow(error: error)
            }
        }
    }
    
    // MARK: - Actions

    @IBAction func backAction(_ sender: UIControl) {
        popViewController()
    }
}
