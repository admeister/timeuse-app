//
//  ChangePasswordViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 02/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var currentPasswordPlaceholderLabel: UILabel!
    @IBOutlet weak var currentPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var currentPasswordView: ExtendedUIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var newPasswordPlaceholderLabel: UILabel!
    @IBOutlet weak var newPasswordView: ExtendedUIView!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var changePasswordButton: ExtendedUIButton!
    
    var currentPassword: String {
        return currentPasswordTextField.text!
    }
    
    var newPassword: String {
        return newPasswordTextField.text!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurePlaceholder(enabled: false)
        configureInputVisibilityController()
        toogleChangePasswordButton(enabled: false)
        configureTextFields()
    }
    
    // MARK: - UI Configuration
    
    func configureTextFields() {
        currentPasswordTextField.selectedTitle = Texts.Other.oldPassword.text
        currentPasswordTextField.title = Texts.Other.oldPassword.text
        currentPasswordTextField.titleFormatter = { text in
            return text.capitalizingFirstLetter()
        }
        newPasswordTextField.selectedTitle = Texts.Other.newPassword.text
        newPasswordTextField.title = Texts.Other.newPassword.text
        newPasswordTextField.titleFormatter = { text in
            return text.capitalizingFirstLetter()
        }
    }
    
    func configureInputVisibilityController() {
        let keyboardController = self.view.addKeyboardVisibilityController()
        keyboardController.dismissKeyboardTouchRecognizer?.ignoreViews = [changePasswordButton, currentPasswordView, newPasswordView]
    }
    
    func toogleChangePasswordButton(enabled: Bool) {
        changePasswordButton.isEnabled = enabled
        changePasswordButton.alpha = enabled ? 1.0 : 0.6
    }
    
    func configurePlaceholder(enabled: Bool) {
        currentPasswordPlaceholderLabel.isHidden = enabled
        newPasswordPlaceholderLabel.isHidden = enabled
    }
    

    // MARK: - Actions
    
    @IBAction func changePasswordAction(_ sender: Any) {
        view.endEditing(true)
        
        guard currentPassword.isValidPassword(), newPassword.isValidPassword() else {
            ErrorDisplay.showErrorToast(withMessage: Texts.Toast.incorrectPassword.text)
            return
        }
        
        activityIndicator.startAnimating()
        toogleChangePasswordButton(enabled: false)
        
        API.changePassword(oldPassword: currentPassword, newPassword: newPassword) { result in
            self.activityIndicator.stopAnimating()
            self.toogleChangePasswordButton(enabled: true)
            switch result {
            case .success(_):
                ErrorDisplay.showSuccesToast(message: Texts.Toast.passwordSuccessfullyChanged.text)
                self.dismiss(animated: true, completion: nil)
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showCurrentPasswordAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        currentPasswordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func showNewPasswordAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        newPasswordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        setTextFields(textField: sender)
        if !currentPassword.isEmpty && !newPassword.isEmpty {
            toogleChangePasswordButton(enabled: true)
        } else {
            toogleChangePasswordButton(enabled: false)
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case currentPasswordTextField:
            newPasswordTextField.becomeFirstResponder()
            return false
        case newPasswordTextField:
            newPasswordTextField.resignFirstResponder()
            return false
        default:
            break
        }
        
        return true
    }
    
    func setTextFields(textField: UITextField, enabled: Bool? = nil) {
        switch textField {
        case currentPasswordTextField:
            currentPasswordPlaceholderLabel.isHidden = true
            currentPasswordView.borderColor = Constants.Colors.main.blue.value
            if let enabled = enabled {
                currentPasswordTextField.setTitleVisible(enabled)
            }
        case newPasswordTextField:
            newPasswordPlaceholderLabel.isHidden = true
            newPasswordView.borderColor = Constants.Colors.main.blue.value
            if let enabled = enabled {
                newPasswordTextField.setTitleVisible(enabled)
            }
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setTextFields(textField: textField, enabled: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case currentPasswordTextField:
            currentPasswordView.borderColor = Constants.Colors.main.lightGrey.value
            currentPasswordTextField.setTitleVisible(false)
            if currentPassword.isEmpty {
                currentPasswordPlaceholderLabel.isHidden = false
            } else {
                currentPasswordPlaceholderLabel.isHidden = true
            }
        case newPasswordTextField:
            newPasswordView.borderColor = Constants.Colors.main.lightGrey.value
            newPasswordTextField.setTitleVisible(false)
            if newPassword.isEmpty {
                newPasswordPlaceholderLabel.isHidden = false
            } else {
                newPasswordPlaceholderLabel.isHidden = true
            }
        default:
            break
        }
    }

}
