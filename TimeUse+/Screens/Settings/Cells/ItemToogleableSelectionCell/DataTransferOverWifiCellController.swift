//
//  ItemToogleableSelectionCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

protocol ItemTooglableSelectionCellDelegate: class {

    func didToogle(item: ItemToogleableSelection)
}

protocol ItemToogleableSelectionProtocol {
    var title: String! { get }
    var description: String! { get }
}
    
class ItemToogleableSelection {
        
    var isSelected: Bool
    var wrappedContent: ItemToogleableSelectionProtocol!
    
    init(wrappedContent: ItemToogleableSelectionProtocol, isSelected: Bool) {
        self.isSelected = isSelected
        self.wrappedContent = wrappedContent
    }
}

class DataTransferOverWifiCellController: CollectionCellController<ItemToogleableSelection, ItemToogleableSelectionCell> {
    
    weak var delegate: ItemTooglableSelectionCellDelegate?
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 135.0))
    }
    
    override func cellSize(for content: ItemToogleableSelection, in collectionView: UICollectionView) -> CGSize {
        var size = super.cellSize
        let cell = self.sizeCalculationCell!
        size.height -= cell.descriptionLabel.frame.height
        self.configureCell(cell, for: content, at: IndexPath(item: 0, section: 0))
        size.height += cell.descriptionLabel.intrinsicContentSize.height
        return size
    }
    
    override func configureCell(_ cell: ItemToogleableSelectionCell, for content: ItemToogleableSelection, at indexPath: IndexPath) {
        cell.titleLabel.text = content.wrappedContent.title
        cell.descriptionLabel.text = content.wrappedContent.description
        cell.switchControl.isOn = content.isSelected
        cell.onSwitch { [weak self] in
            content.isSelected = !content.isSelected
            self?.delegate?.didToogle(item: content)
        }
    }
    
    override func didSelectContent(_ content: ItemToogleableSelection, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
}
