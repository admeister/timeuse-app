//
//  ItemToogleableSelectionCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class ItemToogleableSelectionCell: UICollectionViewCell {

    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var _onSwitch: (()->())!
    func onSwitch(_ callback: (() -> ())!) {
        self._onSwitch = callback
    }
    
    @IBAction func switchAction(_ sender: UISwitch) {
        _onSwitch?()
    }
}
