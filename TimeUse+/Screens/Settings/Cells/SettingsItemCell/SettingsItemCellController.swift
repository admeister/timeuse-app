//
//  SettingsItemCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude
import SafariServices
import MotionTagSDK

enum SettingsItemType: String, ItemToogleableSelectionProtocol {
    
    case dataTransferOverWifi
    case faq
    case terms
    case privacy
    case appVersion
    case email
    case changePassword
    case logout
    
    var title: String! {
        switch self {
        case .faq:
            return Texts.Other.faq.text
        case .terms:
            return Texts.Other.termsAndConditions.text
        case .privacy:
            return Texts.Other.privacyPolicy.text
        case .appVersion:
            return Texts.Other.appVersion.text
        case .changePassword:
            return Texts.Other.changePassword.text
        case .logout:
            return Texts.Other.logout.text
        case .dataTransferOverWifi:
            return Texts.Other.dataTransferOverWifi.text
        case .email:
            return "Email"
        }
    }
    
    var description: String! {
        switch self {
        case .dataTransferOverWifi:
            return Texts.Other.dataTransferOverWifiDescription.text
        default:
            return ""
        }
    }
}

class SettingsItem {
    var type: SettingsItemType
    
    init(type: SettingsItemType) {
        self.type = type
    }
}

class BaseSettingsCellController<C>: CollectionCellController<C, SettingsItemCell> {
    
        init() {
        super.init(cellSize: CGSize(width: -1.0, height: 57.0))
    }
    
    override func configureCell(_ cell: SettingsItemCell, for content: C, at indexPath: IndexPath) {
        
    }
    
    override func didSelectContent(_ content: C, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
}

class SettingsItemCellController: BaseSettingsCellController<SettingsItem> {
    
    var _parent: SettingsViewController {
        return parentViewController as! SettingsViewController
    }
    
    override func configureCell(_ cell: SettingsItemCell, for content: SettingsItem, at indexPath: IndexPath) {
        cell.titleLabel.text = content.type.title
        switch content.type {
        case .terms:
            cell.bottomDividerLeadingConstraint.constant = 16
            cell.toogleCTAIcon(enabled: true)
        case .privacy:
            cell.toogleCTAIcon(enabled: true)
            cell.topDividerView.isHidden = true
            cell.bottomDividerLeadingConstraint.constant = 16
        case .appVersion:
            cell.toogleCTAIcon(enabled: false)
            cell.appVersionLabel.text = getAppVersion()
            cell.topDividerView.isHidden = true
            cell.bottomDividerLeadingConstraint.constant = 16
        case .email:
            cell.toogleCTAIcon(enabled: false)
            cell.appVersionLabel.text = Session.currentUser?.email
            cell.topDividerView.isHidden = true
        case .changePassword:
            cell.toogleCTAIcon(enabled: true)
            cell.bottomDividerView.isHidden = true
        default:
            cell.toogleCTAIcon(enabled: true)
        }
    }
    
    override func didSelectContent(_ content: SettingsItem, at indexPath: IndexPath, in collectionView: UICollectionView) {
        switch content.type {
        case .faq:
            let controller = FAQViewController.instantiateFromHomeStoryboard()
            _parent.navigationController?.pushViewController(controller, animated: true)
        case .privacy:
            showWebpage(urlString: _parent.settings.privacyPolicy)
        case .terms:
            showWebpage(urlString: _parent.settings.termsAndConditions)
        case .changePassword:
            changePassword()
        case .logout:
            logout()
        default:
            break
        }
    }
    
    func showWebpage(urlString: String) {
        var webPage: SFSafariViewController!
        if let url = URL(string: urlString) {
            webPage = SFSafariViewController(url: url)
            _parent.present(webPage, animated: true, completion: nil)
        }
    }
    
    func changePassword() {
        let controller = ChangePasswordViewController.instantiateFromHomeStoryboard()
        _parent.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    func logout() {
        let alert = UIAlertController(title: nil, message: Texts.Other.areYouSureLogout.text, preferredStyle: .alert)
        
        let signOutAction = UIAlertAction(title: Texts.Other.yes.text, style: .default) { action in
            API.logout { result in
                MotionTagHelper.shared.stop()
                Session.close(error: result.error)
            }
        }
        alert.addAction(signOutAction)
        alert.addAction(UIAlertAction(title: Texts.Other.cancel.text, style: .cancel))
        
        _parent.present(alert, animated: true, completion: nil)
    }
}
