//
//  SettingsItemCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class SettingsItemCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var rightArrowImage: UIImageView!
    @IBOutlet weak var topDividerView: UIView!
    @IBOutlet weak var bottomDividerView: UIView!
    @IBOutlet var bottomDividerLeadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bottomDividerLeadingConstraint.priority = UILayoutPriority(1000.0)
    }
    
    func toogleCTAIcon(enabled: Bool) {
        rightArrowImage.isHidden = !enabled
        appVersionLabel.isHidden = enabled
    }
}
