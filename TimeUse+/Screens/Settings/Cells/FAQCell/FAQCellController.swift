//
//  FAQCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 10/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

class FAQCellController: BaseSettingsCellController<FAQ> {
    
    var _parent: FAQViewController {
        return parentViewController as! FAQViewController
    }
    
    override func configureCell(_ cell: SettingsItemCell, for content: FAQ, at indexPath: IndexPath) {
        cell.titleLabel.text = content.title
        cell.topDividerView.isHidden = true
        cell.toogleCTAIcon(enabled: true)
    }
    
    override func didSelectContent(_ content: FAQ, at indexPath: IndexPath, in collectionView: UICollectionView) {
        let controller = FAQDetailsViewController._init(faq: content)
        _parent.navigationController?.pushViewController(controller, animated: true)
    }
    
}
