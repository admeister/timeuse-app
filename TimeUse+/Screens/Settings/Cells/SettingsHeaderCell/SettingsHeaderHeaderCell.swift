//
//  SettingsHeaderHeaderCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude

class SettingsHeaderHeaderCell: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        topConstraint.priority = UILayoutPriority(1000.0)
    }
    
}
