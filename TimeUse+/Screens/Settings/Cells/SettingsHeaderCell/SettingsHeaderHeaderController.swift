//
//  SettingsHeaderHeaderController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude

class SettingsHeader {
    
    enum Config {
        case normal
        case topHeader
        case noHeader
    }
    
    var config: Config = .normal
    var name:String
    
    init(name:String) {
        self.name = name
    }
    
    init() {
        self.name = ""
    }
    
    func with(config:Config) -> SettingsHeader {
        self.config = config
        return self
    }
    
}

class SettingsHeaderHeaderController: CollectionHeaderController<SettingsHeader, SettingsHeaderHeaderCell> {
    init() {
        super.init(headerSize: CGSize(width: -1.0, height: 55.0))
    }
    
    override func headerSize(for content: SettingsHeader, in collectionView: UICollectionView) -> CGSize {
        var size = super.headerSize
        if content.config == .topHeader {
            size.height = 47.0
        } else if content.config == .noHeader {
            size.height = 24.0
        }
        return size
    }
    
    override func configureHeader(_ header: SettingsHeaderHeaderCell, for content: SettingsHeader, at indexPath: IndexPath) {
        header.titleLabel.text = content.name
        if content.config == .topHeader {
            header.topConstraint.constant = 16.0
        }
    }
}
