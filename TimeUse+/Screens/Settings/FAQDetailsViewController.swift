//
//  FAQDetailsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 10/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import WebKit

class FAQDetailsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var faq: FAQ!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    static func _init(faq: FAQ) -> FAQDetailsViewController {
        let controller = FAQDetailsViewController.instantiateFromHomeStoryboard()
        controller.faq = faq
        return controller
    }
    
    // MARK: - UI Configuration
    
    func configureUI() {
        titleLabel.text = faq.title
        webView.loadHTMLString(faq.content, baseURL: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func backAction(_ sender: Any) {
        popViewController()
    }
    

}
