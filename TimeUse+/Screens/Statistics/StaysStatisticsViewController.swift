//
//  StaysStatisticsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude
import Charts

class StaysStatisticsViewController: UIViewController {

    @IBOutlet weak var homeHoursLabel: UILabel!
    @IBOutlet weak var workHoursLabel: UILabel!
    @IBOutlet weak var otherHoursLabel: UILabel!
    @IBOutlet weak var timeSpentAtHomeBackgroundBar: ExtendedUIView!
    @IBOutlet weak var timeSpentAtHomeWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeSpentAtWorkWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var timeSpentAtWorkBackgroundBar: ExtendedUIView!
    @IBOutlet weak var timeSpentAtOtherBackgroundBar: ExtendedUIView!
    @IBOutlet weak var timeSpentAtOtherWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noRecordedActivitiesLabel: UILabel!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var homeRightLabelWidthConstraint: NSLayoutConstraint!
    
    let emptyBarWidth: CGFloat = 25
    var feedController = __CollectionFeedController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        feedController.collectionView = collectionView
        feedController._cellController = MostFrequentActivityCellController()
        feedController.emptyView = nil
        
        pieChartView.legend.enabled = false
        pieChartView.transparentCircleRadiusPercent = 0
        pieChartView.isUserInteractionEnabled = false
    }
    
    // MARK: - UI Configuration
    
    func updateUI(statistics: Statistics) {
        configureTimeSpentAtHome(statistics)
        configureTimeSpentAtWork(statistics)
        configureTimeSpentAtOther(statistics)
        configureChart(statistics)
        configureTopActivities(statistics)
    }
    
    private func configureTimeSpentAtHome(_ statistics: Statistics) {
        homeHoursLabel.text = statistics.staying.summary.timeAtHomeString
        homeHoursLabel.textColor = statistics.staying.summary.timeSpentAtHome != 0 ? Constants.Colors.main.dark.value : Constants.Colors.main.darkGrey.value
        
        timeSpentAtHomeWidthConstraint.constant = statistics.staying.summary.timeSpentAtHome == statistics.staying.summary.stayTime && statistics.staying.summary.stayTime != 0 ? timeSpentAtHomeBackgroundBar.frame.width : timeSpentAtHomeBackgroundBar.frame.width * CGFloat(statistics.staying.summary.thisWeekTimeSpentAtHome) / 100
        if statistics.staying.summary.thisWeekTimeSpentAtHome <= Double(emptyBarWidth) {
            timeSpentAtHomeWidthConstraint.constant += emptyBarWidth
        }
    }
    
    private func configureTimeSpentAtWork(_ statistics: Statistics) {
        workHoursLabel.text = statistics.staying.summary.timeAtWorkString
        workHoursLabel.textColor = statistics.staying.summary.timeSpentAtWork != 0 ? Constants.Colors.main.dark.value : Constants.Colors.main.darkGrey.value
        
        timeSpentAtWorkWidthConstraint.constant = statistics.staying.summary.timeSpentAtWork == statistics.staying.summary.stayTime && statistics.staying.summary.stayTime != 0 ? timeSpentAtWorkBackgroundBar.frame.width : timeSpentAtWorkBackgroundBar.frame.width * CGFloat(statistics.staying.summary.thisWeekTimeSpentAtWork) / 100
        if statistics.staying.summary.thisWeekTimeSpentAtWork <= Double(emptyBarWidth) {
            timeSpentAtWorkWidthConstraint.constant += emptyBarWidth
        }
    }
    
    private func configureTimeSpentAtOther(_ statistics: Statistics) {
        otherHoursLabel.text = statistics.staying.summary.timeAtOtherString
        otherHoursLabel.textColor = statistics.staying.summary.timeSpentAtOther != 0 ? Constants.Colors.main.dark.value : Constants.Colors.main.darkGrey.value
        
        timeSpentAtOtherWidthConstraint.constant = statistics.staying.summary.timeSpentAtOther == statistics.staying.summary.stayTime && statistics.staying.summary.stayTime != 0 ? timeSpentAtOtherBackgroundBar.frame.width : timeSpentAtOtherBackgroundBar.frame.width * CGFloat(statistics.staying.summary.thisWeekTimeSpentAtOther) / 100
        if statistics.staying.summary.thisWeekTimeSpentAtOther <= Double(emptyBarWidth) {
            timeSpentAtOtherWidthConstraint.constant += emptyBarWidth
        }
    }
    
    private func configureChart(_ statistics: Statistics) {
        totalHoursLabel.text = statistics.staying.totalTimeString
        var chartDataEntries: [ChartDataEntry] = []
        var colors: [UIColor] = []
        if statistics.staying.activities.count == 0 {
            colors.append(Constants.Colors.main.lightGrey.value)
            chartDataEntries.append(PieChartDataEntry(value: 1))
        } else {
            statistics.staying.activities.forEach { (activity) in
                chartDataEntries.append(PieChartDataEntry(value: activity.value))
                colors.append(activity.color)
            }
        }
        let chartDataSet = PieChartDataSet(entries: chartDataEntries)
        let chartData = PieChartData(dataSet: chartDataSet)
        chartData.setValueFormatter(ChartFormatter(totalValue: statistics.staying.totalTime))
        chartDataSet.colors = colors
        pieChartView.data = chartData
    }
    
    private func configureTopActivities(_ statistics: Statistics) {
        if statistics.staying.activities.isEmpty {
            feedController._dataSource = DataSource([])
            noRecordedActivitiesLabel.isHidden = false
        } else {
            noRecordedActivitiesLabel.isHidden = true
            statistics.staying.activities.last?.isLast = true
            feedController._dataSource = DataSource(statistics.staying.activities)
        }
    }

}
