//
//  MovingStatisticsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class MovingStatisticsViewController: UIViewController {

    @IBOutlet weak var calendarCollectionView: UICollectionView!
    @IBOutlet weak var transportationCollectionView: UICollectionView!
    @IBOutlet weak var noRecordedEventsLabel: UILabel!
    @IBOutlet weak var emissionsCollectionView: UICollectionView!
    @IBOutlet weak var noEmissionsLabel: UILabel!
    
    var calendarFeedController = CalendarFeedController()
    var transportationFeedController = TransportationFeedController()
    var emissionsFeedController = __CollectionFeedController()
    
    let onSelectionInteraction = Observable<MovingStatisticsOrderType>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureFeedControllers()
    }
    
    func configureFeedControllers() {
        calendarFeedController.collectionView = calendarCollectionView
        calendarCollectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        let cellController = MovingStatisticsCalendarCellController()
        cellController.onSelectionInteraction.subscribe(with: self) { (self, movingModel) in
            self.onSelectionInteraction.notify(value: movingModel.type)
            self.transportationFeedController.type = movingModel.type
        }
        calendarFeedController._cellController = cellController
        
        transportationFeedController.collectionView = transportationCollectionView
        transportationCollectionView.contentInset = UIEdgeInsets(top: 24, left: 16, bottom: 0, right: 16)
        transportationFeedController._cellController = MovingTransportationCellController()
        transportationFeedController.emptyView = nil
        
        emissionsFeedController.collectionView = emissionsCollectionView
        emissionsCollectionView.contentInset = UIEdgeInsets(top: 24, left: 16, bottom: 0, right: 16)
        emissionsFeedController._cellController = EmissionsCellController()
        emissionsFeedController.emptyView = nil
    }
    
    // MARK: - Data Loading
    
    func updateUI(statistics: Statistics) {
        calendarFeedController.movingEvents = MovingModel(value: "\(statistics.moving.summary.totalEvents)", title: Texts.Statistics.movingEvents.text, type: .totalEvents, isSelected: calendarFeedController.movingEvents != nil ? calendarFeedController.movingEvents.isSelected : true)
        calendarFeedController.totalDistance = MovingModel(value: statistics.moving.summary.totalDistanceString, title: Texts.Statistics.totalDistance.text, type: .totalDistance, isSelected: calendarFeedController.totalDistance != nil ? calendarFeedController.totalDistance.isSelected : false)
        calendarFeedController.movingTime = MovingModel(value: statistics.moving.summary.movingTimeString, title: Texts.Statistics.totalTime.text, type: .totalTime, isSelected: calendarFeedController.movingTime != nil ? calendarFeedController.movingTime.isSelected : false)
        calendarFeedController.averageSpeed = MovingModel(value: statistics.moving.summary.averageSpeedString, title: Texts.Statistics.averageSpeed.text, type: .averageSpeed, isSelected: calendarFeedController.averageSpeed != nil ? calendarFeedController.averageSpeed.isSelected : false)
        calendarFeedController._dataSource = DataSource(calendarFeedController.allItems)
        
        if statistics.moving.topEvents.isEmpty {
            noRecordedEventsLabel.isHidden = false
            noEmissionsLabel.isHidden = false
            transportationFeedController._dataSource = DataSource([])
            emissionsFeedController._dataSource = DataSource([])
        } else {
            noRecordedEventsLabel.isHidden = true
            noEmissionsLabel.isHidden = true
            emissionsFeedController._dataSource = DataSource(sortedTopEventsByEmission(topEvents: statistics.moving.topEvents))
            transportationFeedController._dataSource = DataSource(statistics.moving.topEvents)
        }
    }
    
    func sortedTopEventsByEmission(topEvents: [TopEvent]) -> [TopEvent] {
        return topEvents.sorted(by: { return $0.emissions > $1.emissions })
    }
}

class CalendarFeedController: __CollectionFeedController {
    var movingEvents: MovingModel!
    var totalDistance: MovingModel!
    var movingTime: MovingModel!
    var averageSpeed: MovingModel!
    
    var allItems: [MovingModel] {
        return [movingEvents, totalDistance, movingTime, averageSpeed]
    }
}

class TransportationFeedController: __CollectionFeedController {
    var type = MovingStatisticsOrderType.totalEvents
}
