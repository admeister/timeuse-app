//
//  MostExpensesCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 08/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class MostExpensesCellController: CollectionCellController<TopActivity, MostExpensesCell> {
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 57.0))
    }
    
    override func configureCell(_ cell: MostExpensesCell, for content: TopActivity, at indexPath: IndexPath) {
        cell.imageView.layer.cornerRadius = 8
        cell.imageView.backgroundColor = content.color
        cell.budgetLabel.text = "\(Int(content.value)) \(Constants.Currency.chf)"
        cell.budgetLabel.textColor = content.color
        cell.titleLabel.text = content.name
        cell.iconImageView.setImageFromURLString("\(Constants.BASE_URL)\(content.icon)", placeholder: nil, cacheKey: nil)
        if content.isLast {
            cell.dividerView.isHidden = true
        } else {
            cell.dividerView.isHidden = false
        }
    }
    
    override func didSelectContent(_ content: TopActivity, at indexPath: IndexPath, in collectionView: UICollectionView) {
        
    }
}
