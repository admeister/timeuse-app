//
//  MostExpensesCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 08/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class MostExpensesCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var budgetLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
