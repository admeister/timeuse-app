//
//  MostFrequentActivityCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 28/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class MostFrequentActivityCell: UICollectionViewCell {

    @IBOutlet weak var colorImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var hoursLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
