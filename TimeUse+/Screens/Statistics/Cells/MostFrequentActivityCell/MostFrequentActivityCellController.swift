//
//  MostFrequentActivityCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 28/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class MostFrequentActivityCellController: CollectionCellController<TopActivity, MostFrequentActivityCell> {
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 57.0))
    }
    
    override func configureCell(_ cell: MostFrequentActivityCell, for content: TopActivity, at indexPath: IndexPath) {
        cell.colorImageView.layer.cornerRadius = 8
        cell.colorImageView.backgroundColor = content.color
        cell.hoursLabel.text = content.time
        cell.hoursLabel.textColor = content.color
        cell.iconImageView.setImageFromURLString("\(Constants.BASE_URL)\(content.icon)", placeholder: nil, cacheKey: nil)
        cell.titleLabel.text = content.name
        if content.isLast {
            cell.dividerView.isHidden = true
        } else {
            cell.dividerView.isHidden = false
        }
    }
    
    override func didSelectContent(_ content: TopActivity, at indexPath: IndexPath, in collectionView: UICollectionView) {
        
    }
}
