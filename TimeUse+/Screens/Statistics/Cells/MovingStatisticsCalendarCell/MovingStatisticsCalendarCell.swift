//
//  MovingStatisticsCalendarCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class MovingStatisticsCalendarCell: UICollectionViewCell {

    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: ExtendedUIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
