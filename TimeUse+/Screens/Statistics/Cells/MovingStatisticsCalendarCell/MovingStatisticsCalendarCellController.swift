//
//  MovingStatisticsCalendarCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class MovingModel {
    var value: String
    var title: String
    var isSelected: Bool
    var type: MovingStatisticsOrderType
    
    init(value: String, title: String, type: MovingStatisticsOrderType, isSelected: Bool = false) {
        self.value = value
        self.title = title
        self.type = type
        self.isSelected = isSelected
    }
}

class MovingStatisticsCalendarCellController: CollectionCellController<MovingModel, MovingStatisticsCalendarCell> {
    
    let onSelectionInteraction = Observable<MovingModel>()
    
    var _parent: CalendarFeedController {
        return parentViewController as! CalendarFeedController
    }
    
    init() {
        super.init(cellSize: CGSize(width: 167.0, height: 88.0))
        minimumInteritemSpacing = 9
    }
    
    override func cellSize(for content: MovingModel, in collectionView: UICollectionView) -> CGSize {
        var size = super.cellSize
        let minimumInteritemSpacing: CGFloat = 9
        let collectionViewSize = collectionView.frame.size.width - collectionView.contentInset.left - collectionView.contentInset.right - minimumInteritemSpacing
        size.width = collectionViewSize / 2
        return size
    }
    
    override func configureCell(_ cell: MovingStatisticsCalendarCell, for content: MovingModel, at indexPath: IndexPath) {
        cell.valueLabel.text = content.value
        cell.titleLabel.text = content.title
        cell.containerView.backgroundColor = content.isSelected ? Constants.Colors.main.blue.value : Constants.Colors.main.darkGrey.value
    }
    
    override func didSelectContent(_ content: MovingModel, at indexPath: IndexPath, in collectionView: UICollectionView) {
        guard !content.isSelected else {
            return
        }
        _parent.allItems.forEach{ $0.isSelected = false }
        content.isSelected = !content.isSelected
        triggerReload()
        onSelectionInteraction.notify(value: content)
    }
    
    func triggerReload() {
        _parent.collectionView.reloadData()
    }
}
