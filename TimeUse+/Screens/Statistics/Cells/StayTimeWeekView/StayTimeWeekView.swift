//
//  StayTimeWeekView.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 28/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class StayTimeWeekView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var homeView: ExtendedUIView!
    @IBOutlet weak var homeWidth: NSLayoutConstraint!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var otherView: ExtendedUIView!
    @IBOutlet weak var otherLabel: UILabel!
    @IBOutlet weak var otherWidth: NSLayoutConstraint!
    @IBOutlet weak var workView: ExtendedUIView!
    @IBOutlet weak var workWidth: NSLayoutConstraint!
    @IBOutlet weak var workLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromNib()
        configureBars()
    }
    
    func loadFromNib() {
        let bundle = Bundle(for: StayTimeWeekView.self)
        bundle.loadNibNamed("StayTimeWeekView", owner: self, options: nil)
        self.frame.size = contentView.frame.size
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.addSubview(contentView)
    }
    
    private func configureBars() {
        homeView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        otherView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        workView.layer.maskedCorners = []
    }
    
    func configureUI(home: Double, work: Double, other: Double) {
        
        guard home != 0 || work != 0 || other != 0 else {
            homeWidth.constant = 0
            workWidth.constant = 0
            otherWidth.constant = 0
            configureLabels(home: "", work: "", other: "")
            return
        }
        if home == 100 {
            homeView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            homeWidth.constant = contentView.frame.width
            workWidth.constant = 0
            otherWidth.constant = 0
            configureLabels(home: "100%", work: "", other: "")
        } else if work == 100 {
            workView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            homeWidth.constant = 0
            workWidth.constant = contentView.frame.width
            otherWidth.constant = 0
            configureLabels(home: "", work: "100%", other: "")
        } else if other == 100 {
            otherView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            homeWidth.constant = 0
            workWidth.constant = 0
            otherWidth.constant = contentView.frame.width
            configureLabels(home: "", work: "", other: "100%")
        } else {
            if home == 0 {
                workView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            } else if other == 0 {
                workView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            } else {
                configureBars()
            }
            homeWidth.constant = CGFloat(home) * contentView.frame.width / 100
            workWidth.constant = CGFloat(work) * contentView.frame.width / 100
            otherWidth.constant = CGFloat(other) * contentView.frame.width / 100
            let homeValue = home != 0 ? "\(home.rounded(toPlaces: 1))%" : ""
            let workValue = work != 0 ? "\(work.rounded(toPlaces: 1))%" : ""
            let otherValue = other != 0 ? "\(other.rounded(toPlaces: 1))%" : ""
            configureLabels(home: homeValue, work: workValue, other: otherValue)
        }
    }
    
    private func configureLabels(home: String, work: String, other: String) {
        homeLabel.text = home
        workLabel.text = work
        otherLabel.text = other
    }

}
