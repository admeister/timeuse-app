//
//  MovingTransportationCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class BaseMovingStatisticsCellController: CollectionCellController<TopEvent, MovingTransportationCell> {
    
    var _parent: TransportationFeedController {
        return parentViewController as! TransportationFeedController
    }
    
    init() {
        super.init(cellSize: CGSize(width: 167.0, height: 73.0))
        minimumInteritemSpacing = 9
    }
    
    override func cellSize(for content: TopEvent, in collectionView: UICollectionView) -> CGSize {
        var size = super.cellSize
        let minimumInteritemSpacing: CGFloat = 9
        let collectionViewSize = collectionView.frame.size.width - collectionView.contentInset.left - collectionView.contentInset.right - minimumInteritemSpacing
        size.width = collectionViewSize / 2
        
        return size
    }
    
    override func configureCell(_ cell: MovingTransportationCell, for content: TopEvent, at indexPath: IndexPath) {
        cell.iconImageView.image = content.name.icon
        cell.imageContainerView.backgroundColor = content.name.color
        cell.valueLabel.textColor = content.name.color
        cell.subtitleLabel.text = content.name.title
    }
    
    override func didSelectContent(_ content: TopEvent, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
}

class MovingTransportationCellController: BaseMovingStatisticsCellController {
    
    override func configureCell(_ cell: MovingTransportationCell, for content: TopEvent, at indexPath: IndexPath) {
        super.configureCell(cell, for: content, at: indexPath)
        
        var value = ""
        switch _parent.type {
        case .totalEvents:
            value = "\(content.eventsCount)"
        case .totalTime:
            value = content.time
        case .totalDistance:
            value = content.distance
        case .averageSpeed:
            value = content.averageSpeedString
        }
        cell.valueLabel.text = value
    }
}

class EmissionsCellController: BaseMovingStatisticsCellController {
    
    override func configureCell(_ cell: MovingTransportationCell, for content: TopEvent, at indexPath: IndexPath) {
        super.configureCell(cell, for: content, at: indexPath)
        
        cell.valueLabel.text = content.emissionsString
    }
}
