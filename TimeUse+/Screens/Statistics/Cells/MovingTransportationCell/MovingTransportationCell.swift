//
//  MovingTransportationCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 27/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class MovingTransportationCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var imageContainerView: ExtendedUIView!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
