//
//  ExpensesStatisticsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Charts
import Tapptitude

class ExpensesStatisticsViewController: UIViewController {

    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var totalBudgetLabel: UILabel!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    var feedController = __CollectionFeedController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        feedController.collectionView = collectionView
        feedController._cellController =  MostExpensesCellController()
        feedController.emptyView = nil
        
        pieChartView.legend.enabled = false
        pieChartView.transparentCircleRadiusPercent = 0
        pieChartView.isUserInteractionEnabled = false
        
        currencyLabel.text = Constants.Currency.chf
    }
    
    // MARK: - UI Configuration
    
    func updateUI(statistics: Statistics) {
        configureChart(statistics)
        configureTopExpenses(statistics)
    }
    
    private func configureChart(_ statistics: Statistics) {
        totalBudgetLabel.text = "\(statistics.expenses.totalBudget)"
        var chartDataEntries: [ChartDataEntry] = []
        var colors: [UIColor] = []
        if statistics.expenses.activities.count == 0 {
            colors.append(Constants.Colors.main.lightGrey.value)
            chartDataEntries.append(PieChartDataEntry(value: 1))
        } else {
            statistics.expenses.activities.forEach { (activity) in
                chartDataEntries.append(PieChartDataEntry(value: activity.value))
                colors.append(activity.color)
            }
        }
        let chartDataSet = PieChartDataSet(entries: chartDataEntries)
        let chartData = PieChartData(dataSet: chartDataSet)
        chartData.setValueFormatter(ChartFormatter(totalValue: Double(statistics.expenses.totalBudget)))
        chartDataSet.colors = colors
        pieChartView.data = chartData
    }
    
    private func configureTopExpenses(_ statistics: Statistics) {
        if statistics.expenses.activities.isEmpty {
            emptyLabel.isHidden = false
            feedController._dataSource = DataSource([])
        } else {
            emptyLabel.isHidden = true
            statistics.expenses.activities.last?.isLast = true
            feedController._dataSource = DataSource(statistics.expenses.activities)
        }
    }

}

class ChartFormatter: IValueFormatter {
    
    var totalValue: Double
    
    init(totalValue: Double) {
        self.totalValue = totalValue
    }
    
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        let minPercentage = 10.0
        let percentage = Double(round(10*(value/totalValue * 100))/10)
        
        guard percentage < 100 else {
            if totalValue == 0 {
                return ""
            }
            return "100%"
        }
        
        guard percentage > minPercentage else {
            return ""
        }
    
        return "\(percentage)%"
    }
}
