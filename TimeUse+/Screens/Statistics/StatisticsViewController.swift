//
//  StatisticsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import TTSegmentedControl

class StatisticsViewController: UIViewController {

    @IBOutlet weak var segmentedControl: TTSegmentedControl!
    @IBOutlet weak var expensesContainerView: UIView!
    @IBOutlet weak var staysContainerView: UIView!
    @IBOutlet weak var movingContainerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var calendarBackButton: UIButton!
    @IBOutlet weak var calendarHeaderLabel: UILabel!
    @IBOutlet weak var calendarNextButton: UIButton!
    
    var movingStatisticsViewController: MovingStatisticsViewController!
    var staysStatisticsViewController: StaysStatisticsViewController!
    var expensesStatisticsViewController: ExpensesStatisticsViewController!
    
    let didUpdateStatistics = Observable<Statistics>()
    
    var firstDayOfSelectedWeek: Date!
    var lastDayOfSelectedWeek: Date!
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let user = Session.currentUser {
            self.user = user
        }
        
        configureSegmentedControl()
        
        configureDates()
        configureCalendarHeader()
        loadData(fromDate: firstDayOfSelectedWeek, toDate: lastDayOfSelectedWeek)
        
        configureViewControllers()
    }
    
    // MARK: - Data Loading
    
    func loadData(fromDate: Date, toDate: Date, orderBy: MovingStatisticsOrderType = .totalEvents) {
        activityIndicator.startAnimating()
        toogleCalendarButtons(false)
        API.getStatistics(from: fromDate.withFormat("YYYY-MM-dd"), to: toDate.withFormat("YYYY-MM-dd"), timezone: TimeZone.current.identifier, orderBy: orderBy) { (result) in
            self.activityIndicator.stopAnimating()
            self.toogleCalendarButtons(true)
            self.view.isUserInteractionEnabled = true
            switch result {
            case .success(let statistics):
                self.didUpdateStatistics.notify(value: statistics)
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    // MARK: - UI Configuration
    
    func toogleCalendarButtons(_ isEnabled: Bool) {
        calendarBackButton.isEnabled = isEnabled
        calendarNextButton.isEnabled = isEnabled
        segmentedControl.isUserInteractionEnabled = isEnabled
    }
    
    func configureViewControllers() {
        children.forEach { (controller) in
            if controller is MovingStatisticsViewController {
                movingStatisticsViewController = controller as? MovingStatisticsViewController
                didUpdateStatistics.subscribe(with: self) { (self, statistics) in
                    self.movingStatisticsViewController.updateUI(statistics: statistics)
                }
                movingStatisticsViewController.onSelectionInteraction.subscribe(with: self) { (self, orderBy) in
                    self.loadData(fromDate: self.firstDayOfSelectedWeek, toDate: self.lastDayOfSelectedWeek, orderBy: orderBy)
                }
            } else if controller is StaysStatisticsViewController {
                staysStatisticsViewController = controller as? StaysStatisticsViewController
                didUpdateStatistics.subscribe(with: self) { (self, statistics) in
                    self.staysStatisticsViewController.updateUI(statistics: statistics)
                }
            } else if controller is ExpensesStatisticsViewController {
                expensesStatisticsViewController = controller as? ExpensesStatisticsViewController
                didUpdateStatistics.subscribe(with: self) { (self, statistics) in
                    self.expensesStatisticsViewController.updateUI(statistics: statistics)
                }
            }
        }
    }
    
    func configureSegmentedControl() {
        segmentedControl.defaultTextFont = Constants.Fonts.condensedRoboto.condensedRegular.with(size: 15)
        segmentedControl.selectedTextFont = Constants.Fonts.condensedRoboto.condensedRegular.with(size: 15)
        segmentedControl.hasBounceAnimation = true
        segmentedControl.allowChangeThumbWidth = false
        segmentedControl.itemTitles = [
            Texts.Statistics.moving.text,
            Texts.Statistics.stays.text,
            Texts.Statistics.expenses.text
        ]
        updateView()
        segmentedControl.didSelectItemWith = { index, title -> () in
            self.updateView()
        }
    }
    
    func configureDates() {
        firstDayOfSelectedWeek = Date().firstDayOfWeek
        lastDayOfSelectedWeek = firstDayOfSelectedWeek.lastDayOfWeek
    }
    
    func configureCalendarHeader() {
        calendarHeaderLabel.text = firstDayOfSelectedWeek.calendarHeader
        if lastDayOfSelectedWeek > Date() {
            calendarNextButton.alpha = 0.4
        } else {
            calendarNextButton.alpha = 1
        }
        if firstDayOfSelectedWeek < user.createDate {
            calendarBackButton.alpha = 0.4
        } else {
            calendarBackButton.alpha = 1
        }
    }
    
    func updateView() {
        movingContainerView.isHidden = !(segmentedControl.currentIndex == 0)
        staysContainerView.isHidden = !(segmentedControl.currentIndex == 1)
        expensesContainerView.isHidden = !(segmentedControl.currentIndex == 2)
    }

    // MARK: - Actions
    
    @IBAction func backAction(_ sender: UIControl) {
        popViewController()
    }
    
    @IBAction func calendarBackAction(_ sender: Any) {
        guard firstDayOfSelectedWeek > user.createDate else {
            return
        }
        firstDayOfSelectedWeek = Calendar.current.date(byAdding: .weekOfYear, value: -1, to: firstDayOfSelectedWeek)!
        lastDayOfSelectedWeek = firstDayOfSelectedWeek.lastDayOfWeek
        loadData(fromDate: firstDayOfSelectedWeek, toDate: lastDayOfSelectedWeek)
        configureCalendarHeader()
    }
    
    @IBAction func calendarNextAction(_ sender: Any) {
        guard lastDayOfSelectedWeek < Date() else {
            return
        }
        firstDayOfSelectedWeek = Calendar.current.date(byAdding: .weekOfYear, value: 1, to: firstDayOfSelectedWeek)!
        lastDayOfSelectedWeek = firstDayOfSelectedWeek.lastDayOfWeek
        loadData(fromDate: firstDayOfSelectedWeek, toDate: lastDayOfSelectedWeek)
        configureCalendarHeader()
    }
    
}
