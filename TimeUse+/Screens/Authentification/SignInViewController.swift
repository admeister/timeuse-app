//
//  SignInViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 21/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SignInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailView: ExtendedUIView!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailPlaceholderLabel: UILabel!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordView: ExtendedUIView!
    @IBOutlet weak var passwordPlaceholderLabel: UILabel!
    @IBOutlet weak var forgotPasswordView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var signInButton: ExtendedUIButton!
    @IBOutlet weak var resetItHereButton: UIButton!
    
    var email: String {
        return emailTextField.text!
    }
    var password: String {
        return passwordTextField.text!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurePlaceholder(enabled: false)
        configureInputVisibilityController()
        toogleSignInButton(enabled: false)
        configureTextFields()
        configureUI()
    }
    
    // MARK: - UI Configuration
    
    func configureUI() {
        resetItHereButton.setAttributedTitle(Texts.Button.resetPassword.text
                                                .attributed(style: TextStyles.attributes(Constants.Fonts.condensedRoboto.condensedRegular.with(size: 15), color: Constants.Colors.main.blue.value))
                                                .setting(style: TextStyles.underlineStyleAttributes(.single)), for: .normal)
    }
    
    func configureTextFields() {
        emailTextField.titleFormatter = { text in
            return text.capitalized
        }
        passwordTextField.title = Texts.Other.password.text
        passwordTextField.selectedTitle = Texts.Other.password.text
        passwordTextField.titleFormatter = { text in
            return text.capitalizingFirstLetter()
        }
    }
    
    func configureInputVisibilityController() {
        let keyboardController = self.view.addKeyboardVisibilityController()
        keyboardController.dismissKeyboardTouchRecognizer?.ignoreViews = [signInButton, emailView, passwordView, forgotPasswordView]
    }
    
    func toogleSignInButton(enabled: Bool) {
        signInButton.isEnabled = enabled
        signInButton.alpha = enabled ? 1.0 : 0.6
    }
    
    func configurePlaceholder(enabled: Bool) {
        emailPlaceholderLabel.isHidden = enabled
        passwordPlaceholderLabel.isHidden = enabled
    }
    
    // MARK: - Actions
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showPasswordAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func resetPasswordAction(_ sender: UIButton) {
        let controller = ResetPasswordViewController.instantiateFromAuthentificationStoryboard()
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func signInAction(_ sender: UIButton) {
        view.endEditing(true)
        
        guard email.isEmail() else {
            ErrorDisplay.showErrorToast(withMessage: Texts.Toast.incorrectEmail.text)
            return
        }
        
        guard password.isValidPassword() else {
            ErrorDisplay.showErrorToast(withMessage: Texts.Toast.incorrectPassword.text)
            return
        }
        
        activityIndicator.startAnimating()
        toogleSignInButton(enabled: false)
        
        API.loginWithEmail(email, password: password) { result in
            self.activityIndicator.stopAnimating()
            self.toogleSignInButton(enabled: true)
            switch result {
            case .success(_):
                MotionTagHelper.shared.start()
                self.showHomeScreen()
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    func showHomeScreen() {
        let controller = HomeViewController.instantiateFromHomeStoryboard()
        let navigation = UINavigationController(rootViewController: controller)
        self.view.window?.rootViewController = navigation
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
            return false
        case passwordTextField:
            passwordTextField.resignFirstResponder()
            return false
        default:
            break
        }
        
        return true
    }
    
    func setTextFields(textField: UITextField, enabled: Bool? = nil) {
        switch textField {
        case emailTextField:
            emailPlaceholderLabel.isHidden = true
            emailView.borderColor = Constants.Colors.main.blue.value
            if let enabled = enabled {
                emailTextField.setTitleVisible(enabled)
            }
        case passwordTextField:
            passwordPlaceholderLabel.isHidden = true
            passwordView.borderColor = Constants.Colors.main.blue.value
            if let enabled = enabled {
                passwordTextField.setTitleVisible(enabled)
            }
        default:
            break
        }
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        setTextFields(textField: sender)
        if !email.isEmpty && !password.isEmpty {
            toogleSignInButton(enabled: true)
        } else {
            toogleSignInButton(enabled: false)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setTextFields(textField: textField, enabled: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTextField:
            emailView.borderColor = Constants.Colors.main.lightGrey.value
            emailTextField.setTitleVisible(false)
            if email.isEmpty {
                emailPlaceholderLabel.isHidden = false
            } else {
                emailPlaceholderLabel.isHidden = true
            }
        case passwordTextField:
            passwordView.borderColor = Constants.Colors.main.lightGrey.value
            passwordTextField.setTitleVisible(false)
            if password.isEmpty {
                passwordPlaceholderLabel.isHidden = false
            } else {
                passwordPlaceholderLabel.isHidden = true
            }
        default:
            break
        }
    }
    
}
