//
//  SignUpViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 21/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SignUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailView: ExtendedUIView!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailPlaceholderLabel: UILabel!
    @IBOutlet weak var passwordView: ExtendedUIView!
    @IBOutlet weak var passwordPlaceholderLabel: UILabel!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var registrationCodeView: ExtendedUIView!
    @IBOutlet weak var registrationCodePlaceholderLabel: UILabel!
    @IBOutlet weak var registrationCodeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var signUpButton: ExtendedUIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var email: String {
        return emailTextField.text!
    }
    var password: String {
        return passwordTextField.text!
    }
    var registrationCode: String {
        return registrationCodeTextField.text!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurePlaceholder(enabled: false)
        configureInputVisibilityController()
        toogleSignUpButton(enabled: false)
        configureTextFields()
    }
    
    // MARK: - UI Configuration
    
    func configureTextFields() {
        emailTextField.titleFormatter = { text in
            return text.capitalized
        }
        passwordTextField.title = Texts.Other.password.text
        passwordTextField.selectedTitle = Texts.Other.password.text
        passwordTextField.titleFormatter = { text in
            return text.capitalizingFirstLetter()
        }
        registrationCodeTextField.title = Texts.Other.registrationCode.text
        registrationCodeTextField.selectedTitle = Texts.Other.registrationCode.text
        registrationCodeTextField.titleFormatter = { text in
            return text.capitalizingFirstLetter()
        }
    }
    
    func configureInputVisibilityController() {
        let keyboardController = self.view.addKeyboardVisibilityController()
        keyboardController.dismissKeyboardTouchRecognizer?.ignoreViews = [signUpButton, emailView, passwordView, registrationCodeView]
    }
    
    func toogleSignUpButton(enabled: Bool) {
        signUpButton.isEnabled = enabled
        signUpButton.alpha = enabled ? 1.0 : 0.6
    }
    
    func configurePlaceholder(enabled: Bool) {
        emailPlaceholderLabel.isHidden = enabled
        passwordPlaceholderLabel.isHidden = enabled
        registrationCodePlaceholderLabel.isHidden = enabled
    }
        
    // MARK: - Actions
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showPasswordAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func signUpAction(_ sender: UIButton) {
        view.endEditing(true)
        
        guard email.isEmail() else {
            ErrorDisplay.showErrorToast(withMessage: Texts.Toast.incorrectEmail.text)
            return
        }
        
        guard password.isValidPassword() else {
            ErrorDisplay.showErrorToast(withMessage: Texts.Toast.incorrectPassword.text)
            return
        }
        
        activityIndicator.startAnimating()
        toogleSignUpButton(enabled: false)
        
        API.registerWithEmail(email, password: password, registrationCode: registrationCode) { result in
            self.activityIndicator.stopAnimating()
            self.toogleSignUpButton(enabled: true)
            switch result {
            case .success(_):
                MotionTagHelper.shared.start()
                self.showHomeScreen()
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    func showHomeScreen() {
        let controller = HomeViewController.instantiateFromHomeStoryboard()
        let navigation = UINavigationController(rootViewController: controller)
        self.view.window?.rootViewController = navigation
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
            return false
        case passwordTextField:
            registrationCodeTextField.becomeFirstResponder()
            return false
        case registrationCodeTextField:
            registrationCodeTextField.resignFirstResponder()
            return false
        default:
            break
        }
        
        return true
    }
    
    func setTextFields(textField: UITextField, enabled: Bool? = nil) {
        switch textField {
        case emailTextField:
            emailPlaceholderLabel.isHidden = true
            emailView.borderColor = Constants.Colors.main.blue.value
            if let enabled = enabled {
                emailTextField.setTitleVisible(enabled)
            }
        case passwordTextField:
            passwordPlaceholderLabel.isHidden = true
            passwordView.borderColor = Constants.Colors.main.blue.value
            if let enabled = enabled {
                passwordTextField.setTitleVisible(enabled)
            }
        case registrationCodeTextField:
            registrationCodePlaceholderLabel.isHidden = true
            registrationCodeView.borderColor = Constants.Colors.main.blue.value
            if let enabled = enabled {
                registrationCodeTextField.setTitleVisible(enabled)
            }
        default:
            break
        }
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        setTextFields(textField: sender)
        if !email.isEmpty && !password.isEmpty && !registrationCode.isEmpty {
            toogleSignUpButton(enabled: true)
        } else {
            toogleSignUpButton(enabled: false)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setTextFields(textField: textField, enabled: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTextField:
            emailView.borderColor = Constants.Colors.main.lightGrey.value
            emailTextField.setTitleVisible(false)
            if email.isEmpty {
                emailPlaceholderLabel.isHidden = false
            } else {
                emailPlaceholderLabel.isHidden = true
            }
        case passwordTextField:
            passwordView.borderColor = Constants.Colors.main.lightGrey.value
            passwordTextField.setTitleVisible(false)
            if password.isEmpty {
                passwordPlaceholderLabel.isHidden = false
            } else {
                passwordPlaceholderLabel.isHidden = true
            }
        case registrationCodeTextField:
            registrationCodeView.borderColor = Constants.Colors.main.lightGrey.value
            registrationCodeTextField.setTitleVisible(false)
            if registrationCode.isEmpty {
                registrationCodePlaceholderLabel.isHidden = false
            } else {
                registrationCodePlaceholderLabel.isHidden = true
            }
        default:
            break
        }
    }
    
}
