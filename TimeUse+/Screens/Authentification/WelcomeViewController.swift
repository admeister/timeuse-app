//
//  WelcomeViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation

class WelcomeViewController: UIViewController {
    @IBOutlet weak var alreadyHaveAnAccountButton: UIButton!
    
    private lazy var permissions: PermissionsManager = {
        let manager = PermissionsManager()
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
        permissions.obtainLocationPermission()
        permissions.obtainMotionActivityPermission()
        configureUI()
    }
    
    // MARK: - UI Configuration
    func configureUI() {
        alreadyHaveAnAccountButton.setAttributedTitle(Texts.Button.alreadyHaveAnAccount.text
                                                        .attributed(style: TextStyles.attributes(Constants.Fonts.roboto.medium.with(size: 17), color: .white))
                                                        .setting(style: TextStyles.underlineStyleAttributes(.single)), for: .normal)
    }

    // MARK: - Actions
    
    @IBAction func getStartedAction(_ sender: Any) {
        let controller = SignUpViewController.instantiateFromAuthentificationStoryboard()
        navigationController?.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func alreadyHaveAnAccountAction(_ sender: Any) {
        let controller = SignInViewController.instantiateFromAuthentificationStoryboard()
        navigationController?.present(controller, animated: true, completion: nil)
    }
    
}
