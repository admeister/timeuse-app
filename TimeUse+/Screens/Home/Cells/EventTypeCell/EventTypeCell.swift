//
//  EventTypeCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 16/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class EventTypeCell: UICollectionViewCell {

    @IBOutlet weak var imageContainerView: ExtendedUIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var checkMarkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var bottomDividerLeadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bottomDividerLeadingConstraint.priority = UILayoutPriority(1000.0)
    }
}
