//
//  EventTypeCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 16/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

protocol EventTypeCellControllerProtocol: class {
    func didChangeEventType()
}

class EventTypeCellController: CollectionCellController<EventName, EventTypeCell> {
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 65.0))
    }
    
    var _parent: EventTypeViewController {
        return self.parentViewController as! EventTypeViewController
    }
    
    var allEvents: [EventName] {
        return _parent.dataSource?.content ?? []
    }
    
    weak var delegate: EventTypeCellControllerProtocol?
    
    override func configureCell(_ cell: EventTypeCell, for content: EventName, at indexPath: IndexPath) {
        cell.titleLabel.text = content.eventConfig.title
        cell.iconImageView.image = content.eventConfig.icon
        
        if !content.isLast {
            cell.bottomDividerLeadingConstraint.constant = 80
        } else {
            cell.bottomDividerLeadingConstraint.constant = 0
        }
        
        if content.isSelected {
            cell.imageContainerView.borderWidth = 0
            cell.iconImageView.tintColor = .white
            cell.checkMarkImageView.isHidden = false
            cell.titleLabel.font = Constants.Fonts.roboto.medium.with(size: 17)
            cell.imageContainerView.backgroundColor = content.eventConfig.color
        } else {
            cell.imageContainerView.borderWidth = 1
            cell.imageContainerView.backgroundColor = .white
            cell.checkMarkImageView.isHidden = true
            cell.titleLabel.font = Constants.Fonts.roboto.regular.with(size: 17)
            cell.iconImageView.tintColor = content.eventConfig.color
        }
    }
    
    override func didSelectContent(_ content: EventName, at indexPath: IndexPath, in collectionView: UICollectionView) {
        allEvents.forEach{ $0.isSelected = false }
        content.isSelected = !content.isSelected
        TemporaryEvent.shared.eventType = content.eventConfig
        delegate?.didChangeEventType()
        triggerReload()
    }
    
    func triggerReload() {
        _parent.collectionView.reloadData()
    }
}
