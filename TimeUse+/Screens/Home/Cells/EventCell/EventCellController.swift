//
//  EventCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 12/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class EventCellController: CollectionCellController<Event, EventCell> {
    
    var _parent: EventsViewController {
        return self.parentViewController as! EventsViewController
    }
    
    weak var delegate: EventsDelegate?
    weak var completedEventsDelegate: CompletedEventsProtocol?
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 80.0))
    }
    
    var allEvents: [Event] {
        return _parent.dataSource?.content.first ?? []
    }
    
    var lastSelectedEvent: Event!
    
    override func configureCell(_ cell: EventCell, for content: Event, at indexPath: IndexPath) {
        cell.event = content
        cell._parent = _parent
        cell.completedEventsDelegate = completedEventsDelegate
        cell.iconView.image = content.eventConfig.icon
        cell.titleLabel.text = content.eventConfig.title
        cell.titleLabel.textColor = content.eventConfig.titleColor
        cell.startTimeLabel.text = content.startDateTime
        cell.endTimeLabel.text = content.endDateTime
        cell.startTimeLabel.textColor = content.eventConfig.dateColor
        cell.endTimeLabel.textColor = content.eventConfig.dateColor
        
        if content.isFirst && content.isLast {
            cell.topDividerView.isHidden = true
            cell.bottomDividerView.isHidden = true
        } else if content.isFirst {
            cell.topDividerView.isHidden = true
            cell.bottomDividerView.isHidden = false
        } else if content.isLast {
            cell.topDividerView.isHidden = false
            cell.bottomDividerView.isHidden = true
        } else {
            cell.topDividerView.isHidden = false
            cell.bottomDividerView.isHidden = false
        }
        
        switch content.eventConfig {
        case .deleted:
            cell.checkMarkView.isHidden = true
            cell.addDetailsButton.isHidden = true
            cell.imageView.backgroundColor = .white
            cell.imageView.borderWidth = 1
            cell.contentView.backgroundColor = .clear
        default:
            cell.addDetailsButton.isHidden = false
            cell.addDetailsButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
            switch content.status {
            case .complete:
                cell.checkMarkView.isHidden = false
                cell.checkMarkView.tintColor = Constants.Colors.main.blue.value
                cell.imageView.backgroundColor = Constants.Colors.main.lightGrey.value
                cell.imageView.borderWidth = 0
                cell.iconView.tintColor = .white
                cell.addDetailsButton.setTitleColor(Constants.Colors.main.darkGrey.value, for: .normal)
                cell.addDetailsButton.setTitle(Texts.Button.edit.text, for: .normal)
                cell.addDetailsButton.borderColor = Constants.Colors.main.lightGrey.value
                cell.addDetailsButton.borderWidth = 1
                cell.addDetailsButton.backgroundColor = .white
                cell.titleLabel.textColor = Constants.Colors.main.dark.value
            case .incomplete:
                cell.checkMarkView.isHidden = true
                cell.addDetailsButton.setTitleColor(Constants.Colors.main.blue.value, for: .normal)
                cell.addDetailsButton.backgroundColor = Constants.Colors.main.lightBlue.value
                cell.addDetailsButton.setTitle(Texts.Button.addDetails.text, for: .normal)
                cell.addDetailsButton.borderWidth = 0
            }
            configureSelectedCell(cell: cell, content: content)
        }
    }
    
    private func configureSelectedCell(cell: EventCell, content: Event) {
        if content.isSelected {
            cell.contentView.backgroundColor = Constants.Colors.main.ultraLightGrey.value
            switch content.status {
            case .incomplete:
                cell.iconView.tintColor = .white
                cell.imageView.borderWidth = 0
                cell.imageView.backgroundColor = content.eventConfig.color
                cell.addDetailsButton.setTitleColor(.white, for: .normal)
                cell.addDetailsButton.backgroundColor = Constants.Colors.main.blue.value
            default:
                break
            }
        } else {
            cell.contentView.backgroundColor = .clear
            switch content.status {
            case .incomplete:
                cell.imageView.backgroundColor = .white
                cell.imageView.borderWidth = 1
                cell.iconView.tintColor = content.eventConfig.color
                cell.addDetailsButton.setTitleColor(Constants.Colors.main.blue.value, for: .normal)
                cell.addDetailsButton.backgroundColor = Constants.Colors.main.lightBlue.value
            default:
                break
            }
        }
    }
    
    override func didSelectContent(_ content: Event, at indexPath: IndexPath, in collectionView: UICollectionView) {
        
        guard content.eventConfig != .deleted && content.eventConfig != .untracked else {
            return
        }
        
        lastSelectedEvent = allEvents.first(where: { $0.isSelected })
        allEvents.forEach { $0.isSelected = false }
        if content == lastSelectedEvent {
            delegate?.displayEvents(allEvents)
            let cell = collectionView.cellForItem(at: indexPath) as! EventCell
            configureSelectedCell(cell: cell, content: content)
        } else {
            content.isSelected = !content.isSelected
            delegate?.didSelectEvent(content, color: content.eventConfig.color)
            triggerReload()
        }
    }
    
    func triggerReload() {
        _parent.collectionView.reloadData()
    }
}
