//
//  EventCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 12/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class EventCell: UICollectionViewCell {

    @IBOutlet weak var topDividerView: UIView!
    @IBOutlet weak var bottomDividerView: UIView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var imageView: ExtendedUIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var addDetailsButton: ExtendedUIButton!
    @IBOutlet weak var checkMarkView: ExtendedUIView!
    
    var event: Event!
    weak var completedEventsDelegate: CompletedEventsProtocol?
    var _parent: EventsViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func addDetailsAction(_ sender: Any) {
        
        switch event.eventConfig {
        case .untracked:
            let controller = UntrackedEventTypeViewController._init(event: event, delegate: completedEventsDelegate)
            parentViewController?.present(controller, animated: true, completion: nil)
        default:
            let controller = EventDetailsViewController._init(event: event, minEventDurationTime: _parent.minEventDurationTime, completedEventsDelegate: completedEventsDelegate)
            let navigationController = UINavigationController(rootViewController: controller)
            parentViewController?.present(navigationController, animated: true, completion: nil)
        }
    }
    
}
