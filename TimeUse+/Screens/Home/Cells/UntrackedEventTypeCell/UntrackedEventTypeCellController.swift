//
//  UntrackedEventTypeCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 11/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class UntrackedEventTypeCellController: CollectionCellController<EventName, UntrackedEventTypeCell> {
    
    var _parent: UntrackedEventTypeViewController {
        return self.parentViewController as! UntrackedEventTypeViewController
    }
    
    var allEvents: [EventName] {
        return (_parent.dataSource?.content.first ?? []) as! [EventName]
    }
    
    let onSelectionInteraction = Observable<EventName>()
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 65.0))
    }
    
    override func configureCell(_ cell: UntrackedEventTypeCell, for content: EventName, at indexPath: IndexPath) {
        cell.titleLabel.text = content.eventConfig.title
        cell.iconImageView.image = content.eventConfig.icon
        
        if content.isFirst {
            cell.topDividerView.isHidden = false
            cell.bottomDividerLeadingConstraint.constant = 80
        } else if content.isLast {
            cell.topDividerView.isHidden = true
            cell.bottomDividerLeadingConstraint.constant = 0
        } else {
            cell.topDividerView.isHidden = true
            cell.bottomDividerLeadingConstraint.constant = 80
        }
        
        if content.isSelected {
            cell.imageContainerView.borderWidth = 0
            cell.iconImageView.tintColor = .white
            cell.checkImageView.image = #imageLiteral(resourceName: "selected_check_mark")
            cell.imageContainerView.backgroundColor = content.eventConfig.color
        } else {
            cell.imageContainerView.borderWidth = 1
            cell.imageContainerView.backgroundColor = .white
            cell.checkImageView.image = #imageLiteral(resourceName: "unselected_check_mark")
            cell.iconImageView.tintColor = content.eventConfig.color
        }
    }
    
    override func didSelectContent(_ content: EventName, at indexPath: IndexPath, in collectionView: UICollectionView) {
        content.isSelected = !content.isSelected
        triggerReload()
        onSelectionInteraction.notify(value: content)
    }
    
    func triggerReload() {
        _parent.collectionView.reloadData()
    }
}
