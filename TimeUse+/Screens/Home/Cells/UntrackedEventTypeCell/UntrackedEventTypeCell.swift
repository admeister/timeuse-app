//
//  UntrackedEventTypeCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 11/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class UntrackedEventTypeCell: UICollectionViewCell {

    @IBOutlet weak var imageContainerView: ExtendedUIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet var bottomDividerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var topDividerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bottomDividerLeadingConstraint.priority = UILayoutPriority(1000.0)
    }
    
}
