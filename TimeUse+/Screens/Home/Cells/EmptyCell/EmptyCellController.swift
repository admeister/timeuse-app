//
//  EmptyCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 26/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

struct EmptyModel {
    
}

class EmptyCellController: CollectionCellController<EmptyModel, EmptyCell> {
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 16.0))
    }
    
    override func configureCell(_ cell: EmptyCell, for content: EmptyModel, at indexPath: IndexPath) {
    }
    
    override func didSelectContent(_ content: EmptyModel, at indexPath: IndexPath, in collectionView: UICollectionView) {
        
    }
}
