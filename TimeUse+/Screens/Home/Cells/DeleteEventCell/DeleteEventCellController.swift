//
//  DeleteEventCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 29/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

struct DeleteEventModel {
}

class DeleteEventCellController: CollectionCellController<DeleteEventModel, DeleteEventCell> {
    
    let onSelectionInteraction = Observable<Void>()
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 50.0))
    }
    
    override func configureCell(_ cell: DeleteEventCell, for content: DeleteEventModel, at indexPath: IndexPath) {
    }
    
    override func didSelectContent(_ content: DeleteEventModel, at indexPath: IndexPath, in collectionView: UICollectionView) {
        onSelectionInteraction.notify()
    }
}
