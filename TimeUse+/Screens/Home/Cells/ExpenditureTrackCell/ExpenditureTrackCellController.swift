//
//  ExpenditureTrackCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 26/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class ExpenditureTrackModel {
    init() {}
}

class ExpenditureTrackCellController: CollectionCellController<ExpenditureTrackModel, ExpenditureTrackCell> {
    
    var _parent: AddActivitiesViewController {
        return self.parentViewController as! AddActivitiesViewController
    }
    
    weak var delegate: AttributesSelectionProtocol?
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 72.0))
    }
    
    override func configureCell(_ cell: ExpenditureTrackCell, for content: ExpenditureTrackModel, at indexPath: IndexPath) {
        cell._parent = _parent
        cell.delegate = delegate
        cell.configureUI()
    }
    
    override func didSelectContent(_ content: ExpenditureTrackModel, at indexPath: IndexPath, in collectionView: UICollectionView) {
        
    }
}
