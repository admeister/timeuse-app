//
//  ExpenditureTrackCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 26/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

protocol AttributesSelectionProtocol: class {
    func didSelectAttribute()
    func didSelectMoneyAttribute()
}

class ExpenditureTrackCell: UICollectionViewCell {

    @IBOutlet weak var segmentedControl: CustomSegmentedControl!
    
    var _parent: AddActivitiesViewController!
    weak var delegate: AttributesSelectionProtocol?
    var currentIndex: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
     
     func configureUI() {
        let itemTitles = _parent.presets.expenditures.firstSlotValues
        segmentedControl.itemTitles = itemTitles
        let activity = TemporaryEvent.shared.activities.first { (activity) -> Bool in
            return activity.id == _parent.activity.id
        }
        if let attribute = activity?.attribute, let title = attribute.expenditures.first, _parent.temporaryAttribute.expenditures.isEmpty {
            selectItem(title, titles: itemTitles)
        } else if !_parent.temporaryAttribute.expenditures.isEmpty {
            selectItem(_parent.temporaryAttribute.expenditures.first!, titles: itemTitles)
        } else {
            segmentedControl.selectItemAt(index: 0)
            currentIndex = 0
        }
        addAttribute(title: itemTitles[currentIndex])
        segmentedControl.didSelectItemWith = { index, title -> () in
            self.addAttribute(title: title!)
        }
     }
    
    func selectItem(_ title: String, titles: [String]) {
        if let index = titles.firstIndex(of: title) {
            currentIndex = index
            segmentedControl.selectItemAt(index: index)
        }
    }
    
    func addAttribute(title: String) {
        _parent.temporaryAttribute.expenditures.removeAll()
        _parent.temporaryAttribute.expenditures.append(title)
        delegate?.didSelectAttribute()
    }
}
