//
//  ExpenditureStayCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 26/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class ExpenditureStayCell: UICollectionViewCell {
    
    @IBOutlet weak var peopleSegmentedControl: CustomSegmentedControl!
    @IBOutlet weak var needsSegmentedControl: CustomSegmentedControl!
    @IBOutlet weak var requiredSegmentedControl: CustomSegmentedControl!
    
    var _parent: AddActivitiesViewController!
    weak var delegate: AttributesSelectionProtocol?
    
    var peopleItemTitles: [String] = []
    var dailyNeedsTitles: [String] = []
    var requiredTitles: [String] = []
    var peopleSet: Set<String>!
    var needsSet: Set<String>!
    var requiredSet: Set<String>!
    var peopleCurrentIndex: Int!
    var needsCurrentIndex: Int!
    var requiredCurrentIndex: Int!
    
    func configureUI() {
        peopleItemTitles = _parent.presets.expenditures.firstSlotValues
        dailyNeedsTitles = _parent.presets.expenditures.secondSlotValues
        requiredTitles = _parent.presets.expenditures.thirdSlotValues
        
        needsSegmentedControl.isHidden = dailyNeedsTitles.isEmpty
        requiredSegmentedControl.isHidden = requiredTitles.isEmpty
        
        peopleSet = Set(peopleItemTitles)
        peopleSegmentedControl.itemTitles = peopleItemTitles
        
        needsSet = Set(dailyNeedsTitles)
        if !dailyNeedsTitles.isEmpty {
            needsSegmentedControl.itemTitles = dailyNeedsTitles
        }
        
        requiredSet = Set(requiredTitles)
        if !requiredTitles.isEmpty {
            requiredSegmentedControl.itemTitles = requiredTitles
        }
        
        let activity = TemporaryEvent.shared.activities.first { (activity) -> Bool in
            return activity.id == _parent.activity.id
        }
        if let attribute = activity?.attribute, !attribute.expenditures.isEmpty, _parent.temporaryAttribute.expenditures.isEmpty {
            configureControls(titles: attribute.expenditures)
        } else if !_parent.temporaryAttribute.expenditures.isEmpty {
            configureControls(titles: _parent.temporaryAttribute.expenditures)
        } else {
            peopleSegmentedControl.selectItemAt(index: 0)
            needsSegmentedControl.selectItemAt(index: 0)
            requiredSegmentedControl.selectItemAt(index: 0)
            peopleCurrentIndex = 0
            needsCurrentIndex = 0
            requiredCurrentIndex = 0
        }
        addAttribute(title: peopleItemTitles[peopleCurrentIndex])
        if !dailyNeedsTitles.isEmpty {
            addAttribute(title: dailyNeedsTitles[needsCurrentIndex])
        }
        if !requiredTitles.isEmpty {
            addAttribute(title: requiredTitles[requiredCurrentIndex])
        }
        
        peopleSegmentedControl.didSelectItemWith = { index, title -> () in
            self.addAttribute(title: title!)
        }
        needsSegmentedControl.didSelectItemWith = { index, title -> () in
            self.addAttribute(title: title!)
        }
        requiredSegmentedControl.didSelectItemWith = { index, title -> () in
            self.addAttribute(title: title!)
        }
    }
    
    func configureControls(titles: [String]) {
        if !peopleSet.isDisjoint(with: titles) {
            let title = peopleSet.intersection(titles).first!
            peopleCurrentIndex = selectItem(title, titles: peopleItemTitles, segmentedControl: peopleSegmentedControl)
        }
        if !needsSet.isDisjoint(with: titles) {
            let title = needsSet.intersection(titles).first!
            needsCurrentIndex = selectItem(title, titles: dailyNeedsTitles, segmentedControl: needsSegmentedControl)
        }
        if !requiredSet.isDisjoint(with: titles) {
            let title = requiredSet.intersection(titles).first!
            requiredCurrentIndex = selectItem(title, titles: requiredTitles, segmentedControl: requiredSegmentedControl)
        }
    }
    
    func selectItem(_ title: String, titles: [String], segmentedControl: CustomSegmentedControl) -> Int {
        if let index = titles.firstIndex(of: title) {
            segmentedControl.selectItemAt(index: index)
            return index
        }
        return 0
    }
    
    func addAttribute(title: String) {
        if _parent.temporaryAttribute.expenditures.count > 2 {
            if peopleSet.contains(title) {
                _parent.temporaryAttribute.expenditures.remove(object: peopleSet.intersection(_parent.temporaryAttribute.expenditures).first!)
            } else if needsSet.contains(title) {
                _parent.temporaryAttribute.expenditures.remove(object: needsSet.intersection(_parent.temporaryAttribute.expenditures).first!)
            } else if requiredSet.contains(title) {
                _parent.temporaryAttribute.expenditures.remove(object: requiredSet.intersection(_parent.temporaryAttribute.expenditures).first!)
            }
        }
        _parent.temporaryAttribute.expenditures.append(title)
        delegate?.didSelectAttribute()
    }
}
