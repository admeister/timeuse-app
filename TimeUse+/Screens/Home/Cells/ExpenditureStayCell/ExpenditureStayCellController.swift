//
//  ExpenditureStayCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 26/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class ExpenditureStayModel {
    init() {}
}

class ExpenditureStayCellController: CollectionCellController<ExpenditureStayModel, ExpenditureStayCell> {
    
    var _parent: AddActivitiesViewController {
        return self.parentViewController as! AddActivitiesViewController
    }
    
    weak var delegate: AttributesSelectionProtocol?
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 168.0))
    }
    
    override func cellSize(for content: ExpenditureStayModel, in collectionView: UICollectionView) -> CGSize {
        var size = super.cellSize
        let values = _parent.presets.expenditures.allValues
        
        if values.count == 2 {
            size.height = 74
        } else if values.count == 4 {
            size.height = 120
        }
        
        return size
    }
    
    override func configureCell(_ cell: ExpenditureStayCell, for content: ExpenditureStayModel, at indexPath: IndexPath) {
        cell._parent = _parent
        cell.delegate = delegate
        cell.configureUI()
    }
    
    override func didSelectContent(_ content: ExpenditureStayModel, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
}
