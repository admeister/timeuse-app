//
//  EventHeaderController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 12/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude

class EventHeaderController: CollectionHeaderController<String, EventHeaderCell> {
    init() {
        super.init(headerSize: CGSize(width: -1.0, height: 50.0))
    }
    
    override func configureHeader(_ header: EventHeaderCell, for content: String, at indexPath: IndexPath) {
        header.titleLabel.text = content
    }
}
