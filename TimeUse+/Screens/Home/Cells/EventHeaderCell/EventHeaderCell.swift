//
//  EventHeaderCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 12/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude

class EventHeaderCell: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    
}