//
//  ActivityHeaderController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 15/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude

class ActivityHeaderController: CollectionHeaderController<String, ActivityHeaderCell> {
    init() {
        super.init(headerSize: CGSize(width: -1.0, height: 93.0))
    }
    
    override func configureHeader(_ header: ActivityHeaderCell, for content: String, at indexPath: IndexPath) {
    }
}
