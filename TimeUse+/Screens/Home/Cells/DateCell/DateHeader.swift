//
//  DateHeader.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 07/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DateHeader: JTACMonthReusableView  {
    @IBOutlet weak var monthTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

