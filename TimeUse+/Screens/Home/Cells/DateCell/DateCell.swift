//
//  DayCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 06/07/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DateCell: JTACDayCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var markIconImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = containerView.frame.width / 2
    }
}
