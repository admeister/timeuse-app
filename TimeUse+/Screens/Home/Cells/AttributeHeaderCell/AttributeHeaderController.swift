//
//  AttributeHeaderController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 18/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude

class BaseHeaderSection {
    enum AttributeSectionType {
        case time
        case participants
        case money
        case expenditureStay
        case expenditureTrack
        case stay
        case track
        
        var title: String {
            switch self {
            case .time:
                return Texts.Section.time.text
            case .participants:
                return Texts.Section.participants.text
            case .money:
                return Texts.Section.money.text
            case .expenditureStay, .expenditureTrack:
                return Texts.Section.expenditure.text
            case .stay:
                return Texts.Section.stay.text
            case .track:
                return Texts.Section.track.text
            }
        }
    }
    
    var value: String
    
    init(value: String) {
        self.value = value
    }
    
}

class AttributeHeaderController: CollectionHeaderController<BaseHeaderSection, AttributeHeaderCell> {
    init() {
        super.init(headerSize: CGSize(width: -1.0, height: 50.0))
    }
    
    override func configureHeader(_ header: AttributeHeaderCell, for content: BaseHeaderSection, at indexPath: IndexPath) {
        header.titleLabel.text = content.value
    }
}
