//
//  AttributeHeaderCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 18/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude

class AttributeHeaderCell: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    
}