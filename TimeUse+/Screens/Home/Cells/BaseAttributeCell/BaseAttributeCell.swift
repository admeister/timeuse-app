//
//  TimeCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 18/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class BaseAttributeCell: UICollectionViewCell {
    
    @IBOutlet weak var attributeLabel: UILabel!
    @IBOutlet weak var containerView: ExtendedUIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
