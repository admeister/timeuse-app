//
//  TimeCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 18/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class BaseAttributeCellController<C: AttributeProtocol>: CollectionCellController<C, BaseAttributeCell> {
    
    var _parent: CustomCollectionFeedController {
        return self.parentViewController as! CustomCollectionFeedController
    }
    
    init() {
        super.init(cellSize: CGSize(width: 80.0, height: 80.0))
    }
    
    override func configureCell(_ cell: BaseAttributeCell, for content: C, at indexPath: IndexPath) {
        if content.isSelected {
            cell.containerView.borderColor = Constants.Colors.main.blue.value
            cell.containerView.backgroundColor = Constants.Colors.main.lightBlue.value
            cell.attributeLabel.textColor = Constants.Colors.main.blue.value
            cell.descriptionLabel.textColor = Constants.Colors.main.blue.value
        } else {
            cell.containerView.borderColor = Constants.Colors.main.lightGrey.value
            cell.containerView.backgroundColor = .white
            cell.attributeLabel.textColor = Constants.Colors.main.dark.value
            cell.descriptionLabel.textColor = Constants.Colors.main.darkGrey.value
        }
    }
    
    override func didSelectContent(_ content: C, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
    
    func triggerReload() {
        _parent.collectionView.reloadData()
    }
}

protocol AttributeProtocol {
    var isSelected: Bool { get set }
}

class TimeModel: AttributeProtocol {
    
    var isSelected: Bool
    var time: Int // upper value from time interval
    var type: String
    var timeInterval: Int
    private let hour = 60
    
    private func hourMinutes(_ time: Int) -> String {
        if time % hour == 0 {
            return "00"
        }
        return "\(time % hour)"
    }
    
    var timeString: String {
        if time == timeInterval {
            return "<\(timeInterval)"
        }
        if time >= hour + timeInterval {
            let previousTime = time - timeInterval
            return "\(time / hour):\(hourMinutes(time))-\(previousTime / hour):\(hourMinutes(previousTime))"
        }
        return "\(time)-\(time - timeInterval)"
    }
    
    init(time: Int, type: String, timeInterval: Int, isSelected: Bool = false) {
        self.type = type
        self.time = time
        self.timeInterval = timeInterval
        self.isSelected = isSelected
    }
}

class TimeCellController: BaseAttributeCellController<TimeModel> {
    
    var allTimeAttributes: [TimeModel] {
        return _parent.timeModels
    }
    
    let onSelectionInteraction = Observable<TimeModel>()
    
    override func cellSize(for content: TimeModel, in collectionView: UICollectionView) -> CGSize {
        var size = super.cellSize
        let hour = 60
        guard content.time > hour else {
            return size
        }
        let cell = self.sizeCalculationCell!
        size.width -= cell.attributeLabel.frame.width
        self.configureCell(cell, for: content, at: IndexPath(item: 0, section: 0))
        size.width += cell.attributeLabel.intrinsicContentSize.width
        return size
    }
    
    override func configureCell(_ cell: BaseAttributeCell, for content: TimeModel, at indexPath: IndexPath) {
        super.configureCell(cell, for: content, at: indexPath)
        cell.attributeLabel.text = content.timeString
        cell.descriptionLabel.text = content.type
    }
    
    override func didSelectContent(_ content: TimeModel, at indexPath: IndexPath, in collectionView: UICollectionView) {
        allTimeAttributes.forEach { $0.isSelected = false }
        content.isSelected = !content.isSelected
        triggerReload()
        onSelectionInteraction.notify(value: content)
    }
}

class MoneyModel: AttributeProtocol {
    
    var isSelected: Bool
    var value: Int
    var currency: String
    var moneyInterval: Int
    
    var valueString: String {
        guard value != 0 else {
            return "0"
        }
        return "\(value)-\(value + moneyInterval)"
    }
    
    init(value: Int, currency: String, moneyInterval: Int, isSelected: Bool = false) {
        self.currency = currency
        self.value = value
        self.moneyInterval = moneyInterval
        self.isSelected = isSelected
    }
}

class MoneyCellController: BaseAttributeCellController<MoneyModel> {
    
    var allMoneyAttributes: [MoneyModel] {
        return _parent.moneyModels
    }
    
    let onSelectionInteraction = Observable<MoneyModel>()
    
    override func configureCell(_ cell: BaseAttributeCell, for content: MoneyModel, at indexPath: IndexPath) {
        super.configureCell(cell, for: content, at: indexPath)
        cell.descriptionLabel.text = content.currency
        cell.attributeLabel.text = content.valueString
    }
    
    override func didSelectContent(_ content: MoneyModel, at indexPath: IndexPath, in collectionView: UICollectionView) {
        allMoneyAttributes.forEach { $0.isSelected = false }
        content.isSelected = !content.isSelected
        triggerReload()
        onSelectionInteraction.notify(value: content)
    }
}
