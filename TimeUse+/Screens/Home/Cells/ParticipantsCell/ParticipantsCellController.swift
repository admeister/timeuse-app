//
//  ParticipantsCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class Participants {
    init() {}
}

class ParticipantsCellController: CollectionCellController<Participants, ParticipantsCell> {
    
    var _parent: AddActivitiesViewController {
        return self.parentViewController as! AddActivitiesViewController
    }
    
    weak var delegate: AttributesSelectionProtocol?
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 88.0))
    }
    
    override func configureCell(_ cell: ParticipantsCell, for content: Participants, at indexPath: IndexPath) {
        cell._parent = _parent
        cell.delegate = delegate
        cell.configureUI()
    }
    
    override func didSelectContent(_ content: Participants, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
}
