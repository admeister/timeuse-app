//
//  ParticipantsCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import TTSegmentedControl

class CustomSegmentedControl: TTSegmentedControl {
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    func configureUI() {
        self.defaultTextFont = Constants.Fonts.condensedRoboto.condensedBold.with(size: 15)
        self.selectedTextFont = Constants.Fonts.condensedRoboto.condensedBold.with(size: 15)
        self.hasBounceAnimation = true
        self.allowChangeThumbWidth = false
    }
}

class ParticipantsCell: UICollectionViewCell {
    
    @IBOutlet weak var segmentedControl: CustomSegmentedControl!
    
    var _parent: AddActivitiesViewController!
    weak var delegate: AttributesSelectionProtocol?
    var currentIndex: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureUI() {
        let titles = _parent.presets.partners.values
        let itemTitles = _parent.presets.partners.displayedValues
        segmentedControl.itemTitles = itemTitles
        let activity = TemporaryEvent.shared.activities.first { (activity) -> Bool in
            return activity.id == _parent.activity.id
        }
        if let attribute = activity?.attribute, let title = attribute.partners, _parent.temporaryAttribute.partners == nil {
            selectItem(title, titles: titles)
        } else if _parent.temporaryAttribute.partners != nil {
            selectItem(_parent.temporaryAttribute.partners, titles: titles)
        } else {
            segmentedControl.selectItemAt(index: 0)
            currentIndex = 0
        }
        addAttribute(title: titles[currentIndex])
        segmentedControl.didSelectItemWith = { index, title -> () in
            self.addAttribute(title: title!.replacingOccurrences(of: "\n", with: " "))
        }
    }
    
    func selectItem(_ title: String, titles: [String]) {
        if let index = titles.firstIndex(of: title) {
            currentIndex = index
            segmentedControl.selectItemAt(index: index)
        }
    }
    
    func addAttribute(title: String) {
        _parent.temporaryAttribute.partners = title
        delegate?.didSelectAttribute()
    }
}
