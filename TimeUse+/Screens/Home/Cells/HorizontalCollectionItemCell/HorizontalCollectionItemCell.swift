//
//  TimeAttributeCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 18/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class HorizontalCollectionItemCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var feedController = CustomCollectionFeedController()
    
    func configureCell() {
        feedController.collectionView = self.collectionView
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class CustomCollectionFeedController: __CollectionFeedController {
    
    var timeModels: [TimeModel] = []
    var moneyModels: [MoneyModel] = []
}
