//
//  TimeAttributeCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 18/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class HorizontalCollectionItemCellController<C>: CollectionCellController<C, HorizontalCollectionItemCell> {
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 112.0))
    }
    
    override func configureCell(_ cell: HorizontalCollectionItemCell, for content: C, at indexPath: IndexPath) {
        cell.configureCell()
    }
    
    override func didSelectContent(_ content: C, at indexPath: IndexPath, in collectionView: UICollectionView) {
    }
}

class TimeAttributeModel {
    var event: Event
    
    init(event: Event) {
        self.event = event
    }
}

class TimeAttributeCellController: HorizontalCollectionItemCellController<TimeAttributeModel> {
    
    var _parent: AddActivitiesViewController {
        return self.parentViewController as! AddActivitiesViewController
    }
    weak var delegate: AttributesSelectionProtocol?
    
    override func configureCell(_ cell: HorizontalCollectionItemCell, for content: TimeAttributeModel, at indexPath: IndexPath) {
        cell.feedController._cellController = TimeCellController()
        let cellControler: TimeCellController = cell.feedController._cellController as! TimeCellController
        configureObservable(cell: cellControler)
    
        var duration = content.event.upperDuration - TemporaryEvent.shared.duration
        if let attribute = _parent.activity.attribute {
            duration += attribute.duration
        }
        let timeModels = Array(configureTimeModels(duration: duration).reversed())
        cell.feedController.timeModels = timeModels
        cell.feedController._dataSource = DataSource(timeModels)
        super.configureCell(cell, for: content, at: indexPath)
    }
    
    func configureObservable(cell: TimeCellController) {
        cell.onSelectionInteraction.subscribe(with: self) { (self, timeModel) in
            self._parent.temporaryAttribute.duration = timeModel.time
            self.delegate?.didSelectAttribute()
        }
    }
    
    private func configureTimeModels(duration: Int) -> [TimeModel] {
        let timeInterval = _parent.presets.duration.value
        var minutesString = "min"
        var timeModels: [TimeModel] = []
        let activity = TemporaryEvent.shared.activities.first { (activity) -> Bool in
            return activity.id == _parent.activity.id
        }
        
        for index in stride(from: 0, to: duration, by: timeInterval) {
            if index >= 60 {
                minutesString = "h:min"
            }
            
            let value = index + timeInterval
            if ((value == activity?.attribute?.duration || (value == timeInterval && value == duration)) && _parent.temporaryAttribute.duration == nil) || value == _parent.temporaryAttribute.duration {
                let timeModel = TimeModel(time: value, type: minutesString, timeInterval: timeInterval, isSelected: true)
                timeModels.append(timeModel)
                _parent.temporaryAttribute.duration = timeModel.time
                delegate?.didSelectAttribute()
            } else {
                timeModels.append(TimeModel(time: value, type: minutesString, timeInterval: timeInterval))
            }
        }
        return timeModels
    }
}

class MoneyAttributeModel {
    var event: Event
    
    init(event: Event) {
        self.event = event
    }
}

class MoneyAttributeCellController: HorizontalCollectionItemCellController<MoneyAttributeModel> {
    
    var _parent: AddActivitiesViewController {
        return self.parentViewController as! AddActivitiesViewController
    }
    weak var delegate: AttributesSelectionProtocol?
    
    override func configureCell(_ cell: HorizontalCollectionItemCell, for content: MoneyAttributeModel, at indexPath: IndexPath) {
        cell.feedController._cellController = MoneyCellController()
        let cellControler: MoneyCellController = cell.feedController._cellController as! MoneyCellController
        configureObservable(cell: cellControler)
        
        let moneyModels = configureMoneyModels()
        cell.feedController.moneyModels = moneyModels
        cell.feedController._dataSource = DataSource(moneyModels)
        super.configureCell(cell, for: content, at: indexPath)
    }
    
    func configureObservable(cell: MoneyCellController) {
        cell.onSelectionInteraction.subscribe(with: self) { (self, moneyModel) in
            self._parent.temporaryAttribute.budget = moneyModel.value
            self.delegate?.didSelectAttribute()
            self.delegate?.didSelectMoneyAttribute()
        }
    }
    
    private func configureMoneyModels() -> [MoneyModel] {
        var moneyModels: [MoneyModel] = []
        let minValue = _parent.presets.budget.value.min
        let maxValue = _parent.presets.budget.value.max
        let moneyInterval = _parent.presets.budget.value.interval
        let firstSlot = 0
        
        addMoneyModel(value: firstSlot, moneyModels: &moneyModels, moneyInterval: moneyInterval)
        
        for index in stride(from: minValue, to: maxValue, by: moneyInterval) {
            addMoneyModel(value: index, moneyModels: &moneyModels, moneyInterval: moneyInterval)
        }
        return moneyModels
    }
    
    private func addMoneyModel(value: Int, moneyModels: inout [MoneyModel], moneyInterval: Int) {
        let currency = Constants.Currency.chf
        let activity = TemporaryEvent.shared.activities.first { (activity) -> Bool in
            return activity.id == _parent.activity.id
        }
        
        if (value == activity?.attribute?.budget && _parent.temporaryAttribute.budget == nil) || value == _parent.temporaryAttribute.budget {
            let moneyModel = MoneyModel(value: value, currency: currency, moneyInterval: moneyInterval, isSelected: true)
            moneyModels.append(moneyModel)
            _parent.temporaryAttribute.budget = moneyModel.value
            delegate?.didSelectAttribute()
        } else {
            moneyModels.append(MoneyModel(value: value, currency: currency, moneyInterval: moneyInterval))
        }
    }
}
