//
//  ActivityCellController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 15/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude
import Kingfisher

class BaseActivityCellController: CollectionCellController<EventActivity, ActivityCell> {
    
    var onSelectionInteraction = Observable<Void>()
    
    init() {
        super.init(cellSize: CGSize(width: -1.0, height: 57.0))
    }
    
    var _parent: EventDetailsViewController {
        return self.parentViewController as! EventDetailsViewController
    }
    
    override func configureCell(_ cell: ActivityCell, for content: EventActivity, at indexPath: IndexPath) {
        onSelectionInteraction.bind(to: cell.onSelectionInteraction)
        
        cell.activity = content
        cell.event = _parent.event
        cell._parent = _parent
        cell.titleLabel.text = content.name
        cell.iconImageView.setImageFromURLString("\(Constants.BASE_URL)\(content.icon)", placeholder: nil, cacheKey: content.id)
        configureDividersUI(cell: cell, content: content, value: 56)
        cell.configureState(selected: content.isSelected)
        cell.blurView.isHidden = _parent.event.duration > _parent.minEventDurationTime
        cell.isUserInteractionEnabled = _parent.event.duration > _parent.minEventDurationTime
    }
    
    func configureDividersUI(cell: ActivityCell, content: EventActivity, value: CGFloat) {
        if content.isFirst, content.isLast {
            cell.topDividerView.isHidden = false
            cell.bottomDividerView.isHidden = false
            cell.bottomDividerLeadingConstraint.constant = 0
        } else if content.isFirst {
            cell.topDividerView.isHidden = false
            cell.bottomDividerLeadingConstraint.constant = value
        } else if content.isLast {
            cell.topDividerView.isHidden = true
            cell.bottomDividerView.isHidden = false
            cell.bottomDividerLeadingConstraint.constant = 0
        } else {
            cell.topDividerView.isHidden = true
            cell.bottomDividerLeadingConstraint.constant = value
        }
    }
    
    override func didSelectContent(_ content: EventActivity, at indexPath: IndexPath, in collectionView: UICollectionView) {
        switch content.type {
        case .general:
            let cell = collectionView.cellForItem(at: indexPath) as! ActivityCell
            cell.presentActivitiesScreen()
        case .specific:
            switch content.isSelected {
            case true:
                TemporaryEvent.shared.removeActivity(content)
                _parent.didAddActivity()
            case false:
                TemporaryEvent.shared.addActivity(content)
                _parent.didAddActivity()
            }
        }
    }
}
