//
//  ActivityCell.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 15/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit

class ActivityCell: UICollectionViewCell {

    @IBOutlet weak var topDividerView: UIView!
    @IBOutlet weak var bottomDividerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var blurView: UIView!
    
    @IBOutlet var iconContainerViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabelLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var bottomDividerLeadingConstraint: NSLayoutConstraint!
    
    var activity: EventActivity!
    var event: Event!
    var _parent: EventDetailsViewController!
    
    let onSelectionInteraction = Observable<Void>()
    
    func configureState(selected: Bool) {
        switch selected {
        case true:
            titleLabel.textColor = Constants.Colors.main.blue.value
            titleLabel.font = Constants.Fonts.roboto.medium.with(size: 17)
            checkButton.setImage(#imageLiteral(resourceName: "selected_check_mark"), for: .normal)
        case false:
            titleLabel.textColor = Constants.Colors.main.dark.value
            titleLabel.font = Constants.Fonts.roboto.regular.with(size: 17)
            checkButton.setImage(#imageLiteral(resourceName: "unselected_check_mark"), for: .normal)
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bottomDividerLeadingConstraint.priority = UILayoutPriority(1000.0)
        iconContainerViewWidthConstraint.priority = UILayoutPriority(1000.0)
        titleLabelLeadingConstraint.priority = UILayoutPriority(1000.0)
    }
    
    // MARK: - Actions
    
    @IBAction func checkButtonAction(_ sender: UIButton) {
        switch activity.type {
        case .specific:
            switch activity.isSelected {
            case true:
                TemporaryEvent.shared.removeActivity(activity)
                onSelectionInteraction.notify()
            case false:
                TemporaryEvent.shared.addActivity(activity)
                onSelectionInteraction.notify()
            }
        case .general:
            if activity.isSelected {
                TemporaryEvent.shared.removeActivity(activity)
                onSelectionInteraction.notify()
            } else {
                presentActivitiesScreen()
            }
        }
    }
    
    func presentActivitiesScreen() {
        guard activity.attribute != nil || (TemporaryEvent.shared.duration != event.upperDuration && activity.attribute == nil) else {
            ErrorDisplay.showErrorToast(withMessage: Texts.Toast.durationLimitReached.text)
            return
        }
        guard let presets = _parent.presets else {
            ErrorDisplay.showErrorToast(withMessage: Texts.Toast.errorUnknown.text)
            return
        }
        let controller = AddActivitiesViewController._init(event: event, activity: activity, delegate: _parent, presets: presets)
        _parent.present(controller, animated: true, completion: nil)
    }
    
}
