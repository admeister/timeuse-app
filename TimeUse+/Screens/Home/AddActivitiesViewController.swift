//
//  ActivitiesViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 18/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Tapptitude
import UIKit

class AddActivitiesViewController: __CollectionFeedController, AttributesSelectionProtocol {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var dataSource: SectionedDataSource<Any>! {
        didSet { _dataSource = dataSource }
    }
    
    var cellController: MultiCollectionCellController! {
        didSet { _cellController = cellController }
    }
    
    var event: Event!
    var activity: EventActivity!
    var temporaryAttribute: Attribute!
    weak var delegate: EventDetailsProtocol?
    var presets: Presets!
    var content: [[Any]] = []
    var sections: [BaseHeaderSection] = []
    
    static func _init(event: Event, activity: EventActivity, delegate: EventDetailsProtocol, presets: Presets) -> AddActivitiesViewController {
        let controller = AddActivitiesViewController.instantiateFromHomeStoryboard()
        controller.event = event
        controller.activity = activity
        controller.delegate = delegate
        controller.presets = presets
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: true)
        
        temporaryAttribute = Attribute(settings: activity.settings)
        
        headerController = AttributeHeaderController()
        headerIsSticky = true
        configureCellControllers()
        configureUI()
        reloadDataSource()
    }
    
    func configureCellControllers() {
        let timeAttributeCellController = TimeAttributeCellController()
        timeAttributeCellController.delegate = self
        let participantsCellController = ParticipantsCellController()
        participantsCellController.delegate = self
        let moneyAttributeCellController = MoneyAttributeCellController()
        moneyAttributeCellController.delegate = self
        let expenditureStayCellController = ExpenditureStayCellController()
        expenditureStayCellController.delegate = self
        let expenditureTrackCellController = ExpenditureTrackCellController()
        expenditureTrackCellController.delegate = self
        cellController = MultiCollectionCellController([timeAttributeCellController,
                                                        participantsCellController,
                                                        moneyAttributeCellController,
                                                        expenditureStayCellController,
                                                        expenditureTrackCellController])
    }
    
    // MARK: - UI Configuration
    
    func configureUI() {
        titleLabel.text = activity.name
        
        if activity.attribute != nil {
            toogleSaveButton(enabled: true)
        } else {
            toogleSaveButton(enabled: false)
        }
    }
    
    func toogleSaveButton(enabled: Bool) {
        saveButton.isEnabled = enabled
        saveButton.alpha = enabled ? 1 : 0.4
    }
    
    // MARK: - Data Loading
    
    func reloadDataSource() {
        if activity.settings.isDurationActive {
            addTimeSection(to: &content, with: &sections)
        }
        if activity.settings.isPartnersActive {
            addParticipantsSection(to: &content, with: &sections)
        }
        if activity.settings.isBudgetActive {
            addMoneySection(to: &content, with: &sections)
        }
        if activity.settings.isExpendituresActive, activity.attribute?.budget != 0 {
            addExpenditureSection(to: &content, with: &sections)
        }
        
        dataSource = SectionedDataSource(content)
        dataSource.sectionHeaders = sections
    }
    
    private func addTimeSection(to content:inout [[Any]], with sections:inout [BaseHeaderSection]) {
        sections.append(BaseHeaderSection(value: presets.duration.title))
        content.append([TimeAttributeModel(event: event)])
    }
    
    private func addParticipantsSection(to content:inout [[Any]], with sections:inout [BaseHeaderSection]) {
        sections.append(BaseHeaderSection(value: presets.partners.title))
        content.append([Participants()])
    }
    
    private func addMoneySection(to content:inout [[Any]], with sections:inout [BaseHeaderSection]) {
        sections.append(BaseHeaderSection(value: presets.budget.title))
        content.append([MoneyAttributeModel(event: event)])
    }
    
    private func addExpenditureSection(to content:inout [[Any]], with sections:inout [BaseHeaderSection]) {
        sections.append(BaseHeaderSection(value: presets.expenditures.title))
        switch event.type {
        case .stay:
            content.append([ExpenditureStayModel()])
        case .track:
            content.append([ExpenditureTrackModel()])
        case .untracked:
            break
        }
    }

    // MARK: - Actions
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        TemporaryEvent.shared.addActivity(activity, attribute: temporaryAttribute)
        dismiss(animated: true, completion: nil)
        delegate?.didAddActivity()
    }
    
    // MARK: - AttributesSelectionProtocol
    
    func didSelectAttribute() {
        if temporaryAttribute.hasAttributes {
            toogleSaveButton(enabled: true)
        }
    }
    
    func didSelectMoneyAttribute() {
        guard activity.settings.isExpendituresActive else {
            return
        }
        if temporaryAttribute.budget == 0 {
            temporaryAttribute.expenditures = []
            if content.last is [ExpenditureStayModel] || content.last is [ExpenditureTrackModel] {
                content.removeLast()
                sections.removeLast()
                dataSource = SectionedDataSource(content)
                dataSource.sectionHeaders = sections
            }
        } else {
            if (!(content.last is [ExpenditureStayModel]) && event.type == .stay) || (!(content.last is [ExpenditureTrackModel]) && event.type == .track){
                addExpenditureSection(to: &content, with: &sections)
                dataSource = SectionedDataSource(content)
                dataSource.sectionHeaders = sections
            }
        }
    }
}
