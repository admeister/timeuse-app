//
//  CalendarViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 30/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol DateSelectionDelegate: class {
    func didSelectDate(_ date: Date)
}

class CalendarViewController: UIViewController, JTACMonthViewDataSource, JTACMonthViewDelegate {
    
    @IBOutlet weak var calendar: JTACMonthView!
    @IBOutlet weak var alertView: ExtendedUIView!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let dateCellIdentifier = "DateCell"
    let dateHeaderIdentifier = "DateHeader"
    
    weak var delegate: DateSelectionDelegate?
    var formatter: DateFormatter!
    var user: User?
    var calendarEvents: CalendarEvents!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        user = Session.currentUser
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadData()
    }
    
    // MARK: - Data Loading
    
    func loadData() {
        activityIndicator.startAnimating()
        calendar.isHidden = true
        API.getCalendarEvents { result in
            self.activityIndicator.stopAnimating()
            switch result {
            case .success(let calendarEvents):
                self.calendarEvents = calendarEvents
                self.calendar.isHidden = false
                self.configureAlertView()
                self.calendar.reloadData()
            case .failure(let error):
                self.checkAndShow(error: error)
                self.calendar.isHidden = true
            }
        }
    }

    // MARK: - UI Configuration
    
    func configureUI() {
        formatter = DateFormatter()
        formatter.dateFormat = "MMMM YYYY"
        calendar.scrollingMode = .none
        calendar.cellSize = 60
        calendar.scrollToHeaderForDate(Date())
    }
    
    func configureAlertView() {
        alertView.isHidden = calendarEvents.incomplete.isEmpty || (calendarEvents.incomplete.count == 1 && calendarEvents.incomplete.first! == Date().withFormat("YYYY-MM-dd"))
        alertLabel.text = Texts.Other.unvalidatedEvents(calendarEvents.incomplete.count).text
    }
    
    // MARK: - Actions
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refreshDatesAction(_ sender: Any) {
        loadData()
    }
    
    // MARK: - JTACMonthViewDataSource
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        guard let user = Session.currentUser else {
            return ConfigurationParameters(startDate: Date(), endDate: Date())
        }
        return ConfigurationParameters(startDate: user.createDate, endDate: Calendar.current.date(byAdding: .year, value: 10, to: Date())!, generateOutDates: .off, firstDayOfWeek: .monday)
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: dateCellIdentifier, for: indexPath) as! DateCell
        configureCell(view: cell, cellState: cellState)
        return cell
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func configureCell(view: JTACDayCell?, cellState: CellState) {
        guard let cell = view as? DateCell else {
            return
        }
        cell.titleLabel.text = cellState.text
        handleCellUI(cell: cell, cellState: cellState)
    }
        
    func handleCellUI(cell: DateCell, cellState: CellState) {
        guard let user = user else {
            return
        }
        cell.markIconImageView.image = nil
        if cellState.date < Calendar.current.date(byAdding: .day, value: -1, to: user.createDate)! {
            cell.isHidden = false
            cell.titleLabel.textColor = Constants.Colors.main.lightGrey.value
            cell.isUserInteractionEnabled = false
        } else {
            if cellState.dateBelongsTo == .thisMonth {
                cell.isUserInteractionEnabled = true
                cell.isHidden = false
                cell.titleLabel.textColor = Constants.Colors.main.dark.value
            } else {
                cell.isHidden = true
            }
        }
        if calendarEvents != nil {
            calendarEvents.incomplete.forEach { date in
                if date == cellState.date.withFormat("YYYY-MM-dd") {
                    cell.titleLabel.textColor = Constants.Colors.main.red.value
                    cell.markIconImageView.image = #imageLiteral(resourceName: "alert_small_icon")
                    cell.markIconImageView.tintColor = Constants.Colors.main.red.value
                }
            }
            calendarEvents.complete.forEach { date in
                if date == cellState.date.withFormat("YYYY-MM-dd") {
                    cell.titleLabel.textColor = Constants.Colors.main.blue.value
                    cell.markIconImageView.image = #imageLiteral(resourceName: "checkmark_small_icon")
                    cell.markIconImageView.tintColor = Constants.Colors.main.blue.value
                }
            }
        }
        if Calendar.current.isDateInToday(cellState.date) {
            cell.containerView.backgroundColor = Constants.Colors.main.lightGrey.value
            cell.markIconImageView.image = nil
            cell.titleLabel.textColor = Constants.Colors.main.dark.value
        } else {
            cell.containerView.backgroundColor = .clear
        }
        if cellState.dateBelongsTo == .previousMonthWithinBoundary || cellState.dateBelongsTo == .previousMonthOutsideBoundary {
            cell.isHidden = true
        } else {
            cell.isHidden = false
        }
    }
    
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: dateHeaderIdentifier, for: indexPath) as! DateHeader
        header.monthTitle.text = formatter.string(from: range.start)
        return header
    }
    
    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return MonthSize(defaultSize: 65)
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        guard let cell = cell as? DateCell else {
            return
        }
        cell.containerView.backgroundColor = Constants.Colors.main.ultraLightGrey.value
        delegate?.didSelectDate(date)
        dismiss(animated: true, completion: nil)
    }

}
