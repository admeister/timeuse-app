//
//  UntrackedEventTypeViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 11/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

class UntrackedEventTypeViewController: __CollectionFeedController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var saveButton: UIButton!
    
    var dataSource: SectionedDataSource<Any>! {
        didSet { _dataSource = dataSource }
    }
    
    var cellController: UntrackedEventTypeCellController! {
        didSet { _cellController = cellController }
    }
    
    var eventNames: [EventName] = []
    var event: Event!
    var untrackedEvents: [String] = []
    
    weak var delegate: CompletedEventsProtocol?
    
    static func _init(event: Event, delegate: CompletedEventsProtocol?) -> UntrackedEventTypeViewController {
        let controller = UntrackedEventTypeViewController.instantiateFromHomeStoryboard()
        controller.event = event
        controller.delegate = delegate
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        untrackedEvents = event.untrackedEvents.map{ $0.rawValue }
        cellController = UntrackedEventTypeCellController()
        cellController.onSelectionInteraction.subscribe(with: self) { (self, eventName) in
            self.configureUntrackedEvents(eventName)
        }
        headerController = AttributeHeaderController()
        headerIsSticky = true
        toogleSaveButton(enabled: false)
        configureEventNames()
        reloadDataSource()
    }

    // MARK: - UI Configuration
    
    func toogleSaveButton(enabled: Bool) {
        saveButton.isEnabled = enabled
        saveButton.alpha = enabled ? 1 : 0.4
    }
    
    // MARK: - Data Loading
    
    func configureEventNames() {
        for type in EventConfig.allCases {
            guard type != .deleted && type != .untracked else {
                return
            }
            var eventName: EventName!
            if event.untrackedEvents.contains(type) {
                eventName = EventName(eventConfig: type, isSelected: true)
            } else {
                eventName = EventName(eventConfig: type)
            }
            eventNames.append(eventName)
        }
    }
    
    func reloadDataSource() {
        var content: [[Any]] = []
        var sections: [BaseHeaderSection] = []

        addStaySection(to: &content, with: &sections)
        addTrackSection(to: &content, with: &sections)

        dataSource = SectionedDataSource(content)
        dataSource.sectionHeaders = sections
    }
    
    private func addStaySection(to content:inout [[Any]], with sections:inout [BaseHeaderSection]) {
        sections.append(BaseHeaderSection(value: Texts.Section.stay.text))
        let stayEvents = eventNames.filter { $0.isStayEvent }
        stayEvents.first?.isFirst = true
        stayEvents.last?.isLast = true
        content.append(stayEvents)
    }
    
    private func addTrackSection(to content:inout [[Any]], with sections:inout [BaseHeaderSection]) {
        sections.append(BaseHeaderSection(value: Texts.Section.track.text))
        let trackEvents = eventNames.filter { !$0.isStayEvent }
        trackEvents.first?.isFirst = true
        trackEvents.last?.isLast = true
        content.append(trackEvents)
    }
    
    func configureUntrackedEvents(_ eventName: EventName) {
        if untrackedEvents.contains(eventName.eventConfig.rawValue) {
            untrackedEvents.remove(object: eventName.eventConfig.rawValue)
        } else {
            untrackedEvents.append(eventName.eventConfig.rawValue)
        }
        toogleSaveButton(enabled: true)
    }
    
    // MARK: - Actions
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if event.id.isEmpty {
            toogleSaveButton(enabled: false)
            activityIndicator.startAnimating()
            API.addUntrackedEvent(untrackedEvents: untrackedEvents, startDate: event.startDate, endDate: event.endDate, timezone: TimeZone.current.identifier) { (result) in
                self.activityIndicator.stopAnimating()
                self.toogleSaveButton(enabled: true)
                switch result {
                case .failure(let error):
                    self.checkAndShow(error: error)
                case .success(_):
                    self.dismiss(animated: true, completion: nil)
                    self.delegate?.didUpdateUntrackedEvent()
                }
            }
        } else {
            activityIndicator.startAnimating()
            self.toogleSaveButton(enabled: false)
            API.updateUntrackedEvent(id: event.id, untrackedEvents: untrackedEvents) { (result) in
                self.activityIndicator.stopAnimating()
                self.toogleSaveButton(enabled: true)
                switch result {
                case .failure(let error):
                    self.checkAndShow(error: error)
                case .success(_):
                    self.dismiss(animated: true, completion: nil)
                    self.delegate?.didUpdateUntrackedEvent()
                }
            }
        }
    }
    
}
