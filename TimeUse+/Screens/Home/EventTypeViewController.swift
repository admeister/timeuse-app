//
//  EventTypeViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 15/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

protocol EventDetailsProtocol: class {
    func didChangeEventType()
    func didAddActivity()
}

class EventTypeViewController: __CollectionFeedController, EventTypeCellControllerProtocol {

    @IBOutlet weak var navBarView: UIView!
    
    var dataSource: DataSource<EventName>! {
        didSet { _dataSource = dataSource }
    }
    
    var cellController: EventTypeCellController! {
        didSet { _cellController = cellController }
    }
    
    var eventNames: [EventName] = []
    weak var delegate: EventDetailsProtocol?
    
    static func _init(eventNames: [EventName], delegate: EventDetailsProtocol) -> EventTypeViewController {
        let controller = EventTypeViewController.instantiateFromHomeStoryboard()
        controller.eventNames = eventNames
        controller.delegate = delegate
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cellController = EventTypeCellController()
        cellController.delegate = self
        configureNavBarUI()
        reloadDataSource()
    }
    
    // MARK: - UI Configuration
    
    func configureNavBarUI() {
        navBarView.backgroundColor = TemporaryEvent.shared.eventType.color
    }
    
    // MARK: - Data Loading
    
    func reloadDataSource() {
        eventNames.last?.isLast = true
        dataSource = DataSource(eventNames)
    }
    
    // MARK: - Actions
    
    @IBAction func backAction(_ sender: Any) {
        popViewController()
        delegate?.didChangeEventType()
    }
    
    // MARK: - EventTypeCellControllerProtocol
    
    func didChangeEventType() {
        configureNavBarUI()
    }

}
