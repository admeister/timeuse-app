//
//  EventDetailsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 15/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude
import GoogleMaps

protocol CompletedEventsProtocol: class {
    func didSaveActivity()
    func didDeleteEvent()
    func didUpdateUntrackedEvent()
}

class EventDetailsViewController: __CollectionFeedController, GoogleMapsProtocol, EventDetailsProtocol {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageContainerView: ExtendedUIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var eventStartDateLabel: UILabel!
    @IBOutlet weak var eventEndDateLabel: UILabel!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var editButton: ExtendedUIButton!
    @IBOutlet weak var eventDidNotHappenButton: UIButton!
    @IBOutlet weak var saveEventActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noActivityInformationLabel: UILabel!
    
    var dataSource: SectionedDataSource<Any>! {
        didSet { _dataSource = dataSource }
    }
    
    var cellController: MultiCollectionCellController! {
        didSet { _cellController = cellController }
    }
    
    var event: Event!
    var activities: [EventActivity] = []
    var eventNames: [EventName] = []
    var minEventDurationTime: Int!
    weak var completedEventsDelegate: CompletedEventsProtocol?
    var presets: Presets!
    
    static func _init(event: Event, minEventDurationTime: Int, completedEventsDelegate: CompletedEventsProtocol?) -> EventDetailsViewController {
        let controller = EventDetailsViewController.instantiateFromHomeStoryboard()
        controller.event = event
        controller.completedEventsDelegate = completedEventsDelegate
        controller.minEventDurationTime = minEventDurationTime
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: true)
        
        headerController = ActivityHeaderController()
        headerIsSticky = true
        let activityController = BaseActivityCellController()
        activityController.onSelectionInteraction.subscribe(with: self) {
            $0.configureActivities()
            $0.toogleSaveButton(enabled: true)
        }
        let deleteEventController = DeleteEventCellController()
        deleteEventController.onSelectionInteraction.subscribe(with: self) {
            $0.deleteEvent()
        }
        cellController = MultiCollectionCellController([activityController,
                                                        EmptyCellController(),
                                                        deleteEventController])
        configureTemporaryEventType()
        configureMap()
        configureUI()
        configureNavBarUI()
        loadData()
    }
    
    // MARK: - Data Loading
    
    func loadData() {
        editButton.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        API.getEvent(by: event.id) { (result) in
            self.activityIndicator.stopAnimating()
            self.editButton.isUserInteractionEnabled = true
            switch result {
            case .success(let eventData):
                self.activities = eventData.eventActivities
                self.eventNames = eventData.eventNames
                TemporaryEvent.shared.activities = self.activities
                self.configureActivities()
                self.configureSaveButton()
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
        
        API.getPresets { result in
            switch result {
            case .success(let presets):
                self.presets = presets
                self.event.timeInterval = presets.duration.value
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    func configureTemporaryEventType() {
        TemporaryEvent.shared.eventType = event.eventConfig
    }
    
    func configureActivities() {
        TemporaryEvent.shared.refreshActivites()
        TemporaryEvent.shared.activities = activities.filter { $0.displayOn.contains(TemporaryEvent.shared.eventType) }
        TemporaryEvent.shared.completedActivites = TemporaryEvent.shared.activities.filter { $0.isSelected }
        reloadDataSource()
    }
    
    func deleteEvent() {
        let alert = UIAlertController(title: Texts.Other.caution.text, message: Texts.Other.deleteEventDescription.text, preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: Texts.Other.delete.text, style: .destructive) { action in
            self.saveEventActivityIndicator.startAnimating()
            API.deleteEvent(by: self.event.id, callback: { (result) in
                self.saveEventActivityIndicator.stopAnimating()
                switch result {
                case .failure(let error):
                    self.checkAndShow(error: error)
                case .success(_):
                    self.dismiss(animated: true, completion: nil)
                    self.completedEventsDelegate?.didDeleteEvent()
                }
            })
        }
        alert.addAction(deleteAction)
        alert.addAction(UIAlertAction(title: Texts.Other.cancel.text, style: .cancel))
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - UI Configuration
    
    func configureUI() {
        iconImageView.image = TemporaryEvent.shared.eventType.icon
        iconImageView.tintColor = .white
        imageContainerView.backgroundColor = TemporaryEvent.shared.eventType.color
        eventNameLabel.text = TemporaryEvent.shared.eventType.title
        eventStartDateLabel.text = event.startDateTime
        eventEndDateLabel.text = event.endDateTime
        editButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        eventDidNotHappenButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        noActivityInformationLabel.text = Texts.Other.noActivityInformation.text
        noActivityInformationLabel.isHidden = event.duration > minEventDurationTime
    }
    
    func configureSaveButton() {
        if TemporaryEvent.shared.completedActivites.isEmpty {
            toogleSaveButton(enabled: false)
        } else {
            toogleSaveButton(enabled: true)
        }
    }
    
    func configureNavBarUI() {
        navBarView.backgroundColor = TemporaryEvent.shared.eventType.color
    }
    
    func configureMap() {
        mapView.clear()
        switch event.type {
        case .stay:
            let marker = createMarker(latitude: event.geometry.locationCoordinates.first!.latitude, longitude: event.geometry.locationCoordinates.first!.longitude, color: TemporaryEvent.shared.eventType.color)
            mapView.animate(to: GMSCameraPosition(target: marker.position, zoom: 15))
        case .track:
            let polyline = createTrack(coordinates: event.geometry.locationCoordinates, color: TemporaryEvent.shared.eventType.color)
            createTrackeDistanceMarker(coordinate: event.geometry.locationCoordinates[event.geometry.locationCoordinates.count / 2], distance: event.trackDistance)
            mapView.animate(with: GMSCameraUpdate.fit(GMSCoordinateBounds(path: polyline.path!)))
        case .untracked:
            break
        }
    }
    
    func toogleSaveButton(enabled: Bool) {
        saveButton.isEnabled = enabled
        saveButton.alpha = enabled ? 1 : 0.4
    }
    
    func reloadDataSource() {
        var content: [[Any]] = []
        
        let specificActivites = TemporaryEvent.shared.activities.filter({ $0.type == .specific })
        if specificActivites.count == 1 {
            specificActivites.first?.isFirst = true
            specificActivites.first?.isLast = true
        } else {
            specificActivites.first?.isFirst = true
            specificActivites.last?.isLast = true
        }
    
        let generalActivites = TemporaryEvent.shared.activities.filter({ $0.type == .general })
        generalActivites.first?.isFirst = true
        generalActivites.last?.isLast = true
    
        let emptyModel = EmptyModel()
        let deleteEvent = DeleteEventModel()
        
        var _content: [Any] = []
        if specificActivites.count > 0 {
            _content.append(contentsOf: specificActivites)
            _content.append(emptyModel)
            _content.append(contentsOf: generalActivites)
        } else {
            _content.append(contentsOf: generalActivites)
        }
        _content.append(deleteEvent)
        content.append(_content)
        dataSource = SectionedDataSource(content)
        dataSource.sectionHeaders = ["Activities"]
    }
    
    // MARK: - Actions
    
    @IBAction func cancelAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        saveEventActivityIndicator.startAnimating()
        toogleSaveButton(enabled: false)
        API.updateEvent(id: event.id, type: TemporaryEvent.shared.eventType.rawValue, duration: event.duration, activities: TemporaryEvent.shared.completedActivites) { (result) in
            self.saveEventActivityIndicator.stopAnimating()
            self.toogleSaveButton(enabled: true)
            switch result {
            case .success(_):
                self.dismiss(animated: true, completion: nil)
                self.completedEventsDelegate?.didSaveActivity()
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        let controller = EventTypeViewController._init(eventNames: eventNames, delegate: self)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func eventDidNotHappenAction(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: Texts.Other.areYouSure.text, preferredStyle: .alert)
        
        let action = UIAlertAction(title: Texts.Other.yes.text, style: .default) { action in
            self.saveEventActivityIndicator.startAnimating()
            API.mergeEvent(by: self.event.id, callback: { (result) in
                self.saveEventActivityIndicator.stopAnimating()
                switch result {
                case .failure(let error):
                    self.checkAndShow(error: error)
                case .success(_):
                    self.dismiss(animated: true, completion: nil)
                    self.completedEventsDelegate?.didDeleteEvent()
                }
            })
        }
        alert.addAction(action)
        alert.addAction(UIAlertAction(title: Texts.Other.cancel.text, style: .cancel))
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - EventTypeProtocol
    
    func didChangeEventType() {
        configureNavBarUI()
        configureUI()
        configureMap()
        configureActivities()
        toogleSaveButton(enabled: true)
    }
    
    func didAddActivity() {
        configureActivities()
        toogleSaveButton(enabled: true)
    }
}
