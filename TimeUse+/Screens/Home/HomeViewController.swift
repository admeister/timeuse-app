//
//  HomeViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 26/05/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import GoogleMaps
import FloatingPanel

class HomeViewController: UIViewController, FloatingPanelControllerDelegate, EventsDelegate, GoogleMapsProtocol {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var dateButton: ExtendedUIButton!
    
    var locationManager: CLLocationManager?
    var floatingPanelController: FloatingPanelController!
    var eventsController: EventsViewController!
    var allCoordinates: [CLLocationCoordinate2D] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.allowsBackgroundLocationUpdates = true
        
        PushNotificationHandler.shared.registerForPushNotifications()
        configureDate(date: Date())
        setupFloatingPanelController()
    }
    
    // MARK: - UI Configuration
    
    func configureDate(date: Date) {
        dateButton.setTitle(date.withFormat("dd MMMM YYYY"), for: .normal)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    func setupFloatingPanelController() {
        floatingPanelController = FloatingPanelController()
        floatingPanelController.delegate = self
        floatingPanelController.layout = EventsViewControllerFloatingLayout(mapHeight: mapView.frame.height)
        floatingPanelController.surfaceView.appearance.cornerRadius = 10
        
        eventsController = EventsViewController()
        eventsController.delegate = self
        floatingPanelController.set(contentViewController: eventsController)
        floatingPanelController.track(scrollView: eventsController.collectionView)
        
        floatingPanelController.addPanel(toParent: self, animated: true)
    }
    
    func pullDownEventsController() {
        if floatingPanelController.state == .full {
            floatingPanelController.move(to: .tip, animated: true)
        }
    }
    
    // MARK: - EventsDelegate
    
    func didSelectEvent(_ event: Event, color: UIColor) {
        pullDownEventsController()
        mapView.clear()
        switch event.type {
        case .stay:
            let marker = createMarker(latitude: event.geometry.locationCoordinates.first!.latitude, longitude: event.geometry.locationCoordinates.first!.longitude, color: color)
            mapView.animate(to: GMSCameraPosition(target: marker.position, zoom: 15))
        case .track:
            let polyline = createTrack(coordinates: event.geometry.locationCoordinates, color: color)
            mapView.animate(with: GMSCameraUpdate.fit(GMSCoordinateBounds(path: polyline.path!)))
        case .untracked:
            break
        }
    }
    
    func displayEvents(_ events: [Event]) {
        let filteredEvents = events.filter { $0.eventConfig != .deleted && $0.eventConfig != .untracked }
        pullDownEventsController()
        mapView.clear()
        allCoordinates.removeAll()
        filteredEvents.forEach { (event) in
            switch event.type {
            case .stay:
                let coordinate = CLLocationCoordinate2D(latitude: event.geometry.locationCoordinates.first!.latitude, longitude: event.geometry.locationCoordinates.first!.longitude)
                allCoordinates.append(coordinate)
                _ = createMarker(latitude: coordinate.latitude, longitude: coordinate.longitude, color: event.eventConfig.color)
            case .track:
                event.geometry.locationCoordinates.forEach { locationCoordinate in
                    let coordinate = CLLocationCoordinate2D(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude)
                    allCoordinates.append(coordinate)
                }
                _ = createTrack(coordinates: event.geometry.locationCoordinates, color: event.eventConfig.color)
            case .untracked:
                break
            }
        }
        showAllCoordinates()
    }
    
    func didSelectDay(_ date: Date) {
        configureDate(date: date)
    }
    
    func showAllCoordinates() {
        if allCoordinates.isEmpty {
            if let userLocation = mapView.myLocation?.coordinate {
                mapView.camera = GMSCameraPosition(target: userLocation, zoom: 15, bearing: 0, viewingAngle: 0)
            }
        } else {
            var bounds = GMSCoordinateBounds()
            allCoordinates.forEach { (coordinate) in
                bounds = bounds.includingCoordinate(coordinate)
            }
            mapView.setMinZoom(1, maxZoom: 15)
            mapView.animate(with: GMSCameraUpdate.fit(bounds))
            mapView.setMinZoom(1, maxZoom: 20)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func showStatisticsAction(_ sender: UIButton) {
        let controller = StatisticsViewController.instantiateFromStatisticsStoryboard()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func showSettingsAction(_ sender: UIButton) {
        let controller = SettingsViewController.instantiateFromHomeStoryboard()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func showCalendarAction(_ sender: UIButton) {
        let controller = CalendarViewController.instantiateFromHomeStoryboard()
        controller.delegate = eventsController
        present(controller, animated: true, completion: nil)
    }
}

    // MARK: - CLLocationManagerDelegate

extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
          }
            
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
        manager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .notDetermined else {
            return
        }
        if status == .authorizedAlways {
            manager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
        } else {
            mapView.isMyLocationEnabled = false
        }
    }
}

    // MARK: - EventsViewControllerFloatingLayout

class EventsViewControllerFloatingLayout: FloatingPanelLayout {
    let navBarHeight: CGFloat = 50 // nav bar height from storyboard
    
    var mapHeight: CGFloat
    
    init(mapHeight: CGFloat) {
        self.mapHeight = mapHeight
    }
    
    var tipHeight: CGFloat {
        if DeviceType.IS_IPHONE_5 {
            mapHeight = 280 // map height from storyboard
        }
        
        return MainDelegate.instance.window!.safeAreaLayoutGuide.layoutFrame.height - mapHeight - navBarHeight
    }
    
    var position: FloatingPanelPosition {
        .bottom
    }
    
    var initialState: FloatingPanelState {
        .tip
    }
    
    var anchors: [FloatingPanelState : FloatingPanelLayoutAnchoring] {
        [.full: FloatingPanelLayoutAnchor(absoluteInset: navBarHeight, edge: .top, referenceGuide: .safeArea),
         .tip: FloatingPanelLayoutAnchor(absoluteInset: tipHeight, edge: .bottom, referenceGuide: .safeArea)]
    }
}
