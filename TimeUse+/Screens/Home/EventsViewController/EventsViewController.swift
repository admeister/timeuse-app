//
//  EventsViewController.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 12/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import UIKit
import Tapptitude

protocol EventsDelegate: class {
    func displayEvents(_ events: [Event])
    func didSelectEvent(_ event: Event, color: UIColor)
    func didSelectDay(_ date: Date)
}

class EventsViewController: __CollectionFeedController, DateSelectionDelegate, CompletedEventsProtocol {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    weak var delegate: EventsDelegate?
    
    var dataSource: SectionedDataSource<Event>! {
        didSet { _dataSource = dataSource }
    }
    
    var cellController: EventCellController! {
        didSet { _cellController = cellController }
    }
    
    var events: [Event] = []
    var date: Date!
    var timeInterval: Double!
    let minutes: Double = 60
    var minEventDurationTime: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerController = EventHeaderController()
        headerIsSticky = true
        cellController = EventCellController()
        cellController.completedEventsDelegate = self
        cellController.delegate = delegate
        date = Date()
        loadEventsWithFormmatedDate()
    }

    // MARK: - Data Loading
    
    func loadEvents(date: String) {
        activityIndicator.startAnimating()
        API.getEvents(by: date, timezone: TimeZone.current.identifier) { (result) in
            self.activityIndicator.stopAnimating()
            switch result {
            case .success(let eventsData):
                self.timeInterval = eventsData.settings.untrackedEventDuration
                self.minEventDurationTime = eventsData.settings.minEventDurationTime
                self.configureEvents(eventsData.events)
                self.delegate?.displayEvents(self.events)
                self.reloadDataSource()
            case .failure(let error):
                self.checkAndShow(error: error)
            }
        }
    }
    
    func reloadDataSource() {
        guard !events.isEmpty else {
            emptyView?.isHidden = false
            dataSource = []
            return
        }
        events.first?.isFirst = true
        events.last?.isLast = true
        dataSource = SectionedDataSource([events])
        dataSource.sectionHeaders = [Texts.Other.myEvents.text]
    }
    
    private func configureEvents(_ eventsList: [Event]) {
        events = eventsList
        guard date < Date() else {
            return
        }
        let calendar = Calendar.current
        if calendar.isDateInToday(date) || calendar.isDateInYesterday(date) {
            guard !events.isEmpty else {
                return
            }
            if events.count == 1 && events.first?.eventConfig != .untracked && validTimeInterval(startDate: events.first!.startDate, endDate: Calendar.current.startOfDay(for: date)) {
                addFirstUntrackedEvent()
            } else if events.count > 1 {
                addUntrackedEvents()
            }
        } else {
            if events.isEmpty {
                let event = Event(eventConfig: .untracked, startDate: calendar.startOfDay(for: date), endDate: date.endOfDay)
                events.append(event)
            } else if events.count == 1 && events.first?.eventConfig != .untracked {
                if validTimeInterval(startDate: events.first!.startDate, endDate: calendar.startOfDay(for: date)) {
                    addFirstUntrackedEvent()
                }
                if validTimeInterval(startDate: date.endOfDay, endDate: events.last!.endDate) {
                    addLastUntrackedEvent()
                }
            } else if events.count > 1 {
                addUntrackedEvents()
                if validTimeInterval(startDate: date.endOfDay, endDate: events.last!.endDate) && events.last!.eventConfig != .untracked {
                    addLastUntrackedEvent()
                }
            }
        }
    }
    
    private func addFirstUntrackedEvent() {
        let firstEvent = Event(eventConfig: .untracked, startDate: Calendar.current.startOfDay(for: date), endDate: events.first!.startDate)
        events.insert(firstEvent, at: 0)
    }
    
    private func addLastUntrackedEvent() {
        let lastEvent = Event(eventConfig: .untracked, startDate: events.last!.endDate, endDate: date.endOfDay)
        events.append(lastEvent)
    }
    
    private func validTimeInterval(startDate: Date, endDate: Date) -> Bool {
        return (startDate.timeIntervalSince1970 - endDate.timeIntervalSince1970)/minutes > timeInterval
    }
    
    private func addUntrackedEvents() {
        var index = 0
        if validTimeInterval(startDate: events.first!.startDate, endDate: Calendar.current.startOfDay(for: date)) && events.first!.eventConfig != .untracked {
            addFirstUntrackedEvent()
            index = 1
        }
        while index < events.count - 1 {
            if validTimeInterval(startDate: events[index + 1].startDate, endDate: events[index].endDate) && events[index].eventConfig != .untracked && events[index + 1].eventConfig != .untracked {
                let event = Event(eventConfig: .untracked, startDate: events[index].endDate, endDate: events[index + 1].startDate)
                events.insert(event, at: index + 1)
            }
            index += 1
        }
    }

    // MARK: - DateSelectionDelegate
    
    func didSelectDate(_ date: Date) {
        self.date = date
        loadEventsWithFormmatedDate()
        delegate?.didSelectDay(date)
    }
    
    // MARK: - Helpers
    
    func loadEventsWithFormmatedDate() {
        loadEvents(date: date.withFormat("YYYY-MM-dd"))
    }
    
    // MARK: - EventDetailsViewController
    
    func didSaveActivity() {
        loadEventsWithFormmatedDate()
    }
    
    func didDeleteEvent() {
        loadEventsWithFormmatedDate()
    }
    
    func didUpdateUntrackedEvent() {
        loadEventsWithFormmatedDate()
    }
    
}
