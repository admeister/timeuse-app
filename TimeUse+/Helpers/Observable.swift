//
//  Observable.swift
//  OnTapp-Consumer
//
//  Created by Ionut Costin on 21/02/2020.
//  Copyright © 2020 OnTapp. All rights reserved.
//

import Foundation

class Subscription<Input> {

    weak var subscriber : AnyObject?
    var _callback: ((Input) -> Void)
    
    init(subscriber:AnyObject, with callback: @escaping (Input) -> Void) {
        self.subscriber = subscriber
        self._callback = callback
    }
}


protocol AnyObservable {
    var anyValue:Any? { get }
    
    func subscribeForAny<Object : AnyObject>(with object: Object, with callback: @escaping (Object, Any) -> Void)
}


class Observable<Value>: AnyObservable {
    
    var subscriptions:[Subscription<Value>] = []
    
    var debouncer:Debouncer?

    private(set) var value:Value!
    
    init() { }
    
    init(value:Value) {
        self.value = value
    }
    
    @discardableResult
    func subscribe<Object : AnyObject>(with object: Object, with callback: @escaping (Object, Value) -> Void) -> Observable<Value> {
        
        let subscription = Subscription<Value>(subscriber: object) { [weak object] input in
            guard let object = object else {
                return
            }
            callback(object, input)
        }
        subscriptions.append(subscription)
        return self
    }
    
    @discardableResult
    func subscribeWithLast<Object : AnyObject>(with object: Object, with callback: @escaping (Object, Value) -> Void) -> Observable<Value> {
        self.subscribe(with: object, with: callback)
        if let value = self.value {
            callback(object,value)
        }
        return self
    }
    
    
    func notify(value:Value) {
        self.value = value
        subscriptions = subscriptions.filter({ $0.subscriber != nil})
        subscriptions.forEach({ $0._callback(value)})
    }

    func notifyCurrent() {
        if let value = self.value {
            notify(value: value)
        }
    }
    
    func silentSet(_ value:Value) {
        self.value = value
    }
    
    func throtteled(by timeInterval:TimeInterval) -> Observable<Value> {
        
        let newObservable:Observable = Observable<Value>()
        newObservable.debouncer = Debouncer(timeInterval: timeInterval, handler: { [weak newObservable] in
            guard let _newObservable = newObservable else {
                return
            }
            _newObservable.notify(value: _newObservable.value!)
        })
        self.subscribe(with: newObservable) { (newObservable, value) in
            newObservable.value = value
            newObservable.debouncer?.fire()
        }
        return newObservable
    }
    
    func debounced(by timeInterval:TimeInterval) -> Observable<Value> {
        
        let newObservable:Observable = Observable<Value>()
        newObservable.debouncer = Debouncer(timeInterval: timeInterval, handler: { [weak newObservable] in
            guard let _newObservable = newObservable else {
                return
            }
            _newObservable.notify(value: _newObservable.value!)
        })
        newObservable.debouncer?.onlyThrottle = false
        self.subscribe(with: newObservable) { (newObservable, value) in
            newObservable.value = value
            newObservable.debouncer?.fire()
        }
        return newObservable
    }
    
    func mapped<T>(_ mapping:@escaping (Value) throws -> (T) ) -> Observable<T> {
        let newObservable:Observable<T> = Observable<T>()
        self.subscribe(with: newObservable) { (newObservable, value) in
            if let mapped = try? mapping(value) {
                newObservable.notify(value: mapped)
            }
        }
        return newObservable
    }
    
    func filtered(_ filtering:@escaping (Value) throws -> Bool) -> Observable<Value> {
        let newObservable:Observable<Value> = Observable<Value>()
        self.subscribe(with: newObservable) { (newObservable, value) in
            if let meetsCondition = try? filtering(value), meetsCondition == true  {
                newObservable.notify(value: value)
            }
        }
        return newObservable
    }
    
    func mapToVoid() -> Observable<Void> {
        let newObservable:Observable<Void> = Observable<Void>()
        self.subscribe(with: self) { (self, _) in
            newObservable.notify(value: ())
        }
        return newObservable
    }
    

    func clear() {
        self.subscriptions = []
    }
    
    
    func isSubscriber(_ object:AnyObject) -> Bool {
        return self.subscriptions.contains(where: { $0.subscriber === object})
    }
 
    func unsubscribe(object:AnyObject) {
        self.subscriptions.removeAll { (subscription) -> Bool in
            if subscription.subscriber == nil {
                return true
            } else {
                return subscription.subscriber! === object
            }
        }
    }
    
    func unbind(from observable:Observable<Value>) {
        observable.unsubscribe(object: self)
    }
    
    func bind(to observable:Observable<Value>) {
        observable.subscribe(with: self) { (self, value) in
            self.notify(value: value)
        }
    }
    
    func bindOnce(to observable:Observable<Value>) {
        if observable.isSubscriber(self) {
            return
        }
        self.bind(to: observable)
    }
    
    func bindOnce<T>(to observable:Observable<T>, _ mapping:@escaping (T) throws -> (Value?) ) {
        if observable.isSubscriber(self) {
            return
        }
        self.bind(to: observable, mapping)
    }


    func bind<T>(to observable:Observable<T>, _ mapping:@escaping (T) throws -> (Value?) ) {
        observable.subscribe(with: self) { (self, value) in
            if let mapped = try? mapping(value) {
                self.notify(value: mapped)
            }
        }
    }
    
    //MARK: AnyObservable
    
    var anyValue: Any? { return value }
    
    func subscribeForAny<Object : AnyObject>(with object: Object, with callback: @escaping (Object, Any) -> Void) {
        self.subscribe(with: object) { (object, value) in
            callback(object,value)
        }
    }
    
    func disposedBy(bag:inout [AnyObservable]) {
        bag.append(self)
    }

}


func whenAnyEmmited<Value>(_ observables:[AnyObservable], perform: @escaping (() -> Value)) -> Observable<Value> {
    let newObservable:Observable<Value> = Observable<Value>()
    observables.forEach({ $0.subscribeForAny(with: newObservable, with: { (newObservable, _) in
        newObservable.notify(value: perform())
    })})
    return newObservable
}

func whenAnyEmmited(_ observables:[Observable<Void>]) -> Observable<Void> {
    return whenAnyEmmited(observables, perform: { () -> Void in
        return
    })
    
}



func combine<T,U,Value>(_ first:Observable<T>, _ second:Observable<U>, _ combine: @escaping ((T,U) -> Value)) -> Observable<Value> {
    let newObservable:Observable<Value> = Observable<Value>()
    
    let _callback = { [weak first, weak second, weak newObservable] in
        guard let t = first?.value, let u = second?.value else {
            return
        }
        newObservable?.notify(value: combine(t,u))
    }
    first.subscribe(with: newObservable) { (newObservable, _) in _callback() }
    second.subscribe(with: newObservable) { (newObservable, _) in _callback() }
    
    return newObservable
}



extension Observable where Value == Void {
    
    func notify() {
        self.notify(value: ())
    }
    
    func bind<T>(to observable:Observable<T>) {
        observable.subscribe(with: self) { (self, value) in
            self.notify()
        }
    }
    
    @discardableResult
    func subscribe<Object : AnyObject>(with object: Object, with callback: @escaping (Object) -> Void) -> Observable<Void>{
        return self.subscribe(with: object) { (object, _) in
            callback(object)
        }
    }
}

