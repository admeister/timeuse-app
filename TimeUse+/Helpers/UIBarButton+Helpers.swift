//
//  UIBarButton+Helpers.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    static func backButtonWithController(_ controller:UIViewController) -> UIBarButtonItem {
        let backButton = UIBarButtonItem(image: UIImage(named: "backArrow"), style: .plain, target: controller, action: #selector(UIViewController.popViewController))
        backButton.tintColor = UIColorFromRGB(0x959697)
        return backButton
    }
}

extension UIViewController {
    @objc func popViewController() {
        _ = navigationController?.popViewController(animated: true)
    }
}
