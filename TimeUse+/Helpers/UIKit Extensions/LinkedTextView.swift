//
//  LinkedTextView.swift
//  PeopleFinders
//
//  Created by Efraim Budusan on 12/18/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

class LinkedTextView: UITextView, UITextViewDelegate {
    
    weak var _delegate:UITextViewDelegate?
    
    override var delegate: UITextViewDelegate? {
        get {
            return self._delegate
        }
        set {
            self._delegate = newValue
        }
    }
    
    var callbacks:[String:Bindable<Void>] = [:]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
        self.isEditable = false
        self.dataDetectorTypes = [UIDataDetectorTypes.link]
    }
    
    
    func setAction(for key:String) -> Bindable<Void> {
        
        let urlString = String(key.hashValue)
        let attrs:TextAttributes = [.link: urlString]
        self.attributedText = self.attributedText.adding(attributes: attrs, to: key)
        self.linkTextAttributes = self.attributedText.attributes(for: key)
        let binding:Bindable<Void> = Bindable()
        callbacks[urlString] = binding
        return binding
    }
    
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL,
                         in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        let urlString = URL.absoluteString
        if let bindable = callbacks[urlString] {
            bindable.notify?(())
            return false
        }
        return true
        
    }

}
