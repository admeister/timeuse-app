//
//  OpinonUITextField.swift
//  Opinon
//
//  Created by Efraim Budusan on 4/16/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import UIKit

class ExtendedUITextField: UITextField {
    
    var showClearButton:Bool = true
    
    var observation:NSKeyValueObservation!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.set(placeHolderColor: Constants.Colors.main.darkGrey.value)
        let currentFontSize = self.font?.pointSize ?? 14
        self.set(placeHolderFont: Constants.Fonts.roboto.regular.with(size: currentFontSize))
        self.rightViewMode = .whileEditing
        self.clearButtonMode = .never
//        self.tintColor = Constants.Colors.main.lightBlue.value
        self.tintColor = UIColor(named: "lightBlue")

        let clearButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: 20))
        clearButton.setImage(#imageLiteral(resourceName: "clear_glyph"), for: .normal)
        clearButton.addTarget(self, action: #selector(clearAction), for: .touchUpInside)
        self.rightView = clearButton
        self.rightView?.isHidden = true
        self.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
                
        observation = self.observe(\.text) {
            [unowned self] (object, observedChange) in
                self.editingChanged(self)
        }
    }

    deinit {
        if let observation = observation {
            observation.invalidate()
            removeObserver(observation, forKeyPath: "text")
        }
    }
    
    @objc func editingChanged(_ sender:Any) {
        
        if showClearButton {
            self.rightView?.isHidden = self.text == ""
        } else {
            self.rightView?.isHidden = true
        }
        
        
    }
    
    @objc func clearAction(_ sender:Any) {
        self.text = ""
        self.sendActions(for: .editingChanged)
    }
 

}
