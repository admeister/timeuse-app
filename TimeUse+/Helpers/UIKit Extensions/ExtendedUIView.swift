//
//  ExtendedUIView.swift
//  StairsShare
//
//  Created by Efraim Budusan on 5/23/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

struct ShadowInfo {
    
    var color:UIColor = .black
    var opacity:Float = 0.3
    var offset:CGSize = CGSize.zero
    var radius:CGFloat = 3
    
}

enum ShadowType:String {
    case `default`
    
    var info:ShadowInfo {
        switch self {
        case .default:
            return ShadowInfo()
        }
    }
}

class ExtendedUIView: UIView {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0
    
    //MARK: - Shadow Info
    
    @IBInspectable var hasShadow: Bool = false
    @IBInspectable var shadowColor: UIColor = ShadowType.default.info.color
    @IBInspectable var shadowOpacity: Float = ShadowType.default.info.opacity
    @IBInspectable var shadowOffset: CGSize = ShadowType.default.info.offset
    @IBInspectable var shadowRadius: CGFloat = ShadowType.default.info.radius
    @IBInspectable var shadowType: String = ShadowType.default.rawValue
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        if cornerRadius < 0 {
            self.layer.cornerRadius = self.frame.height / 2
        } else {
            self.layer.cornerRadius = cornerRadius
        }
        
        if hasShadow {
            if let type = ShadowType.init(rawValue: shadowType) {
                addDropShadow(shadowInfo: type.info)
            } else {
                addDropShadow(color: shadowColor, opacity: shadowOpacity, offSet: shadowOffset, radius: shadowRadius)
            }
        }
        
        
    }
    
    func addDropShadow(shadowInfo:ShadowInfo) {
        self.addDropShadow(color: shadowInfo.color,
                           opacity: shadowInfo.opacity,
                           offSet: shadowInfo.offset,
                           radius: shadowInfo.radius)
    }
    
    
}

extension UIView {
    
        func addDropShadow(color: UIColor = .black, opacity: Float = 0.2, offSet: CGSize = CGSize(width: 0, height: 0), radius: CGFloat = 3, scale: Bool = true) {
            layer.masksToBounds = false
            layer.shadowColor = color.cgColor
            layer.shadowOpacity = opacity
            layer.shadowOffset = offSet
            layer.shadowRadius = radius
            layer.shouldRasterize = true
            layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat, rect: CGRect) {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
}
