//
//  UINavigationController + PopGesture.swift
//  StairsShare
//
//  Created by Efraim Budusan on 5/29/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

class PopGestureNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        if responds(to: #selector(getter: interactivePopGestureRecognizer)) {
            interactivePopGestureRecognizer?.delegate = self
            delegate = self
        }
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if responds(to: #selector(getter: interactivePopGestureRecognizer)) {
            interactivePopGestureRecognizer?.isEnabled = false
        }
        super.pushViewController(viewController, animated: animated)
    }
}

extension PopGestureNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        interactivePopGestureRecognizer?.isEnabled = (responds(to: #selector(getter: interactivePopGestureRecognizer)) && viewControllers.count > 1)
    }
}

extension PopGestureNavigationController: UIGestureRecognizerDelegate {}
