//
//  UITextField + Extensions.swift
//  Opinon
//
//  Created by Efraim Budusan on 1/12/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func set(placeHolderColor:UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                        attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor])
    }
    
    func set(placeHolderFont:UIFont) {
        
        if let attributedText = self.attributedPlaceholder {
            var attributes = attributedText.attributes(for: self.placeholder!)
            attributes![NSAttributedString.Key.font] = placeHolderFont
            self.attributedPlaceholder = NSAttributedString.init(string: self.placeholder!, attributes: attributes)
        } else {
            self.attributedPlaceholder = NSAttributedString.init(string: self.placeholder!, attributes: [NSAttributedString.Key.font: placeHolderFont])
        }
    }
    
    func set(placeHolderText:String) {
        if let attributedText = self.attributedPlaceholder {
            let attributes = attributedText.attributes(for: self.placeholder!)
            self.attributedPlaceholder = NSAttributedString(string: placeHolderText, attributes: attributes)
        } else {
            self.placeholder = placeHolderText
        }
    }
}
