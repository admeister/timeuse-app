//
//  ExtendedUIButton.swift
//  StairsShare
//
//  Created by Efraim Budusan on 5/23/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

class ExtendedUIButton: UIButton {
    
    @IBInspectable var offset = CGPoint(x: 3, y: 1 )
    @IBInspectable var isRightImageButton: Bool = false
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 1
    @IBInspectable var highlightColor: UIColor? {
        didSet {
            _backgroundColor = self.backgroundColor
        }
    }
    
    @IBInspectable var selectedBackgroundColor: UIColor? {
        didSet {
            _backgroundColor = self.backgroundColor
        }
    }
    
    @IBInspectable var imageTintColor: UIColor!
    
    private var _backgroundColor:UIColor!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = cornerRadius
        self.tintColor = imageTintColor
    }
    
    override var isHighlighted: Bool {
        didSet {
            if highlightColor != nil {
                self.backgroundColor = isHighlighted ?  highlightColor : _backgroundColor
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            if selectedBackgroundColor != nil {
                self.backgroundColor = isSelected ?  selectedBackgroundColor : _backgroundColor
            }
        }
    }
    
    
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        
        if isRightImageButton {
            var rect = super.imageRect(forContentRect: contentRect)
            rect.origin.x = super.titleRect(forContentRect: contentRect).maxX + offset.x
            rect.origin.y += offset.y
            return rect
        }
        return super.imageRect(forContentRect: contentRect)
    }
    
    override func contentRect(forBounds bounds: CGRect) -> CGRect {
        
        if isRightImageButton {
            let rect = super.contentRect(forBounds: bounds)
            let imageRect = self.imageRect(forContentRect: rect)
            return CGRect(origin: CGPoint(x: rect.origin.x - (imageRect.width + offset.x), y: rect.origin.y), size: rect.size)
        }
        return super.contentRect(forBounds: bounds)
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        self.layoutSubviews()
    }
    
    override func setAttributedTitle(_ title: NSAttributedString?, for state: UIControl.State) {
        super.setAttributedTitle(title, for: state)
        self.layoutSubviews()
    }
    
}
