//
//  UIView+Image.swift
//  Shebah
//
//  Created by Dan Vleju on 10/19/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    
    func captureImageByTakingScaleIntoAccount() -> UIImage {
        let size = self.bounds.size
        //    http://developer.apple.com/library/ios/#qa/qa1703/_index.html
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        //- grab the image using current screen scale
        if self.superview != nil {
            self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        }
        else {
            self.layer.render(in: UIGraphicsGetCurrentContext()!)
        }
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func captureImage() -> UIImage {
        let size = self.bounds.size
        //    http://developer.apple.com/library/ios/#qa/qa1703/_index.html
        UIGraphicsBeginImageContext(size)
        //UIGraphicsBeginImageContextWithOptions(size, NO, 0); - grab the image using current screen scale
        if self.superview != nil {
            self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        }
        else {
            self.layer.render(in: UIGraphicsGetCurrentContext()!)
        }
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func imageWithEdgeInsets(_ edgeInsets: UIEdgeInsets) -> UIImage {
        let rect = self.bounds.inset(by: edgeInsets)
        UIGraphicsBeginImageContext(rect.size)
        UIGraphicsGetCurrentContext()!.translateBy(x: -edgeInsets.left, y: -edgeInsets.top)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

extension UIScrollView {
    //http://stackoverflow.com/questions/6275426/i-want-to-generate-a-image-of-uiscrollview
    
    func _captureImage() -> UIImage {
        UIGraphicsBeginImageContext(self.contentSize)
        self.subviews.last!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
