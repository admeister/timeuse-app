//
//  ExtendedUIControl.swift
//  StairsShare
//
//  Created by Efraim Budusan on 5/23/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

class ExtendedUIControl: UIControl {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    
    private var _backgroundColor:UIColor!
    @IBInspectable var highlightColor: UIColor? {
        didSet {
            _backgroundColor = self.backgroundColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = cornerRadius
    }
    
    override var isHighlighted: Bool {
        didSet {
            if highlightColor != nil {
                self.backgroundColor = isHighlighted ?  highlightColor : _backgroundColor
            }
        }
    }
    
}
