//
//  UIImageView + Tint.swift
//  Opinon
//
//  Created by Efraim Budusan on 3/20/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit


class TintedUIImageView:UIImageView {
    
    @IBInspectable var imageTintColor:UIColor!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
        self.tintColor = imageTintColor
    }
    
    
}

