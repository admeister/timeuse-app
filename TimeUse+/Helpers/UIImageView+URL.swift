//
//  UIImageView+URL.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    private func getImageResource(url: URL?, cacheKey: String?) -> ImageResource? {
        if let url = url {
            return ImageResource(downloadURL: url, cacheKey: cacheKey)
        }
        return nil
    }
    
    func setImageFrom(_ url: URL?, placeholder: UIImage?, cacheKey: String?, _ completion: (() -> Void)? = nil) {
        let resource: Resource? = getImageResource(url: url, cacheKey: cacheKey) ?? url
        self.kf.setImage(with: resource, placeholder: placeholder, options: nil, progressBlock: nil) { _ in
            completion?()
        }
    }
    
    func setImageFromURLString(_ urlString: String?, placeholder: UIImage?, cacheKey: String?) {
        let resource: Resource?
        if let urlString = urlString {
            resource = getImageResource(url: URL(string: urlString), cacheKey: cacheKey)
        } else {
            resource = URL(string: urlString ?? "")
        }
        
        self.kf.setImage(with: resource, placeholder: placeholder, options: nil, progressBlock: nil)
    }
}

