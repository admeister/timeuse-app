//
//  String+Password.swift
//  Reloadly
//
//  Created by Havrisciuc Robert on 11/8/17.
//  Copyright © 2017 Tapptitude. All rights reserved.
//

import Foundation

extension String {
    
    func containsSpecialCharacters() -> Bool {
        return self.components(separatedBy: CharacterSet.alphanumerics).joined(separator: "").count > 0
    }
    
    func containsUppercasedLetters() -> Bool {
        return self.components(separatedBy: CharacterSet.uppercaseLetters.inverted).joined(separator: "").count > 0
    }
    
    func containsLowercasedLetters() -> Bool {
        return self.components(separatedBy: CharacterSet.lowercaseLetters.inverted).joined(separator: "").count > 0
    }
    
    func containsDigits() -> Bool {
        return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "").count > 0
    }
    
    func digitString() -> String {
        return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    func isNumber() -> Bool {
        return !self.containsLowercasedLetters() && !self.containsUppercasedLetters() && !self.containsSpecialCharacters()
    }
    
    func containsOnlyLetters() -> Bool {
        return !self.isEmpty && self.range(of: "[^a-zA-Z ]", options: .regularExpression) == nil
    }
    
    func isValidSSN() -> Bool {
        return !self.isEmpty && self.range(of: "^(?!(000|666|9))\\d{3}-(?!00)\\d{2}-(?!0000)\\d{4}$", options: .regularExpression) != nil
    }
    
    func isLongFormat() -> Bool {
        return !self.isEmpty && self.range(of: "^\\d{1,2}[/]\\d{1,2}[/]\\d{4}$", options: .regularExpression) != nil
    }
    
    func isMediumFormat() -> Bool {
        return !self.isEmpty && self.range(of: "^\\d{1,2}[/]\\d{4}$", options: .regularExpression) != nil
    }
    
    func isShortFormat() -> Bool {
        return !self.isEmpty && self.range(of: "^\\d{4}$", options: .regularExpression) != nil
    }
    
    func extractLetters() -> String {
        // extract letters and replace "-_" with " "
        return self.replacingOccurrences(of: "[^a-zA-Z_ //-]", with: "", options: .regularExpression).replacingOccurrences(of: "[_-]", with: " ", options: .regularExpression)
    }
    
    func configureName() -> String {
        // remove ["mrs", "mr", "ms", "mx", "sr", "jr"] and "name", 'name', (name)
        
        let possibleSuffixesAndPrefixes = ["mrs", "mr", "ms", "sr", "jr", "dr", "mrs.", "mr.", "ms.", "sr.", "jr.", "dr."]
        
        var components = self.components(separatedBy: " ")
        
        if possibleSuffixesAndPrefixes.contains(components.first!.lowercased()) {
            components.remove(at: 0)
        }
        
        components.removeAll(where: {$0.isEmpty})
        
        components.enumerated().forEach { (index, word) in
            if (word.first! == "(" && word.last! == ")") || (word.first! == "\"" && word.last! == "\"") || (word.first! == "'" && word.last! == "'") {
                components.remove(at: index)
            }
        }
        
        var name = components.joined(separator: " ")
        
        name = name.extractLetters()
        
        if name.last == " " {
            name.removeLast()
        }
        
        return name
    }
    
    func randomString() -> String {
        return String((0..<self.count).map{ _ in self.randomElement()! })
    }
}
