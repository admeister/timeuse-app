//
//  GoogleMapsHelper.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/06/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import Foundation
import GoogleMaps

protocol GoogleMapsProtocol {
    var mapView: GMSMapView! { get set }
}

extension GoogleMapsProtocol {
    func createMarker(latitude: Double, longitude: Double, color: UIColor) -> GMSMarker {
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        marker.icon = GMSMarker.markerImage(with: color)
        marker.map = mapView
        marker.isTappable = false
        
        return marker
    }
    
    func createTrack(coordinates: [LocationCoordinates], color: UIColor) -> GMSPolyline {
        let path = GMSMutablePath()
        coordinates.forEach { (coordinate) in
            path.addLatitude(coordinate.latitude, longitude: coordinate.longitude)
        }
        
        let polyline = GMSPolyline(path: path)
        polyline.spans = [GMSStyleSpan(color: color)]
        polyline.strokeWidth = 6
        polyline.geodesic = true
        polyline.map = mapView
        
        return polyline
    }
    
    func createTrackeDistanceMarker(coordinate: LocationCoordinates, distance: String) {
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude))
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 65, height: 35))
        let imageView = UIImageView(image: #imageLiteral(resourceName: "polyline_title_icon"))
        imageView.contentMode = .scaleAspectFill
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 65).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 35).isActive = true
       
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 65, height: 35))
        label.text = distance
        label.font = Constants.Fonts.condensedRoboto.condensedRegular.with(size: 15)
        label.textColor = Constants.Colors.main.dark.value
        label.textAlignment = .center
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let image = renderer.image { ctx in
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        
        marker.icon = image
        marker.isTappable = false
        marker.map = mapView
    }
}
