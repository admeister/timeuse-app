//
//  Date+Extensions.swift
//  OnTapp-Consumer
//
//  Created by Ionut Costin on 25/02/2020.
//  Copyright © 2020 OnTapp. All rights reserved.
//

import Foundation

extension Date {
    
    var birthday: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        return dateFormatter.string(from: self)
    }
    
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        return dateFormatter.string(from: self)
    }
    
    var gmtTime: String {
        let dateFormatter = DateFormatters.gmtDateFormatter
        dateFormatter.dateFormat = "HH:mm"
        
        return dateFormatter.string(from: self)
    }
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return dateFormatter.string(from: self)
    }
    
    var monthYear: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        
        return dateFormatter.string(from: self)
    }
    
    var firstDayOfWeek: Date {
        let firstDay = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        return firstDay!
    }
    
    var lastDayOfWeek: Date {
        let lastDay = Calendar.current.date(byAdding: .day, value: 6, to: self)
        return lastDay!
    }
    
    var day: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        
        return dateFormatter.string(from: self)
    }
    
    func withFormat(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    var calendarHeader: String {
        return "\(self.day) - \(self.lastDayOfWeek.day) \(self.lastDayOfWeek.monthYear)"
    }
}

extension Date {
    
    var age: Int? {
        let components = Calendar.current.dateComponents([.year], from: self, to: Date())
        return components.year
    }
    
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: self)!
    }
}

class DateFormatters {
    static var currentDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.locale = Calendar.current.locale
        formatter.timeZone = Calendar.current.timeZone
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter
    }
    
    static var gmtDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.calendar = Calendar.current
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter
    }
}
