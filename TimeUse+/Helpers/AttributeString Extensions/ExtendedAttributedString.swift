//
//  ExtendedAttributedString.swift
//  Opinon
//
//  Created by Efraim Budusan on 9/4/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation
import UIKit

class ExtendedUILabel: UILabel {
    
    @IBInspectable var mimimumLineHeight:CGFloat = -1.0
    
    var _attributedText:NSAttributedString?
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let attributeText = self.attributedText {
            self.attributedText = addParagraphIfNeeded(attributeText)
        }
    }
    

    override var attributedText: NSAttributedString? {
        get {
            return super.attributedText
        }

        set {
            var _newValue:NSAttributedString? = newValue
            if let attrString = _newValue {
                _newValue = addParagraphIfNeeded(attrString)
            }
            super.attributedText = _newValue
        }
    }
    
    func addParagraphIfNeeded(_ attributedString:NSAttributedString) -> NSAttributedString {
        
        let paragraphStye = NSMutableParagraphStyle()
        
        if mimimumLineHeight != -1.0 {
            paragraphStye.minimumLineHeight = mimimumLineHeight
        }
        
        
        let string = attributedString.string
        let range = string.range(of: string)!
        let nsRange = NSRange(range,in:string)

        let attributeText = NSMutableAttributedString(attributedString: attributedString)
        attributeText.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStye], range:nsRange)
        
        return attributeText
        
        
        
    }
    
    
    
    
    
}


