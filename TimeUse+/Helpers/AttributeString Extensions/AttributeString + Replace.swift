//
//  AttributeString + Replace.swift
//  Webcar
//
//  Created by Efraim Budusan on 7/26/17.
//  Copyright © 2017 Tapptitude. All rights reserved.
//

import Foundation
import UIKit


public typealias TextAttributes = [NSAttributedString.Key:Any]

extension NSAttributedString {
    
    func replacing(key:String, with valueString:String) -> NSAttributedString {
        
        if let range = self.string.range(of:key) {
            let nsRange = NSRange(range, in: self.string)
            let mutableText = NSMutableAttributedString(attributedString: self)
            mutableText.replaceCharacters(in: nsRange, with: valueString)
            return mutableText as NSAttributedString
        }
        return self
    }
    
    func replacing(with valueString:String) -> NSAttributedString {
        let mutableText = NSMutableAttributedString(attributedString: self)
        mutableText.mutableString.setString(valueString)
        return mutableText as NSAttributedString
    }
    
    func replacing(by dict:[String:String]) -> NSAttributedString {
        
        let mutableText = NSMutableAttributedString(attributedString: self)
        for (key,value) in dict {
            let stringValue = mutableText.string
            if let range = stringValue.range(of:key) {
                let nsRange = NSRange(range,in:stringValue)
                mutableText.replaceCharacters(in: nsRange, with: value)
            }
        }
        return mutableText
    }
    
    func attributedString(for key:String) -> NSAttributedString? {
        
        if let range = self.string.range(of:key) {
            let nsRange = NSRange(range,in:self.string)
            return self.attributedSubstring(from: nsRange)
        }
        return nil
    }
    
    func setting(style:TextAttributes, for key:String) -> NSAttributedString {
        let mutableText = NSMutableAttributedString(attributedString: self)
        let stringValue = mutableText.string
        if let range = stringValue.range(of: key) {
            let nsRange = NSRange(range,in:stringValue)
            mutableText.addAttributes(style, range: nsRange)
        }
        return mutableText
    }
    
    func setting(style:TextAttributes, for keys:[String]) -> NSAttributedString {
        let mutableText = NSMutableAttributedString(attributedString: self)
        for key in keys {
            let stringValue = mutableText.string
            if let range = stringValue.range(of: key) {
                let nsRange = NSRange(range,in:stringValue)
                mutableText.addAttributes(style, range: nsRange)
            }
        }
        return mutableText
    }
    
    func setting(style:TextAttributes) -> NSAttributedString {
        return self.setting(style: style, for: self.string)
    }
   
    func attributes(for key:String) -> TextAttributes? {
        
        if let range = self.string.range(of:key) {
            let nsRange = NSRange(range,in:self.string)
            return self.attributes(at: nsRange.lowerBound, longestEffectiveRange: nil, in: nsRange)
        }
        return nil
    }
    
    class func build(fromValuesWithTemplates values:[(String,TextAttributes)]) -> NSAttributedString {
        return values.reduce(NSMutableAttributedString(), { (result, value) -> NSMutableAttributedString in
            result.append(NSAttributedString(string: value.0, attributes: value.1))
            return result
        })
    }
    
    func adding(attributes:TextAttributes, to key:String) -> NSAttributedString {
        if let range = self.string.range(of: key) {
            let nsRange = NSRange(range,in:self.string)
            let mutableCopy = NSMutableAttributedString(attributedString: self)
            mutableCopy.addAttributes(attributes, range: nsRange)
            return mutableCopy as NSAttributedString
        }
        return self
    }
}

extension String {
    
    func occuranceCount(of substring:String) -> Int {
        return self.components(separatedBy: substring).count - 1
    }
}

extension String {
    func attributed(style: TextAttributes) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: style)
    }
}


extension Dictionary: Equatable where Key == NSAttributedString.Key, Value == Any  {
    
    public static func == (lhs: Dictionary<NSAttributedString.Key,Value>, rhs: Dictionary<NSAttributedString.Key,Value>) -> Bool  {

        if lhs.count != rhs.count {
            return false
        }
        
        if let fontA = lhs[NSAttributedString.Key.font] as? UIFont ,
            let fontB = rhs[NSAttributedString.Key.font] as? UIFont {
            if fontA != fontB {
                return false
            }
        }
        return true
    }
    
    public static func !=(lhs: Dictionary<NSAttributedString.Key,Value>, rhs: Dictionary<NSAttributedString.Key,Value>) -> Bool  {
        return !(lhs == rhs)
    }

    
}

