//
//  ErrorDisplay.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 19/05/2020.
//  Copyright (c) 2020 Tapptitude. All rights reserved.
//

import UIKit
import SwiftMessages
import Tapptitude

class ErrorDisplay: TTErrorDisplay {
    
    static func showError(_ error: NSError, fromViewController: UIViewController) {
        let viewController:UIViewController? = fromViewController
        self.checkAndShowError(error, fromViewController: viewController)
    }
    
    static func checkAndShowError(_ error:NSError?, fromViewController:UIViewController?) {
        guard (error != nil) else {
            return
        }
        
        let error = error!
        let ignoreError = ((error.domain == "NSURLErrorDomain" && error.code == -999)
            || (error.domain == "WebKitErrorDomain" && error.code == 102));
        
        if (ignoreError) {
            return;
        }
        var fromController = fromViewController
        if fromViewController?.isViewLoaded == true {
            let isNotVisible = fromViewController?.view.window == nil
            if isNotVisible {
                fromController = UIApplication.shared.windows.first{ $0.isKeyWindow }?.rootViewController
            }
        }
        
        if error.domain == "NSURLErrorDomain" && error.code == NSURLErrorNotConnectedToInternet {
            self.showNoNetworkErrorWithAction(fromController)
            return
        }
        
        showErrorToast(withMessage: error.localizedDescription, fromViewController: fromController)
    }
    
    static func showErrorToast(with error:Error) {
        self.showErrorToast(withMessage: error.localizedDescription)
    }
    
    static func showSuccesToast(message:String, title:String? = nil, fromViewController:UIViewController? = nil) {
        
        let view = try! SwiftMessages.viewFromNib(named: "CustomToast") as! CustomToastView
        view.configureContent(title: title ?? Texts.Toast.success.text, body: message)
        view.configure(for: .success)
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.preferredStatusBarStyle = .default
        SwiftMessages.show(config: config, view: view)
    }
    
    static func showErrorToast(withMessage message:String, title:String? = nil,  fromViewController:UIViewController? = nil) {
        
        let view = try! SwiftMessages.viewFromNib(named: "CustomToast") as! CustomToastView
        
        view.configureContent(title: title ?? Texts.Toast.error.text, body: message)
        view.configure(for: .error)
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.preferredStatusBarStyle = .default
        SwiftMessages.show(config: config, view: view)
    }
    
    static func showAlertToast(withMessage message:String, title:String? = nil,  fromViewController:UIViewController? = nil) {
        
        let view = try! SwiftMessages.viewFromNib(named: "CustomToast") as! CustomToastView
        
        view.configureContent(title: title ?? "", body: message)
        view.configure(for: .alert)
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.preferredStatusBarStyle = .lightContent
        SwiftMessages.show(config: config, view: view)
    }

    static func showNoNetworkErrorWithAction(_ fromViewController:UIViewController?) {
        let alertController = UIAlertController(title: nil, message: Texts.Toast.noNetworkConnection.text, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: Texts.Other.settings.text, style: .default, handler: { (_) -> Void in
            let url = URL(string: UIApplication.openSettingsURLString)
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }))
        
        let controller = fromViewController ?? UIApplication.shared.windows.first{ $0.isKeyWindow }?.rootViewController
        controller?.present(alertController, animated: true, completion: nil)
    }
}


extension UIViewController {
    
    
    func checkAndShowError(_ error : NSError?) {
        ErrorDisplay.checkAndShowError(error, fromViewController:self)
    }
    
    func checkAndShow(error : Error?) {
        ErrorDisplay.checkAndShowError(error as NSError? , fromViewController:self)
    }
}
