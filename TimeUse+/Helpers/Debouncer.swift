//
//  Debouncer.swift
//  OnTapp-Consumer
//
//  Created by Ionut Costin on 21/02/2020.
//  Copyright © 2020 OnTapp. All rights reserved.
//

import Foundation

class Debouncer {
    
    /**
     Create a new Debouncer instance with the provided time interval.
     
     - parameter timeInterval: The time interval of the debounce window.
     */
    init(timeInterval: TimeInterval, delay: TimeInterval = 0) {
        self.timeInterval = timeInterval
        self.delay = delay
    }
    
    init(timeInterval:TimeInterval, handler:@escaping Handler) {
        self.timeInterval = timeInterval
        self.delay = 0
        self.handler = handler
    }
    
    var onlyThrottle:Bool = true
    
    typealias Handler = () -> Void
    
    /// Closure to be debounced.
    /// Perform the work you would like to be debounced in this handler.
    var handler: Handler?
    
    /// Time interval of the debounce window.
    private let timeInterval: TimeInterval
    private let delay: TimeInterval
    
    private var delayTimer: Timer?
    private var debounceTimer: Timer?
    
    /// Indicate that the handler should be invoked.
    /// Begins the debounce window with the duration of the time interval parameter.
    func fire() {
        
        if let timer = debounceTimer, timer.isValid, onlyThrottle {
            return
        }
        // Invalidate existing timer if there is one
        debounceTimer?.invalidate()
        debounceTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false, block: { [weak self] (timer) in
            self?.handleTimer(timer)
        })
    }
    
    private func handleTimer(_ timer: Timer) {
        guard timer.isValid else {
            return
        }
        handler?()
    }
    
    deinit {
        debounceTimer?.invalidate()
    }
}
