//
//  NotificationsHandler.swift
//  OnTapp-Bartender
//
//  Created by Ionut Costin on 23/04/2020.
//  Copyright © 2020 OnTapp. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationsHandler {
    
    static let shared = NotificationsHandler()
    
    //MARK: - UNUserNotificationCenterDelegate
    
    func handleNotification(_ notification: UNNotification, withCompletionHandler completionHandler: @escaping () -> Void) {

        handleNotificationTap(userInfo: notification.request.content.userInfo)

        completionHandler()
    }

    func handleNotification(_ notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        completionHandler([.alert])
    }

    //MARK: - Application
    
    func handleNotificationUserInfo(_ userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

//        handleSilentNotification(userInfo: userInfo)

        completionHandler(.newData)
    }
    
    //MARK: - Handle Notification Tap
    
    private func handleNotificationTap(userInfo: [AnyHashable: Any]) {
//        guard let notificationBase = notificationDecode(userInfo: userInfo) else { return }
    }
    
    
}
