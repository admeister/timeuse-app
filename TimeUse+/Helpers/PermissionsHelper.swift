//
//  PermissionsHelper.swift
//  TimeUse
//
//  Created by Robert Havrisciuc on 04.03.2021.
//  Copyright © 2021 Tapptitude. All rights reserved.
//

import CoreLocation
import CoreMotion

class PermissionsManager: NSObject {
    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.allowsBackgroundLocationUpdates = true
        manager.delegate = self
        return manager
    }()

    private lazy var motionManager: CMMotionActivityManager = {
        let manager = CMMotionActivityManager()
        return manager
    }()

    func obtainLocationPermission() {
        if #available(iOS 13.0, *) {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.requestAlwaysAuthorization()
        }
    }

    func obtainMotionActivityPermission() {
        motionManager.queryActivityStarting(from: Date(), to: Date(), to: OperationQueue.main) { _, _ in
        }
    }
}

extension PermissionsManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if case .authorizedWhenInUse = status {
            locationManager.requestAlwaysAuthorization()
        }
    }
}
