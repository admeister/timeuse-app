//
//  PushNotificationHandler.swift
//  OnTapp
//
//  Created by Ionut Costin on 15/04/2020.
//  Copyright © 2020 OnTapp. All rights reserved.
//

import UIKit
import FirebaseMessaging

class PushNotificationHandler: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    static let shared = PushNotificationHandler()
    
    //MARK: - Getters and Setters
    
    var apnsToken: Data? {
        get { Messaging.messaging().apnsToken }
        set { Messaging.messaging().apnsToken = newValue }
    }
    
    func registerForPushNotifications() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) {[weak self] (authorized, error) in
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
                self?.updateFCMForCurrentUser()
            }
        }
    }
    
    func updateFCMForCurrentUser() {
        guard Session.isValidSession(), let token = Messaging.messaging().fcmToken else { return }
        API.updatePushToken(token)
    }
    
    //MARK: - MessagingDelegate
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        updateFCMForCurrentUser()
    }
    
    //MARK: - UNUserNotificationCenterDelegate
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        NotificationsHandler.shared.handleNotification(response.notification, withCompletionHandler: completionHandler)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        NotificationsHandler.shared.handleNotification(notification, withCompletionHandler: completionHandler)
    }
    
    //MARK: - Application
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        NotificationsHandler.shared.handleNotificationUserInfo(userInfo, fetchCompletionHandler: completionHandler)
    }
    
}
