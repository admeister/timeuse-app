//
//  as.swift
//  PeopleFinders
//
//  Created by Efraim Budusan on 12/14/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation

struct AssociatedObjectContainerKey {
    static var associatedObjectContainer = "associatedObjectContainer"
}

public protocol AssociatedObjectContainerProtocol: class {
    
    var associatedObjectContainer:[String:Any] { get set }
    func getObject<T>(for key:String, defaultInitializer:(() -> T)) -> T
    func setObject<T>(object:T, for key:String)
}

public extension AssociatedObjectContainerProtocol {
    
    var associatedObjectContainer:[String:Any] {
        get {
            
            return (objc_getAssociatedObject(self, &AssociatedObjectContainerKey.associatedObjectContainer) as? [String:Any]) ?? [:]
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectContainerKey.associatedObjectContainer, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func setObject<T>(object:T, for key:String) {
        associatedObjectContainer[key] = object
    }
    
    func getObject<T>(for key:String, defaultInitializer:(() -> T)) -> T {
        if let object = associatedObjectContainer[key] as? T {
            return object
        } else {
            let object = defaultInitializer()
            associatedObjectContainer[key] = object
            return object
        }
        
    }
}
