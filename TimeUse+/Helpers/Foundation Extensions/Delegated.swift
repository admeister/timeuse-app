//
//  Delegation.swift
//  PeopleFinders
//
//  Created by Efraim Budusan on 12/14/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation

struct Delegated<Input> {
    
    private(set) var callback: ((Input) -> Void)?
    
    mutating func delegate<Object : AnyObject>(to object: Object, with callback: @escaping (Object, Input) -> Void) {
        self.callback = { [weak object] input in
            guard let object = object else {
                return
            }
            callback(object, input)
        }
    }
    
    
}




class Bindable<Input> {
    
    private(set) var notify: ((Input) -> Void)?
    
    func bind<Object : AnyObject>(to object: Object, with callback: @escaping (Object, Input) -> Void) {
        self.notify = { [weak object] input in
            guard let object = object else {
                return
            }
            callback(object, input)
        }
    }
    
    
}
