//
//  String + OptionalCompose.swift
//  PeopleFinders
//
//  Created by Efraim Budusan on 11/28/18.
//  Copyright © 2018 Tapptitude. All rights reserved.
//

import Foundation


extension String {
    
    
    static func compose(_ values: String?...) -> String? {
        
        guard values.count > 0 else {
            return nil
        }
        var final:String = ""
        for value in values {
            if let _value = value {
                final += _value
            } else {
                return nil
            }
        }
        return final
        
    }
    
    
}

extension String {
    
    init?(_ value:Int?) {
        guard let value = value else {
            return nil
        }
        self.init(describing:value)
    }
    
    
    init?(_ value:Double?) {
        guard let value = value else {
            return nil
        }
        self.init(describing:value)
    }
    
    
}
