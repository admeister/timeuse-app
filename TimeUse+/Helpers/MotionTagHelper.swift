//
//  MotionTagHelper.swift
//  TimeUse+
//
//  Created by Robert Havrisciuc on 24/08/2020.
//  Copyright © 2020 Tapptitude. All rights reserved.
//

import MotionTagSDK

protocol MotionTagHelperDelegate: class {
    func didFinishSetup()
}

class MotionTagHelper: NSObject {
    private(set) var isSetupFinished = false
    private(set) var trackingStatus = false

    static var shared = MotionTagHelper()

    private var motionTag: MotionTag!
    weak var delegate: MotionTagHelperDelegate?
    
    private override init() {
        super.init()
        
        var settings: [String: AnyObject] = [:]
        
        if Session.isValidSession(), let currentUser = Session.currentUser {
            settings = currentUser.settings.motionTagSettings
        } else {
            settings = [kMTDataTransferMode: DataTransferMode.wifiOnly.rawValue as AnyObject,
                        kMTBatterySavingsMode: true as AnyObject]
        }

        motionTag = MotionTagCore.sharedInstance(withToken: Session.motionTagJWTToken, settings: settings, completion: {
            self.isSetupFinished = true
            self.delegate?.didFinishSetup()
        })
        motionTag.delegate = self
    }
    
    func start(settings: [String: AnyObject]? = nil) {
        if let settings = settings {
            motionTag.start(withToken: Session.motionTagJWTToken, settings: settings)
        } else {
            motionTag.start(withToken: Session.motionTagJWTToken)
        }
    }

    func stop() {
        motionTag.stop()
    }
}

// MARK: MotionTagDelegate

extension MotionTagHelper: MotionTagDelegate {
    func trackingStatusChanged(_ isTracking: Bool) {
        print("MotionTag SDK trackingStatusChanged: \(isTracking)")
        trackingStatus = isTracking
    }

    func locationAuthorizationStatusDidChange(_ status: CLAuthorizationStatus, precise: Bool) {
        print("MotionTag SDK   CLAuthorizationStatus: \(status.rawValue) precise: \(precise)")
    }

    func motionActivityAuthorized(_ authorized: Bool) {
        print("MotionTag SDK   motionActivityAuthorized: \(authorized)")
    }

    func didTrackLocation(_ location: CLLocation) {
        print("MotionTag SDK didTrackLocation - CLLocation: \(location)")
    }

    func didTransmitData(timestamp: Date, lastEventTimestamp: Date) {
        print("MotionTag SDK didTransmitData - timestamp: \(timestamp), lastEventTimestamp: \(lastEventTimestamp)")
    }
}
